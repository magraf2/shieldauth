package ch.shield.auth.engine;

import lombok.Builder;
import lombok.Getter;

import java.util.Map;
import java.util.stream.Stream;

@Builder
public class ContextRootObject {
    private final Map<String, String[]> inputs;
    private final Map<String, String[]> headers;
    private final Map<String, Object> attributes;

    @Getter
    private final Input input = new Input();
    @Getter
    private final Header header = new Header();
    @Getter
    private final Session session = new Session();

    private class Input {
        public String get(String name){
            return Stream.of(inputs.get(name)).findFirst().orElse(null);
        }

        public String get(String name, int i){
            return inputs.get(name)[i];
        }
    }

    private class Header {
        public String get(String name){
            return Stream.of(headers.get(name)).findFirst().orElse(null);
        }

        public String get(String name, int i){
            return headers.get(name)[i];
        }
    }

    private class Session {
        public String get(String name){
            return (String) attributes.get(name);
        }
    }
}

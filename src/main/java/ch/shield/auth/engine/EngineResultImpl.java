package ch.shield.auth.engine;

import ch.shield.auth.core.engine.EngineResult;
import ch.shield.auth.core.repository.User;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.Singular;

import java.util.Map;
import java.util.Optional;

@Data
@Builder
class EngineResultImpl implements EngineResult {
    private final boolean finished;
    @NonNull @Singular
    private final Map<String, Object> sessionAttributes;
    private final User user;
    private final String guiName;
    @NonNull @Singular
    private final Map<String, Object> guiAttributes;
    private final Integer statusCode;

    @Override
    public Optional<User> getUser() {
        return Optional.ofNullable(user);
    }

    @Override
    public Optional<String> getGuiName() {
        return Optional.ofNullable(guiName);
    }

    @Override
    public Optional<Integer> getStatusCode() {
        return Optional.ofNullable(statusCode);
    }
}

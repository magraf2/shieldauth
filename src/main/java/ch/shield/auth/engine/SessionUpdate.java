package ch.shield.auth.engine;

import ch.shield.auth.core.engine.EngineException;
import ch.shield.auth.core.repository.User;
import ch.shield.auth.core.state.State;
import ch.shield.auth.core.state.result.StateResult;
import ch.shield.auth.core.state.result.SuccessStateResult;
import ch.shield.auth.core.state.result.TransitionStateResult;
import lombok.Builder;
import lombok.Data;

import java.util.Map;

@Data
@Builder
class SessionUpdate {
    private final Map<String, Object> attributes;
    private final User user;
    private final boolean finished;
    private final State lastState;
    private final String lastTransition;
    private final EngineException exception;

    static SessionUpdate success(StateResult stateResult, State state) {
        final var builder = SessionUpdate.builder()
                .attributes(stateResult.getSessionAttributes())
                .user(stateResult.getUser().orElse(null))
                .lastState(state)
                .exception(null)
                .finished(stateResult instanceof SuccessStateResult);
        if(stateResult instanceof TransitionStateResult){
            final var transitionStateResult = (TransitionStateResult) stateResult;
            builder.lastTransition(transitionStateResult.getTransition());
        }
        return builder.build();
    }

    static SessionUpdate stateException(State state) {
        return SessionUpdate.builder()
                .attributes(Map.of())
                .user(null)
                .finished(false)
                .lastState(state)
                .lastTransition(null)
                .exception(null)
                .build();
    }

    static SessionUpdate stateFinalException(EngineException exception, State state) {
        return SessionUpdate.builder()
                .attributes(Map.of())
                .user(null)
                .finished(false)
                .lastState(state)
                .lastTransition(null)
                .exception(exception)
                .build();
    }
}
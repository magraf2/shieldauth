package ch.shield.auth.engine;

import ch.shield.auth.core.LoggerConstants;
import ch.shield.auth.core.config.ConfigFile;
import ch.shield.auth.core.engine.*;
import ch.shield.auth.core.state.Context;
import ch.shield.auth.core.state.State;
import ch.shield.auth.core.state.result.RenderableStateResult;
import ch.shield.auth.core.state.result.StateFinalException;
import ch.shield.auth.core.state.result.StateResult;
import ch.shield.auth.core.state.result.TransitionStateResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Component
public class EngineImpl implements Engine {
    private final static Logger LOG = LoggerFactory.getLogger(LoggerConstants.LOGGER_ENGINE);
    private final Map<UUID, Session> sessions = new ConcurrentHashMap<>();
    private final int maxStates;
    private final int timeoutInSeconds;
    private final ConfigFile configFile;

    public EngineImpl(@Value("${engine.maxStates:20}") final int maxStates, @Value("${session.inmemory.timeoutSec:300}") final int timeoutInSeconds,
                      @Autowired ConfigFile configFile) {
        this.maxStates = maxStates;
        this.timeoutInSeconds = timeoutInSeconds;
        this.configFile = configFile;
    }

    @Scheduled(fixedRateString = "${session.inmemory.cleanupRateMs:10000}")
    public void cleanUp() {
        LOG.debug("Searching for outdated sessions");
        var outdated = sessions.values().stream()
                .filter(s -> s.isOutdated(timeoutInSeconds))
                .collect(Collectors.toList());
        LOG.debug(String.format("Found '%s' outdated sessions", outdated.size()));
        for (var session : outdated) {
            kill(session.getId());
        }
        LOG.info(String.format("Killed '%s' outdated sessions", outdated.size()));
    }

    @Override
    public UUID init(@NotNull SessionAttributes sessionAttributes) {
        final var session = new Session(sessionAttributes);
        sessions.put(session.getId(), session);
        LOG.info("Created session {}", session.getId());
        if (LOG.isDebugEnabled())
            LOG.debug("Session is {}", session);
        return session.getId();
    }

    @Override
    public EngineResult request(@NotNull EngineRequest request) {
        final var session = getSession(request.getSessionId());
        LOG.debug("Start request for session {} as soon as possible", session.getId());
        return session.execute(s -> executeRequest(s, request));
    }

    private EngineResult executeRequest(Session session, EngineRequest request) {
        LOG.info("Start request for session {}", session.getId());
        LOG.debug("Session is {}", session);
        for (var i = 0; i < maxStates; i++) {
            throwIfException(session);
            if (session.isFinished()) {
                LOG.info("Authentication has finished successfully");
                return finished(session);
            }
            final var state = getNextState(session);
            final var context = getContext(session, request, state);
            final var stateResult = executeStep(state, context, session);
            if (stateResult instanceof  TransitionStateResult) {
                final var transitionStateResult = (TransitionStateResult) stateResult;
                LOG.debug("Follow transition {}", transitionStateResult.getTransition());
            }
            if (stateResult instanceof RenderableStateResult) {
                final var renderableStateResult = (RenderableStateResult) stateResult;
                LOG.info("State shows gui {} without exception", renderableStateResult.getGuiName());
                return renderable(renderableStateResult, session);
            }
        }
        throw EngineExceptionImpl.maxStatesReached();
    }

    private static void throwIfException(final Session session){
        final var exception = session.getException();
        if (exception.isPresent()) {
            LOG.warn("Authentication has failed: {}", exception.get().getMessage(), exception.get());
            throw exception.get();
        }
    }

    private static EngineResult finished(final Session session){
        return EngineResultImpl.builder()
                .user(session.getUser().orElse(null))
                .finished(true)
                .sessionAttributes(session.getAttributes())
                .build();
    }

    private State getNextState(final Session session){
        final var flow = session.getSessionAttributes().getFlow();
        final var lastState = session.getLastState().orElse(null);
        final var lastTransition = session.getLastTransition().orElse(null);
        final var state = configFile.getNextState(flow, lastState, lastTransition);
        LOG.debug("Next state is {}", state.getName());
        return state;
    }

    private Context getContext(final Session session, final EngineRequest request, final State state){
        final var flow = session.getSessionAttributes().getFlow();
        final var repository = configFile.getRepository(flow);
        final var lastState = session.getLastState().orElse(null);
        final var lastTransition = session.getLastTransition().orElse(null);
        final var context = ContextImpl.builder()
                .inputs(getInputs(state, request, session))
                .user(session.getUser().orElse(null))
                .repository(repository)
                .attributes(session.getAttributes())
                .lastState(lastState)
                .lastTransition(lastTransition)
                .build();
        LOG.debug("Context is {}", context);
        return context;
    }

    private Map<String, String> getInputs(final State state, final EngineRequest request, final Session session){
        final var contextRoot = ContextRootObject.builder()
                .headers(request.getHeaders())
                .inputs(request.getInputs())
                .attributes(session.getAttributes())
                .build();
        final var context = new StandardEvaluationContext(contextRoot);
        final var parser = new SpelExpressionParser();
        return configFile.getInputs(state).entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, x -> parse(parser, context, x.getValue())));
    }

    private static String parse(final SpelExpressionParser parser, final StandardEvaluationContext context, final String expression){
        //FIXME test this!
        final var exp = parser.parseExpression(expression);
        return (String) exp.getValue(context);
    }

    private StateResult executeStep(final State state, final Context context, final Session session) {
        SessionUpdate sessionUpdate = null;
        try {
            final var stateResult = state.execute(context);
            LOG.debug("Result is {}", stateResult);
            sessionUpdate = SessionUpdate.success(stateResult, state);
            return stateResult;
        } catch (StateFinalException e) {
            LOG.info("There was a StateException while execute state {}: {}", state, e.getMessage());
            final var engineException = EngineExceptionImpl.wrap(e, e.getStatusCode(), e.getOutputMessage());
            sessionUpdate = SessionUpdate.stateFinalException(engineException, state);
            throw engineException;
        } catch (Throwable e) {
            LOG.warn("There was a Exception while execute state {}: {}", state, e.getMessage());
            final var engineException = EngineExceptionImpl.wrap(e);
            sessionUpdate = SessionUpdate.stateFinalException(engineException, state);
            throw engineException;
        } finally {
            updateSession(session, sessionUpdate);
        }
    }

    private static void updateSession(final Session session, final SessionUpdate sessionUpdate){
        session.update(sessionUpdate);
        LOG.debug("Session is {}", session);
    }

    private static EngineResult renderable(final RenderableStateResult renderableStateResult, final Session session){
        return EngineResultImpl.builder()
                .user(session.getUser().orElse(null))
                .guiName(renderableStateResult.getGuiName())
                .guiAttributes(renderableStateResult.getGuiAttributes())
                .statusCode(renderableStateResult.getStatusCode())
                .finished(session.isFinished())
                .sessionAttributes(session.getAttributes())
                .build();
    }

    @Override
    public boolean isFinished(@NotNull UUID sessionId) {
        final var session = getSession(sessionId);
        LOG.info("Checking session {}", session.getId());
        LOG.debug("Session is {}", session);
        final var exception = session.getException();
        if (exception.isPresent()) {
            LOG.warn("Authentication has failed: {}", exception.get().getMessage(), exception.get());
            throw exception.get();
        }
        if (session.isFinished()) {
            LOG.info("Authentication has finished successfully");
        } else {
            LOG.info("Authentication is in Progress");
        }
        return session.isFinished();
    }

    @Override
    public <T extends SessionAttributes> T getAttributes(@NotNull UUID sessionId, @NotNull Class<T> type) {
        final var session = getSession(sessionId);
        return type.cast(session.getSessionAttributes());
    }

    private void kill(@NotNull UUID sessionId) {
        //Do this in context of session so that we do not face concurrency issues
        LOG.debug("Kill session {} as soon as possible", sessionId);
        final var session = getSession(sessionId);
        session.execute(s -> sessions.remove(s.getId()));
        LOG.info("Killed session {}", session.getId());
    }

    private Session getSession(UUID sessionId) {
        LOG.debug("Try to load session {}", sessionId);
        final var session = sessions.get(sessionId);
        if (session == null) {
            throw EngineExceptionImpl.invalidSession(sessionId);
        }
        return session;
    }
}
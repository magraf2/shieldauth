package ch.shield.auth.engine;

import ch.shield.auth.core.engine.EngineException;
import ch.shield.auth.core.engine.SessionAttributes;
import ch.shield.auth.core.repository.User;
import ch.shield.auth.core.state.State;
import lombok.Getter;
import lombok.ToString;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.function.Function;

@Getter
@ToString
class Session {
    private final UUID id;
    private final SessionAttributes sessionAttributes;
    private final Map<String, Object> attributes;
    private EngineException exception;
    private User user;
    private boolean finished;
    private State lastState;
    private String lastTransition;
    private LocalDateTime lastAccess;

    Session(SessionAttributes sessionAttributes) {
        this.id = UUID.randomUUID();
        this.sessionAttributes = sessionAttributes;
        this.attributes = new HashMap<>();
        this.lastAccess = LocalDateTime.now();
    }

    Optional<State> getLastState() {
        return Optional.ofNullable(lastState);
    }

    Optional<String> getLastTransition() {
        return Optional.ofNullable(lastTransition);
    }

    Optional<User> getUser() {
        return Optional.ofNullable(user);
    }

    Optional<EngineException> getException() {
        return Optional.ofNullable(exception);
    }

    Map<String, Object> getAttributes() {
        return Collections.unmodifiableMap(attributes);
    }

    synchronized <T> T execute(Function<Session, T> action) {
        return action.apply(this);
    }

    void update(SessionUpdate update) {
        attributes.putAll(update.getAttributes());
        //Delete attributes that are set to null
        update.getAttributes().entrySet().stream()
                .filter(x -> x.getValue() == null)
                .map(Map.Entry::getKey)
                .forEach(attributes::remove);
        exception = update.getException();
        user = update.getUser() == null ? user : update.getUser();
        finished = update.isFinished();
        lastState = update.getLastState();
        lastTransition = update.getLastTransition();
        this.lastAccess = LocalDateTime.now();
    }

    boolean isOutdated(long timeoutInSeconds) {
        final var sinceLastAccess = lastAccess.until(LocalDateTime.now(), ChronoUnit.SECONDS);
        return sinceLastAccess > timeoutInSeconds;
    }
}


package ch.shield.auth.engine;

import ch.shield.auth.core.engine.EngineException;

import java.util.UUID;

public class EngineExceptionImpl extends EngineException {
    private final static String OUT_GENERAL = "General Server Error";
    private final static String OUT_SESSION = "Invalid session or timeout";

    private EngineExceptionImpl(String message, Throwable cause, int statusCode, String outputMessage) {
        super(message, cause, statusCode, outputMessage);
    }

    static EngineException maxStatesReached() {
        return new EngineExceptionImpl("Max number of states reached", null, 500, OUT_GENERAL);
    }

    public static EngineException invalidSession(final UUID sessionId) {
        return new EngineExceptionImpl("Session '" + sessionId + "' does not exist no more", null, 400, OUT_SESSION);
    }

    public static EngineException wrap(final Throwable cause, final int statusCode, final String outputMessage) {
        return new EngineExceptionImpl(cause.getMessage(), cause, statusCode, outputMessage);
    }

    public static EngineException wrap(final Throwable cause) {
        return new EngineExceptionImpl(cause.getMessage(), cause, 500, OUT_GENERAL);
    }

}

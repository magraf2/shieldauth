package ch.shield.auth.engine;

import ch.shield.auth.core.repository.Repository;
import ch.shield.auth.core.repository.User;
import ch.shield.auth.core.state.Context;
import ch.shield.auth.core.state.State;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Singular;

import java.util.Map;
import java.util.Optional;

@Getter
@Builder
class ContextImpl implements Context {
    @NonNull @Singular
    private final Map<String, String> inputs;
    private final User user;
    @NonNull
    private final Repository repository;
    @NonNull @Singular
    private final Map<String, Object> attributes;
    private final State lastState;
    private final String lastTransition;

    @Override
    public Optional<String> getInput(final String name) {
        return Optional.ofNullable(inputs.get(name));
    }

    @Override
    public <T> Optional<T> getAttribute(String name, Class<T> type) {
        final var attribute = attributes.get(name);
        if(attribute == null) {
            return Optional.empty();
        }
        return Optional.of(type.cast(attribute));
    }

    @Override
    public Optional<User> getUser() {
        return Optional.ofNullable(user);
    }

    @Override
    public Optional<State> getLastState() {
        return Optional.ofNullable(lastState);
    }

    @Override
    public Optional<String> getLastTransition() {
        return Optional.ofNullable(lastTransition);
    }
}

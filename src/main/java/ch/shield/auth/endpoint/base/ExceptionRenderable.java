package ch.shield.auth.endpoint.base;

import ch.shield.auth.core.Renderable;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import java.util.List;
import java.util.Map;

@Data
class ExceptionRenderable implements Renderable {
    private final String name = "error";
    private final Map<String, List<String>> headers = Map.of();
    private final Map<String, Object> attributes;
    private final int statusCode;

    @Builder
    ExceptionRenderable(@NonNull final String message, final int statusCode) {
        this.statusCode = statusCode;
        this.attributes = Map.of(Renderable.EXCEPTION, message);
    }
}

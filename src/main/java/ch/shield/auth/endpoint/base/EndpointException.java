package ch.shield.auth.endpoint.base;

import ch.shield.auth.core.Renderable;
import ch.shield.auth.core.engine.EngineException;
import lombok.Getter;

public class EndpointException extends RuntimeException {
    private final static String OUT_STEP_UP_NOT_SUPPORTED = "Step up is not supported";
    private final static String OUT_GENERAL = "General Server Error";
    private final static String OUT_MESSAGE_FLOW = "Mal configuration of authentication config";
    private final static String OUT_PARSE_INPUT = "Could not parse input";
    private final static String OUT_INVALID_COOKIE = "Invalid Session Cookie";
    @Getter
    private final int statusCode;
    @Getter
    private final String outputMessage;
    @Getter
    private final Renderable renderable;

    private EndpointException(final String message, final String outputMessage, final int statusCode) {
        super(message);
        this.statusCode = statusCode;
        this.outputMessage = outputMessage;
        this.renderable = new ExceptionRenderable(outputMessage, statusCode);
    }

    private EndpointException(final String message, final Throwable e, final String outputMessage, final int statusCode) {
        super(message, e);
        this.statusCode = statusCode;
        this.outputMessage = outputMessage;
        this.renderable = new ExceptionRenderable(outputMessage, statusCode);
    }

    public static EndpointException missingSessionAttribute(String property, String attribute) {
        final var message = String.format("Session attribute '%s' is not set. This session attribute is configured as %s", attribute, property);
        return new EndpointException(message, OUT_MESSAGE_FLOW, 500);
    }

    public static EndpointException unknownEngineResult() {
        final var message = "Result of engine_old is not renderable or finished";
        return new EndpointException(message, OUT_MESSAGE_FLOW, 500);
    }

    public static EndpointException stepUpNotSupported() {
        return new EndpointException(OUT_STEP_UP_NOT_SUPPORTED, OUT_STEP_UP_NOT_SUPPORTED, 400);
    }

    public static EndpointException couldNotParseInput(Exception e) {
        final var message = "Could not parse input " + e.getMessage();
        return new EndpointException(message, e, OUT_PARSE_INPUT, 400);
    }

    static EndpointException couldNotParseSessionId(String sessionCookie, Exception e) {
        final var message = "Session Cookie " + sessionCookie + " is invalid as it is not a UUID: " + e.getMessage();
        return new EndpointException(message, e, OUT_INVALID_COOKIE, 400);
    }

    public static EndpointException wrap(Exception e) {
        if (e instanceof EndpointException) {
            return (EndpointException) e;
        }
        if (e instanceof EngineException) {
            final var exception = (EngineException) e;
            return new EndpointException(e.getMessage(), e, exception.getOutputMessage(), exception.getStatusCode());
        }
        return new EndpointException(e.getMessage(), e, OUT_GENERAL, 500);
    }

}

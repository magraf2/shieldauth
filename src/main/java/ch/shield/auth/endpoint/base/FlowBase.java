package ch.shield.auth.endpoint.base;

import ch.shield.auth.core.config.Flow;
import ch.shield.auth.core.config.FlowInitialization;
import lombok.Getter;

public class FlowBase implements Flow {
    @Getter
    private String name;

    @Override
    public void initialize(final FlowInitialization flowInitialization) {
        name = flowInitialization.getName();
    }
}

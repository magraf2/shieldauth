package ch.shield.auth.endpoint.base;

import org.springframework.http.HttpHeaders;
import org.springframework.mock.web.MockCookie;
import org.springframework.util.StringUtils;

import javax.servlet.http.Cookie;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Utils {

    private Utils() {

    }

    public static UUID parseUUID(String uuidAsString) {
        try {
            return UUID.fromString(uuidAsString);
        } catch (Exception e) {
            throw EndpointException.couldNotParseSessionId(uuidAsString, e);
        }

    }

    public static String getCookieHeader(Cookie cookie) {
        StringBuilder buf = new StringBuilder();
        buf.append(cookie.getName()).append('=').append(cookie.getValue() == null ? "" : cookie.getValue());
        if (StringUtils.hasText(cookie.getPath())) {
            buf.append("; Path=").append(cookie.getPath());
        }

        if (StringUtils.hasText(cookie.getDomain())) {
            buf.append("; Domain=").append(cookie.getDomain());
        }

        int maxAge = cookie.getMaxAge();
        if (maxAge >= 0) {
            buf.append("; Max-Age=").append(maxAge);
            buf.append("; Expires=");
            HttpHeaders headers = new HttpHeaders();
            headers.setExpires(maxAge > 0 ? System.currentTimeMillis() + 1000L * (long)maxAge : 0L);
            buf.append(headers.getFirst("Expires"));
        }

        if (cookie.getSecure()) {
            buf.append("; Secure");
        }

        if (cookie.isHttpOnly()) {
            buf.append("; HttpOnly");
        }

        if (cookie instanceof MockCookie) {
            MockCookie mockCookie = (MockCookie)cookie;
            if (StringUtils.hasText(mockCookie.getSameSite())) {
                buf.append("; SameSite=").append(mockCookie.getSameSite());
            }
        }

        return buf.toString();
    }


    public static <K, F, T>
    Collector<Map.Entry<K, F>, ?, Map<K, T>> toMap(Function<? super F, ? extends T> valueMapper) {
        return Collectors.toMap(Map.Entry::getKey, x -> valueMapper.apply(x.getValue()));
    }

}

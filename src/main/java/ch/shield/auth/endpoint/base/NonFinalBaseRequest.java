package ch.shield.auth.endpoint.base;

import ch.shield.auth.core.engine.EngineRequest;
import ch.shield.auth.core.engine.SessionAttributes;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Locale;
import java.util.Map;
import java.util.UUID;

@Getter
@Builder
public class NonFinalBaseRequest implements EngineRequest {
    private final UUID id = UUID.randomUUID();
    @NonNull
    private final UUID sessionId;
    @NonNull
    private final Map<String, String[]> headers;
    @NonNull
    private final Map<String, String[]> inputs;
    @NonNull
    private final SessionAttributes sessionAttributes;
    @NonNull
    private final UriComponentsBuilder requestUri;
    @NonNull
    private final Locale locale;
}
package ch.shield.auth.endpoint.json;

import ch.shield.auth.core.LoggerConstants;
import ch.shield.auth.core.Renderable;
import ch.shield.auth.core.engine.Engine;
import ch.shield.auth.core.engine.SessionAttributes;
import ch.shield.auth.endpoint.base.EndpointException;
import ch.shield.auth.endpoint.base.NonFinalBaseRequest;
import ch.shield.auth.endpoint.base.Utils;
import ch.shield.auth.endpoint.json.dto.JsonGuiResponse;
import ch.shield.auth.endpoint.json.dto.JsonRedirectResponse;
import com.google.common.base.Strings;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.constraints.NotNull;
import java.net.URI;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;


@Controller
@RequestMapping("/authenticate")
public class JsonEndpoint {
    private final static Logger LOG = LoggerFactory.getLogger(LoggerConstants.LOGGER_JSON);
    private final Engine engine;

    public JsonEndpoint(@Autowired final Engine engine) {
        this.engine = engine;
    }

    private static Map<String, String[]> parseJsonBody(String jsonString) {
        final var ret = new HashMap<String, String[]>();
        if (Strings.isNullOrEmpty(jsonString))
            return ret;
        try {
            final var jsonObject = new JSONObject(jsonString);
            final var names = jsonObject.names();
            for (var i = 0; i < jsonObject.length(); i++) {
                final var name = names.getString(i);
                final var value = jsonObject.getString(name);
                ret.put(name, new String[]{value});
            }
            return ret;
        } catch (JSONException e) {
            throw EndpointException.couldNotParseInput(e);
        }
    }

    private static URI getFinalEndpoint(final NonFinalBaseRequest request) {
        final var finalEndpoint = request.getSessionAttributes().getFinalEndpoint();
        if (finalEndpoint.isAbsolute()) {
            return finalEndpoint;
        }
        return request.getRequestUri().replacePath(finalEndpoint.getPath()).build().toUri();
    }

    private static Map<String, String[]> convertMultiValueMap(MultiValueMap<String, String> headers) {
        return headers.entrySet().stream().collect(Utils.toMap(x -> x.toArray(new String[0])));
    }

    private static ResponseEntity<String> handleFinished(final NonFinalBaseRequest request) {
        final var finalEndpoint = getFinalEndpoint(request);
        final var response = JsonRedirectResponse.builder().target(finalEndpoint).build();
        LOG.info("Authentication finished therefore redirect to " + response.getTarget());
        return response.write();
    }

    private static ResponseEntity<String> handleSessionException(final NonFinalBaseRequest request, final Exception e) {
        final var finalEndpoint = getFinalEndpoint(request);
        final var response = JsonRedirectResponse.builder().target(finalEndpoint).build();
        LOG.info("Exception therefore redirect to " + response.getTarget(), e);
        return response.write();
    }

    private static ResponseEntity<String> handleNoSessionException(final Exception e) {
        final var endpointException = EndpointException.wrap(e);
        final var renderable = endpointException.getRenderable();
        LOG.info("Exception and no session therefore show gui " + renderable.getName(), e);
        return handleRenderable(renderable);
    }

    private static ResponseEntity<String> handleRenderable(final Renderable renderable) {
        final var response = JsonGuiResponse.builder().renderable(renderable).build();
        LOG.info("Show gui " + renderable.getName());
        return response.write();
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> post(@RequestBody final String requestBody,
                                       @RequestHeader final MultiValueMap<String, String> headers,
                                       @NotNull @CookieValue("login") final String sessionCookie,
                                       final Locale locale, final UriComponentsBuilder requestUri) {
        try {
            final var sessionId = Utils.parseUUID(sessionCookie);
            final var sessionAttributes = getSessionAttributes(sessionId);
            final var request = NonFinalBaseRequest.builder()
                    .sessionId(sessionId)
                    .sessionAttributes(sessionAttributes)
                    .headers(convertMultiValueMap(headers))
                    .requestUri(requestUri)
                    .inputs(parseJsonBody(requestBody))
                    .locale(locale)
                    .build();
            return authenticate(request);
        } catch (Exception e) {
            return handleNoSessionException(e);
        }
    }

    @GetMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> get(@RequestHeader final MultiValueMap<String, String> headers,
                                      @NotNull @CookieValue("login") final String sessionCookie,
                                      final Locale locale, final UriComponentsBuilder requestUri) {
        try {
            final var sessionId = Utils.parseUUID(sessionCookie);
            final var sessionAttributes = getSessionAttributes(sessionId);
            final var request = NonFinalBaseRequest.builder()
                    .sessionId(sessionId)
                    .sessionAttributes(sessionAttributes)
                    .headers(convertMultiValueMap(headers))
                    .requestUri(requestUri)
                    .inputs(Map.of())
                    .locale(locale)
                    .build();
            return authenticate(request);
        } catch (Exception e) {
            return handleNoSessionException(e);
        }
    }

    private ResponseEntity<String> authenticate(final NonFinalBaseRequest request) {
        try {
            MDC.put(LoggerConstants.REQUEST_ID, request.getId().toString());
            MDC.put(LoggerConstants.SESSION_ID, request.getSessionId().toString());
            LOG.info("Start request");
            return handleRequest(request);
        } catch (Exception e) {
            return handleSessionException(request, e);
        }
    }

    private SessionAttributes getSessionAttributes(UUID sessionId) {
        return engine.getAttributes(sessionId, SessionAttributes.class);
    }

    private ResponseEntity<String> handleRequest(final NonFinalBaseRequest request) {
        final var engineResult = engine.request(request);
        if (engineResult.isFinished()) {
            return handleFinished(request);
        }
        final var renderable = engineResult.getRenderable().orElseThrow(EndpointException::unknownEngineResult);
        return handleRenderable(renderable);
    }
}
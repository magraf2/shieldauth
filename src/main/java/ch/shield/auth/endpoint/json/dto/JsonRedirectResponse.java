package ch.shield.auth.endpoint.json.dto;

import ch.shield.auth.core.LoggerConstants;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URI;

@Builder
public class JsonRedirectResponse {
    private final static Logger LOG = LoggerFactory.getLogger(LoggerConstants.LOGGER_JSON);
    @Getter
    @NonNull
    private final URI target;

    public ResponseEntity<String> write() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Location", target.toString());
        return new ResponseEntity<>(null, headers, HttpStatus.FOUND);
    }
}

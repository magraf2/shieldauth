package ch.shield.auth.endpoint.json.dto;

import ch.shield.auth.core.LoggerConstants;
import ch.shield.auth.core.Renderable;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

@Builder
public class JsonGuiResponse {
    private final static Logger LOG = LoggerFactory.getLogger(LoggerConstants.LOGGER_JSON);
    @Getter
    @NonNull
    private final Renderable renderable;


    public ResponseEntity<String> write() {
        final var body = getBody();
        final var headers = getHeaders();
        final var statusCode = getStatusCode();
        return new ResponseEntity<>(body, headers, statusCode);
    }

    private String getBody() {
        return new JSONObject(renderable.getAttributes()).toString();
    }

    private HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        headers.add("Pragma", "no-cache");
        headers.add("Cache-Control", "no-cache");
        headers.add("Expires", "0");
        renderable.getHeaders().forEach((name, values) -> values.forEach(value -> headers.add(name, value)));
        return headers;
    }

    private HttpStatus getStatusCode() {
        final var statusCode = renderable.getStatusCode();
        return HttpStatus.resolve(statusCode);
    }
}

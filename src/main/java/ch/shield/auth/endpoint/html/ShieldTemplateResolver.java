package ch.shield.auth.endpoint.html;

import lombok.Getter;
import org.thymeleaf.IEngineConfiguration;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.FileTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;
import org.thymeleaf.templateresolver.TemplateResolution;

import java.util.Map;

class ShieldTemplateResolver implements ITemplateResolver {
    @Getter
    private final String name = "ShieldTemplateResolver";
    @Getter
    private final Integer order = null;
    private final FileTemplateResolver fileResolver;
    private final ClassLoaderTemplateResolver classLoaderResolver;

    ShieldTemplateResolver(final String diskPrefix) {
        fileResolver = new FileTemplateResolver();
        fileResolver.setPrefix(diskPrefix);
        fileResolver.setSuffix(".html");
        fileResolver.setTemplateMode(TemplateMode.HTML);

        classLoaderResolver = new ClassLoaderTemplateResolver();
        classLoaderResolver.setPrefix("templates/");
        classLoaderResolver.setSuffix(".html");
        classLoaderResolver.setTemplateMode(TemplateMode.HTML);
    }

    @Override
    public TemplateResolution resolveTemplate(IEngineConfiguration configuration, String ownerTemplate, String template, Map<String, Object> templateResolutionAttributes) {
        final var fileResolution = fileResolver.resolveTemplate(configuration, ownerTemplate, template, templateResolutionAttributes);
        if (fileResolution.getTemplateResource().exists()) {
            return fileResolution;
        }
        final var classLoaderResolution = classLoaderResolver.resolveTemplate(configuration, ownerTemplate, template, templateResolutionAttributes);
        if (classLoaderResolution.getTemplateResource().exists()) {
            return classLoaderResolution;
        }
        return classLoaderResolver.resolveTemplate(configuration, ownerTemplate, "default", templateResolutionAttributes);
    }
}

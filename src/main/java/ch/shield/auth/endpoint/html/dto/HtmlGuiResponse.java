package ch.shield.auth.endpoint.html.dto;

import ch.shield.auth.core.LoggerConstants;
import ch.shield.auth.core.Renderable;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.Locale;

@Builder
public class HtmlGuiResponse {
    private final static Logger LOG = LoggerFactory.getLogger(LoggerConstants.LOGGER_HTML);
    @Getter
    @NonNull
    private final Renderable renderable;
    @NonNull
    private final TemplateEngine templateEngine;
    @NonNull
    private final Locale locale;

    public ResponseEntity<String> write() {
        final var body = getBody();
        final var headers = getHeaders();
        final var statusCode = getStatusCode();
        return new ResponseEntity<>(body, headers, statusCode);
    }

    private String getBody() {
        final var ctx = new Context(locale, renderable.getAttributes());
        return templateEngine.process(renderable.getName(), ctx);
    }

    private HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.TEXT_HTML);
        headers.add("Pragma", "no-cache");
        headers.add("Cache-Control", "no-cache");
        headers.add("Expires", "0");
        renderable.getHeaders().forEach((name, values) -> values.forEach(value -> headers.add(name, value)));
        return headers;
    }

    private HttpStatus getStatusCode() {
        final var statusCode = renderable.getStatusCode();
        return HttpStatus.resolve(statusCode);
    }
}

package ch.shield.auth.endpoint.html;

import ch.shield.auth.core.LoggerConstants;
import ch.shield.auth.core.Renderable;
import ch.shield.auth.core.engine.Engine;
import ch.shield.auth.core.engine.SessionAttributes;
import ch.shield.auth.endpoint.base.EndpointException;
import ch.shield.auth.endpoint.base.NonFinalBaseRequest;
import ch.shield.auth.endpoint.base.Utils;
import ch.shield.auth.endpoint.html.dto.HtmlGuiResponse;
import ch.shield.auth.endpoint.html.dto.HtmlRedirectResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.validation.constraints.NotNull;
import java.net.URI;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

@Controller
@RequestMapping("/authenticate")
public class HtmlEndpoint {
    private final static Logger LOG = LoggerFactory.getLogger(LoggerConstants.LOGGER_HTML);
    private final Engine engine;
    private final TemplateEngine templateEngine;

    public HtmlEndpoint(@Autowired final Engine engine, @Value("${endpoint.html.templatePath:/var/opt/shieldAuth/template/}") String diskPath) {
        this.engine = engine;
        final var templateResolver = new ShieldTemplateResolver(diskPath);
        this.templateEngine = new SpringTemplateEngine();
        this.templateEngine.setTemplateResolver(templateResolver);
    }

    private static Map<String, String[]> convertMultiValueMap(MultiValueMap<String, String> headers) {
        return headers.entrySet().stream().collect(Utils.toMap(x -> x.toArray(new String[0])));
    }

    private static ResponseEntity<String> handleFinished(final NonFinalBaseRequest request) {
        final var finalEndpoint = getFinalEndpoint(request);
        final var response = HtmlRedirectResponse.builder().target(finalEndpoint).build();
        LOG.info("Authentication finished therefore redirect to " + response.getTarget());
        return response.write();
    }

    private static URI getFinalEndpoint(final NonFinalBaseRequest request) {
        final var finalEndpoint = request.getSessionAttributes().getFinalEndpoint();
        if (finalEndpoint.isAbsolute()) {
            return finalEndpoint;
        }
        return request.getRequestUri().replacePath(finalEndpoint.getPath()).build().toUri();
    }

    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.TEXT_HTML_VALUE)
    public ResponseEntity<String> post(@RequestBody final MultiValueMap<String, String> requestBody,
                                       @RequestHeader final MultiValueMap<String, String> headers,
                                       @NotNull @CookieValue("login") final String sessionCookie,
                                       final Locale locale, final UriComponentsBuilder requestUri) {
        try {
            final var sessionId = Utils.parseUUID(sessionCookie);
            final var sessionAttributes = getSessionAttributes(sessionId);
            final var request = NonFinalBaseRequest.builder()
                    .sessionId(sessionId)
                    .sessionAttributes(sessionAttributes)
                    .headers(convertMultiValueMap(headers))
                    .requestUri(requestUri)
                    .inputs(convertMultiValueMap(requestBody))
                    .locale(locale)
                    .build();
            return authenticate(request);
        } catch (Exception e) {
            return handleNoSessionException(locale, e);
        }
    }

    @GetMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.TEXT_HTML_VALUE)
    public ResponseEntity<String> get(@RequestHeader final MultiValueMap<String, String> headers,
                                      @NotNull @CookieValue("login") final String sessionCookie,
                                      final Locale locale, final UriComponentsBuilder requestUri) {
        try {
            final var sessionId = Utils.parseUUID(sessionCookie);
            final var sessionAttributes = getSessionAttributes(sessionId);
            final var request = NonFinalBaseRequest.builder()
                    .sessionId(sessionId)
                    .sessionAttributes(sessionAttributes)
                    .headers(convertMultiValueMap(headers))
                    .requestUri(requestUri)
                    .inputs(Map.of())
                    .locale(locale)
                    .build();
            return authenticate(request);
        } catch (Exception e) {
            return handleNoSessionException(locale, e);
        }
    }

    private ResponseEntity<String> handleRenderable(final Renderable renderable, final Locale locale) {
        final var response = HtmlGuiResponse.builder()
                .templateEngine(templateEngine)
                .renderable(renderable)
                .locale(locale)
                .build();
        LOG.info("Show gui " + renderable.getName());
        return response.write();
    }

    private ResponseEntity<String> authenticate(final NonFinalBaseRequest request) {
        try {
            MDC.put(LoggerConstants.REQUEST_ID, request.getId().toString());
            MDC.put(LoggerConstants.SESSION_ID, request.getSessionId().toString());
            LOG.info("Start request");
            return handleRequest(request);
        } catch (Exception e) {
            return handleSessionException(request, e);
        }
    }

    private SessionAttributes getSessionAttributes(UUID sessionId) {
        return engine.getAttributes(sessionId, SessionAttributes.class);
    }

    private ResponseEntity<String> handleRequest(final NonFinalBaseRequest request) {
        final var engineResult = engine.request(request);
        if (engineResult.isFinished()) {
            return handleFinished(request);
        }
        final var renderable = engineResult.getRenderable().orElseThrow(EndpointException::unknownEngineResult);
        return handleRenderable(renderable, request.getLocale());
    }

    private ResponseEntity<String> handleSessionException(final NonFinalBaseRequest request, final Exception e) {
        final var finalEndpoint = getFinalEndpoint(request);
        final var response = HtmlRedirectResponse.builder().target(finalEndpoint).build();
        LOG.info("Exception therefore redirect to " + response.getTarget(), e);
        return response.write();
    }

    private ResponseEntity<String> handleNoSessionException(final Locale locale, final Exception e) {
        final var endpointException = EndpointException.wrap(e);
        final var renderable = endpointException.getRenderable();
        LOG.info("Exception and no session therefore show gui " + renderable.getName(), e);
        return handleRenderable(renderable, locale);
    }
}
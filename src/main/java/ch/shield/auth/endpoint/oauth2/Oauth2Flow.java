package ch.shield.auth.endpoint.oauth2;

import ch.shield.auth.config.FlowInitializeException;
import ch.shield.auth.core.config.FlowInitialization;
import ch.shield.auth.core.engine.EngineResult;
import ch.shield.auth.endpoint.base.EndpointException;
import ch.shield.auth.endpoint.base.FlowBase;
import ch.shield.auth.endpoint.oauth2.dto.ResponseType;
import lombok.Getter;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.*;
import java.util.stream.Collectors;

public class Oauth2Flow extends FlowBase {
    private final static String PROPERTY_SCOPE = "scope";
    private final static String PROPERTY_AUDIENCES = "audience";
    private final static String PROPERTY_TYPES = "types";
    private final static String PROPERTY_ISSUER = "issuer";
    private final static String PROPERTY_EXPIRES_SECONDS = "expiresSeconds";
    private final static String PROPERTY_TOKEN_KEY_BASE64 = "tokenKeyBase64";

    private final static String TYPES_DEFAULT = "code";
    private final static String EXPIRES_SECONDS_DEFAULT = "7200";
    private final static String PROPERTY_CLIENT_PREFIX = "client.";

    private String scopeSessionAttribute;
    private String audienceSessionAttribute;
    private List<ResponseType> types;
    private Map<String, String> clientIdSecrets;
    private String issuer;
    @Getter
    private RSAPublicKey key;
    @Getter
    private long expiresSeconds;

    private static List<String> toStringList(Object obj, String property, String attribute) {
        if (!(obj instanceof List)) {
            throw EndpointException.missingSessionAttribute(property, attribute);
        }
        final var list = (List) obj;
        final var ret = new ArrayList<String>(list.size());
        for (Object o : list) {
            ret.add(o.toString());
        }
        return ret;
    }

    private static RSAPublicKey getKeyFromProperties(Properties properties) {
        final var tokenKeyBase64 = properties.getProperty(PROPERTY_TOKEN_KEY_BASE64, null);
        if (tokenKeyBase64 == null) {
            throw FlowInitializeException.missingProperty(Oauth2Flow.class, PROPERTY_TOKEN_KEY_BASE64);
        }
        try {
            final var keyFactory = KeyFactory.getInstance("RSA");
            final var tokenKeyByte = Base64.getDecoder().decode(tokenKeyBase64);
            final var privateKeySpec = new X509EncodedKeySpec(tokenKeyByte);
            return (RSAPublicKey) keyFactory.generatePublic(privateKeySpec);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw FlowInitializeException.invalidProperty(PROPERTY_TOKEN_KEY_BASE64, "Invalid key exception: " + e.getMessage());
        }
    }

    @Override
    public void initialize(FlowInitialization flowInitialization) {
        super.initialize(flowInitialization);
        final var properties = flowInitialization.getProperties();
        scopeSessionAttribute = properties.getProperty(PROPERTY_SCOPE, PROPERTY_SCOPE);
        audienceSessionAttribute = properties.getProperty(PROPERTY_AUDIENCES, PROPERTY_AUDIENCES);
        types = Arrays.stream(properties.getProperty(PROPERTY_TYPES, TYPES_DEFAULT).split(","))
                .map(ResponseType::parse)
                .filter(x -> !x.equals(ResponseType.UNKNOWN))
                .collect(Collectors.toList());
        issuer = properties.getProperty(PROPERTY_ISSUER, null);
        key = getKeyFromProperties(properties);
        expiresSeconds = Long.parseLong(properties.getProperty(PROPERTY_EXPIRES_SECONDS, EXPIRES_SECONDS_DEFAULT));
        clientIdSecrets = properties.stringPropertyNames().stream()
                .filter(x -> x.startsWith(PROPERTY_CLIENT_PREFIX))
                .collect(Collectors.toMap(x -> x.substring(PROPERTY_CLIENT_PREFIX.length()), properties::getProperty));
    }

    public boolean isValid(String clientId, ResponseType type) {
        return types.contains(type) && clientIdSecrets.containsKey(clientId);
    }

    boolean isAllowed(String clientId, String clientSecret) {
        return clientIdSecrets.containsKey(clientId) && clientIdSecrets.get(clientId).equals(clientSecret);
    }

    public Optional<String> getIssuer() {
        return Optional.ofNullable(issuer);
    }

    List<String> getScopes(EngineResult result, Oauth2Attributes attributes) {
        final var scopeObj = result.getSessionAttributes().get(scopeSessionAttribute);
        if (scopeObj != null) {
            return toStringList(scopeObj, PROPERTY_SCOPE, scopeSessionAttribute);
        }

        //Scope attribute is not set, try endpoint attribute
        final var endpointScope = attributes.getScopes();
        if (endpointScope != null) {
            return endpointScope;
        }
        return List.of();
    }

    List<String> getAudiences(EngineResult result) {
        final var audienceObj = result.getSessionAttributes().get(audienceSessionAttribute);
        if (audienceObj != null) {
            return toStringList(audienceObj, PROPERTY_AUDIENCES, audienceSessionAttribute);
        }
        return List.of();
    }
}

package ch.shield.auth.endpoint.oauth2.dto;


import ch.shield.auth.endpoint.oauth2.Oauth2Flow;
import ch.shield.auth.endpoint.oauth2.Oauth2Token;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.NonNull;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class AccessTokenResponse {
    @JsonProperty("access_token")
    private final String accessToken;
    @JsonProperty("token_type")
    private final String tokenType;
    @JsonProperty("refresh_token")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private final String refreshToken;
    @JsonProperty("expires_in")
    private final long expiresIn;
    @JsonProperty("scope")
    private final String scope;

    @Builder
    private AccessTokenResponse(@NonNull final Oauth2Token oauth2Token, @NonNull final Oauth2Flow flow) {
        this.accessToken = oauth2Token.computeAccessToken(flow.getIssuer().orElse(null), flow.getKey());
        //TODO OAUTH2: Support other oauth2Token types
        this.tokenType = "bearer";
        this.expiresIn = LocalDateTime.now().until(oauth2Token.getExpiresAt(), ChronoUnit.SECONDS);
        //TODO OAUTH2: Support refresh oauth2Token
        this.refreshToken = null;
        this.scope = String.join(" ", oauth2Token.getScopes());
    }
}

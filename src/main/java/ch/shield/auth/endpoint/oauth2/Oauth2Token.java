package ch.shield.auth.endpoint.oauth2;

import ch.shield.auth.core.repository.Property;
import org.springframework.lang.Nullable;

import java.net.URI;
import java.security.interfaces.RSAPublicKey;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public interface Oauth2Token extends Property {
    URI getRedirectUri();

    String getClientId();

    LocalDateTime getExpiresAt();

    List<String> getScopes();

    UUID getCode();

    String computeAccessToken(@Nullable String issuer, RSAPublicKey key);

    void update(TokenData tokenData);
}

package ch.shield.auth.endpoint.oauth2.dto;

@SuppressWarnings("unused")
public enum Error {
    INVALID_REQUEST, UNAUTHORIZED_CLIENT, ACCESS_DENIED, UNSUPPORTED_RESPONSE_TYPE, INVALID_SCOPE, SERVER_ERROR, TEMPORARY_UNAVAILABLE, INVALID_CLIENT, INVALID_GRANT, UNSUPPORTED_GRANT_TYPE;

    public String toString() {
        return super.toString().toLowerCase();
    }
}
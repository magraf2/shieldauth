package ch.shield.auth.endpoint.oauth2;

import ch.shield.auth.core.engine.SessionAttributes;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

import java.net.URI;
import java.util.List;

@Builder
@Getter
public class Oauth2Attributes implements SessionAttributes {
    private final String state;
    private final URI redirectUri;
    @NonNull
    private final Oauth2Flow flow;
    @NonNull
    private final String clientId;
    private final List<String> scopes;
    @NonNull
    private final URI finalEndpoint;
}

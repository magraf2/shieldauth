package ch.shield.auth.endpoint.oauth2.dto;

import ch.shield.auth.endpoint.oauth2.Oauth2Exception;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Getter
public class AuthorizationRequest {
    private final String id;
    private final ResponseType responseType;
    private final String clientId;
    private final URI redirectUri;
    private final List<String> scopes;
    private final String state;
    private final UriComponentsBuilder requestUri;

    @Setter
    private UUID sessionId;

    public AuthorizationRequest(Map<String, String> params, UUID sessionId, UriComponentsBuilder requestUri) {
        this.id = UUID.randomUUID().toString();
        this.sessionId = sessionId;
        this.responseType = ResponseType.parse(params.get("response_type"));
        this.clientId = params.get("client_id");
        this.redirectUri = parseUri(params.get("redirect_uri"));
        this.state = params.get("state");
        this.requestUri = requestUri;
        final var inputScopes = params.get("scope");
        this.scopes = inputScopes == null ? List.of() : List.of(inputScopes.split(" "));
    }

    private static URI parseUri(String uri) {
        try {
            return new URI(uri);
        } catch (Exception e) {
            throw Oauth2Exception.couldNotParseUri(uri, e);
        }
    }
}
package ch.shield.auth.endpoint.oauth2.dto;

@SuppressWarnings("unused")
public enum ResponseType {
    CODE, TOKEN, UNKNOWN;

    public static ResponseType parse(String grantType) {
        try {
            return ResponseType.valueOf(grantType.toUpperCase());
        } catch (IllegalArgumentException e) {
            return UNKNOWN;
        }
    }

    public Object out() {
        return toString().toLowerCase();
    }
}

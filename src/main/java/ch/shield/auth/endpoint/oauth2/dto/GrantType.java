package ch.shield.auth.endpoint.oauth2.dto;

@SuppressWarnings("unused")
public enum GrantType {
    AUTHORIZATION_CODE, PASSWORD, CLIENT_CREDENTIALS, UNKNOWN;

    public static GrantType parse(String grantType) {
        try {
            return GrantType.valueOf(grantType.toUpperCase());
        } catch (IllegalArgumentException e) {
            return UNKNOWN;
        }
    }

    public Object out() {
        return toString().toLowerCase();
    }
}

package ch.shield.auth.endpoint.oauth2;

import lombok.Builder;
import lombok.Data;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Data
@Builder
public class TokenData {
    private final URI redirectUri;
    private final String clientId;
    private final LocalDateTime expiresAt;
    private final List<String> wantedScopes;
    private final List<String> audiences;
    private final UUID code;
}

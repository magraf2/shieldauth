package ch.shield.auth.endpoint.oauth2.dto;

import ch.shield.auth.endpoint.oauth2.Oauth2Exception;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import java.net.URI;

@Getter
public class NoRedirectErrorResponse {
    @JsonProperty("error")
    private final Error error;
    @JsonProperty("error_description")
    private final String errorDescription;
    @JsonProperty("error_uri")
    private final URI errorUri;

    @Builder
    private NoRedirectErrorResponse(@NonNull final Oauth2Exception exception) {
        this.error = exception.getError();
        this.errorDescription = exception.getOutputMessage();
        this.errorUri = exception.getErrorUri();
    }
}
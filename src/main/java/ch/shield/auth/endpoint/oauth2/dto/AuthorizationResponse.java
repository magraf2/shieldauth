package ch.shield.auth.endpoint.oauth2.dto;


import ch.shield.auth.endpoint.oauth2.Oauth2Attributes;
import lombok.Builder;
import lombok.NonNull;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

public class AuthorizationResponse {
    private final TokenInput tokenInput;
    private final String state;
    private final URI redirectUri;

    @Builder
    private AuthorizationResponse(@NonNull final TokenInput tokenInput, @NonNull final Oauth2Attributes attributes) {
        this.state = attributes.getState();
        this.redirectUri = attributes.getRedirectUri();
        this.tokenInput = tokenInput;
    }

    public ResponseEntity<NoRedirectErrorResponse> write() {
        final var headers = getHeaders();
        return new ResponseEntity<>(null, headers, HttpStatus.FOUND);
    }

    private HttpHeaders getHeaders() {
        final var redirectUrlBuilder = UriComponentsBuilder
                .fromUri(redirectUri)
                .queryParam("code", tokenInput.toString());
        if (state != null) {
            redirectUrlBuilder.queryParam("state", state);
        }
        final var redirectUrl = redirectUrlBuilder.build().toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(redirectUrl);
        return headers;
    }
}

package ch.shield.auth.endpoint.oauth2;

import ch.shield.auth.core.LoggerConstants;
import ch.shield.auth.core.config.ConfigFile;
import ch.shield.auth.core.engine.Engine;
import ch.shield.auth.core.engine.EngineException;
import ch.shield.auth.core.engine.EngineResult;
import ch.shield.auth.core.engine.SessionAttributes;
import ch.shield.auth.endpoint.base.EndpointException;
import ch.shield.auth.endpoint.oauth2.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.UUID;


@Controller
@RequestMapping("/oauth2")
public class Oauth2Endpoint {
    private final static Logger LOG = LoggerFactory.getLogger(LoggerConstants.LOGGER_OAUTH2);
    private final Engine engine;
    private final ConfigFile configFile;
    private final String contextPath;

    public Oauth2Endpoint(@Autowired final Engine engine, @Autowired final ConfigFile configFile, @Value("#{servletContext.contextPath}") String contextPath) {
        this.engine = engine;
        this.configFile = configFile;
        this.contextPath = contextPath;
    }

    @RequestMapping(value = "/authenticate", produces = MediaType.APPLICATION_JSON_VALUE)
    public final ResponseEntity<NoRedirectErrorResponse> authenticate(@RequestParam final Map<String,String> params,
                                                                      @CookieValue(value = "login", required = false) UUID incomingSessionId,
                                                                      UriComponentsBuilder requestUri) {
        try {
            final var request = new AuthorizationRequest(params, incomingSessionId, requestUri);
            MDC.put(LoggerConstants.REQUEST_ID, request.getId());
            LOG.info("Start authentication request");
            return handleRequest(request);
        } catch (Exception e) {
            return handleNoSessionException(e);
        }
    }

    private ResponseEntity<NoRedirectErrorResponse> handleRequest(final AuthorizationRequest request) {
        try {
            prepareSession(request);
            switch (request.getResponseType()) {
                case CODE:
                    return handleCodeRequest(request);
                default:
                    //TODO OAUTH2: Support Implicit Flow
                    throw Oauth2Exception.unsupportedResponseType(request.getResponseType());
            }
        } catch (Exception e) {
            return handleAuthenticationException(request, e);
        }
    }

    private void prepareSession(final AuthorizationRequest request) {
        //FIXME GENERAL: Support step ups
        if (request.getSessionId() != null) {
            if (engine.getAttributes(request.getSessionId(), SessionAttributes.class) != null) {
                throw EndpointException.stepUpNotSupported();
            }
            LOG.info("Got an outdated session, ignore it");
        }

        final var flow = configFile.getFlows(Oauth2Flow.class).stream()
                .filter(x -> x.isValid(request.getClientId(), request.getResponseType()))
                .findFirst()
                .orElseThrow(() -> Oauth2Exception.clientIdNotValidForFlow(request.getClientId(), request.getResponseType()));
        final var finalUri = request.getRequestUri().replacePath(contextPath + "/oauth2/final").build().toUri();
        final var attributes = Oauth2Attributes.builder()
                .flow(flow)
                .clientId(request.getClientId())
                .redirectUri(request.getRedirectUri())
                .state(request.getState())
                .finalEndpoint(finalUri)
                .scopes(request.getScopes())
                .build();
        final var sessionId = engine.init(attributes);
        MDC.put(LoggerConstants.SESSION_ID, sessionId.toString());
        request.setSessionId(sessionId);
    }

    private ResponseEntity<NoRedirectErrorResponse> handleCodeRequest(final AuthorizationRequest request) {
        final var loginUrl = request.getRequestUri().replacePath(contextPath + "/authenticate").build().toUri();
        final var response = InitialResponse.builder()
                .sessionId(request.getSessionId())
                .loginUri(loginUrl)
                .cookiePath(contextPath)
                .build();
        LOG.info("Redirect to login endpoint " + response.getLoginUri());
        return response.write();
    }

    private static ResponseEntity<NoRedirectErrorResponse> handleAuthenticationException(final AuthorizationRequest request, final Exception e) {
        final var oauth2Exception = Oauth2Exception.wrap(e);
        final var response = AuthorizationErrorResponse.builder()
                .exception(oauth2Exception)
                .redirectUri(request.getRedirectUri())
                .state(request.getState())
                .build();
        LOG.info("Exception therefore redirect to " + response.getRedirectUri(), e);
        return response.write();
    }

    private static ResponseEntity<NoRedirectErrorResponse> handleNoSessionException(final Exception exception) {
        final var oauth2Exception = Oauth2Exception.wrap(exception);
        final var response = NoRedirectErrorResponse.builder().exception(oauth2Exception).build();
        LOG.info("Exception and no session. Send error message", exception);
        return new ResponseEntity<>(response, null, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping("/final")
    public final ResponseEntity<NoRedirectErrorResponse> finalize(@CookieValue(value = "login") UUID sessionId) {
        try {
            final var request = new FinalizeRequest(sessionId);
            MDC.put(LoggerConstants.REQUEST_ID, request.getId());
            MDC.put(LoggerConstants.SESSION_ID, sessionId.toString());
            LOG.info("Start final request");
            return handleFinalizeRequest(request);
        } catch (Exception e) {
            return handleNoSessionException(e);
        }
    }

    private ResponseEntity<NoRedirectErrorResponse> handleFinalizeRequest(final FinalizeRequest request) {
        try {
            final var engineResult = engine.check(request.getSessionId());
            if (engineResult.isFinished()) {
                return handleFinished(engineResult, request);
            }
            //The session is not finished and has no exception
            throw Oauth2Exception.notFinished();
        } catch (EngineException | EndpointException | Oauth2Exception e) {
            return handleFinalizeException(request, e);
        }
    }

    private ResponseEntity<NoRedirectErrorResponse> handleFinished(final EngineResult engineResult, final FinalizeRequest request) {
        final var oauth2Attributes = engine.getAttributes(request.getSessionId(), Oauth2Attributes.class);
        final var flow = oauth2Attributes.getFlow();
        final var token = generateToken(oauth2Attributes, engineResult);
        final var tokenInput = TokenInput.build(flow.getName(), token);
        final var response = AuthorizationResponse.builder().tokenInput(tokenInput).attributes(oauth2Attributes).build();
        LOG.info("Successful authentication. Generate authorization response");
        return response.write();
    }

    private static Oauth2Token generateToken(final Oauth2Attributes attributes, final EngineResult result) {
        final var flow = attributes.getFlow();
        final var user = result.getUser().orElseThrow(Oauth2Exception::noUserSet);
        final var token = user.createProperty(Oauth2Token.class);
        final var tokenData = TokenData.builder()
                .audiences(flow.getAudiences(result))
                .clientId(attributes.getClientId())
                .expiresAt(LocalDateTime.now().plusSeconds(flow.getExpiresSeconds()))
                .code(UUID.randomUUID())
                .redirectUri(attributes.getRedirectUri())
                .wantedScopes(flow.getScopes(result, attributes))
                .build();
        token.update(tokenData);
        return token;
    }

    private ResponseEntity<NoRedirectErrorResponse> handleFinalizeException(final FinalizeRequest request, final Exception e) {
        final var oauth2Exception = Oauth2Exception.wrap(e);
        final var oauth2Attributes = engine.getAttributes(request.getSessionId(), Oauth2Attributes.class);
        final var response = AuthorizationErrorResponse.builder()
                .exception(oauth2Exception)
                .redirectUri(oauth2Attributes.getRedirectUri())
                .state(oauth2Attributes.getState())
                .build();
        LOG.info("Exception therefore redirect to " + response.getRedirectUri(), e);
        return response.write();
    }

    @PostMapping(value = "/token", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public final ResponseEntity<?> token(@RequestParam final Map<String,String> params) {
        try {
            final var request = new AccessTokenRequest(params);
            MDC.put(LoggerConstants.REQUEST_ID, request.getId());
            LOG.info("Start Oauth2Token Request");
            return handleTokenRequest(request);
        } catch (Exception e) {
            return handleNoSessionException(e);
        }
    }

    private  ResponseEntity<AccessTokenResponse> handleTokenRequest(final AccessTokenRequest request) {
        switch (request.getGrantType()) {
            case AUTHORIZATION_CODE:
                return handleAuthorizationCodeRequest(request);
            default:
                //TODO OAUTH2: Support Password Flow
                //TODO OAUTH2: Support Client Credential Flow
                throw Oauth2Exception.unsupportedGrantType(request.getGrantType());
        }
    }

    private ResponseEntity<AccessTokenResponse> handleAuthorizationCodeRequest(final AccessTokenRequest request) {
        final var tokenInput = TokenInput.build(request.getCode());
        final var flowName = tokenInput.getFlowName();
        final var flow = configFile.getFlow(flowName, Oauth2Flow.class).orElseThrow(() -> Oauth2Exception.flowNotKnown(flowName));
        final var tokenId = tokenInput.getTokenId();
        final var repository = configFile.getRepository(flow);
        final var token = repository.getPropertyByValue(Oauth2Token.class, tokenId.toString()).stream()
                .findFirst().orElseThrow(() -> Oauth2Exception.tokenNotFound(tokenInput.toString()));
        checkAccessTokenSession(request, token, flow);
        final var response = AccessTokenResponse.builder().oauth2Token(token).flow(flow).build();
        LOG.info("Generated token response");
        return new ResponseEntity<>(response, null, HttpStatus.OK);
    }

    private void checkAccessTokenSession(AccessTokenRequest request, Oauth2Token oauth2Token, Oauth2Flow flow) {
        if (request.getRedirectUri() == null) {
            //TODO OAUTH2:  Support optional redirect URI
            throw Oauth2Exception.optionalRedirectUriNotSupported();
        }
        final var redirectUri = request.getRedirectUri();
        if (!redirectUri.equals(oauth2Token.getRedirectUri())) {
            throw Oauth2Exception.wrongRedirectUriForCode(redirectUri, oauth2Token.getRedirectUri());
        }
        if (!request.getClientId().equals(oauth2Token.getClientId())) {
            throw Oauth2Exception.wrongClientIdForCode(request.getClientId(), oauth2Token.getClientId());
        }
        if (request.getClientSecret() == null) {
            //TODO OAUTH2: Support other means of client authentication
            throw Oauth2Exception.clientAuthenticationFailed();
        }
        if (!flow.isAllowed(request.getClientId(), request.getClientSecret())) {
            throw Oauth2Exception.clientAuthenticationFailed();
        }
        //TODO enable refresh oauth2Token: Do not delete oauth2Token here...
        oauth2Token.getUser().removeProperty(oauth2Token);
    }
}
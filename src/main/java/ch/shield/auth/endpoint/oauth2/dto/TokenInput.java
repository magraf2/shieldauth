package ch.shield.auth.endpoint.oauth2.dto;

import ch.shield.auth.endpoint.oauth2.Oauth2Exception;
import ch.shield.auth.endpoint.oauth2.Oauth2Token;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.Base64;
import java.util.UUID;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class TokenInput {
    @Getter
    private final String flowName;
    @Getter
    private final UUID tokenId;

    public static TokenInput build(final String flowName, final Oauth2Token oauth2Token) {
        return new TokenInput(flowName, oauth2Token.getCode());
    }

    public static TokenInput build(String token) {
        try {
            final var bytes = Base64.getUrlDecoder().decode(token);
            final var jsonString = new String(bytes);
            final var jsonArray = new JSONArray(jsonString);
            final var flowName = jsonArray.getString(0);
            final var tokenId = jsonArray.getString(1);
            final var tokenIdUUID = UUID.fromString(tokenId);
            return new TokenInput(flowName, tokenIdUUID);
        } catch (JSONException e) {
            throw Oauth2Exception.invalidToken(token, e);
        }
    }

    @Override
    public String toString() {
        final var mapper = new ObjectMapper();
        final var arrayNode = mapper.createArrayNode();
        arrayNode.add(flowName);
        arrayNode.add(tokenId.toString());
        final var jsonString = arrayNode.toString();
        return Base64.getUrlEncoder().encodeToString(jsonString.getBytes());
    }
}

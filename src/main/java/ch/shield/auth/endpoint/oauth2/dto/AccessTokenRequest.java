package ch.shield.auth.endpoint.oauth2.dto;

import ch.shield.auth.endpoint.oauth2.Oauth2Exception;
import lombok.Getter;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.UUID;

@Getter
public class AccessTokenRequest {
    private final String id;
    private final GrantType grantType;
    private final String code;
    private final URI redirectUri;
    private final String clientId;
    private final String clientSecret;

    public AccessTokenRequest(Map<String, String> params) {
        this.id = UUID.randomUUID().toString();
        this.grantType = GrantType.parse(params.get("grant_type"));
        this.clientId = params.get("client_id");
        this.clientSecret = params.get("client_secret");
        this.redirectUri = parseUri(params.get("redirect_uri"));
        this.code = params.get("code");
    }

    private static URI parseUri(String uri) {
        try {
            return new URI(uri);
        } catch (URISyntaxException e) {
            throw Oauth2Exception.couldNotParseUri(uri, e);
        }
    }
}

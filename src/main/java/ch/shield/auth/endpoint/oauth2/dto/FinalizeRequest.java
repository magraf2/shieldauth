package ch.shield.auth.endpoint.oauth2.dto;

import ch.shield.auth.core.engine.EngineRequest;
import lombok.Getter;

import java.util.Map;
import java.util.UUID;

@Getter
public class FinalizeRequest implements EngineRequest {
    private final String id;
    private final UUID sessionId;
    private final Map<String, String[]> inputs = Map.of();
    private final Map<String, String[]> headers;

    public FinalizeRequest(final UUID sessionId) {
        this.id = UUID.randomUUID().toString();
        this.sessionId = sessionId;
        this.headers = Map.of();
    }
}

package ch.shield.auth.endpoint.oauth2.dto;

import ch.shield.auth.core.LoggerConstants;
import ch.shield.auth.endpoint.base.Utils;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.Cookie;
import java.net.URI;
import java.util.UUID;

@Getter
@Builder
public class InitialResponse {
    private final static Logger LOG = LoggerFactory.getLogger(LoggerConstants.LOGGER_OAUTH2);
    @NonNull
    private final UUID sessionId;
    @NonNull
    private final URI loginUri;
    @NonNull
    private final String cookiePath;

    public ResponseEntity<NoRedirectErrorResponse> write() {
        final var headers = getHeaders();
        return new ResponseEntity<>(null, headers, HttpStatus.FOUND);
    }

    private HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(loginUri);
        final var cookie = new Cookie("login", sessionId.toString());
        cookie.setPath(cookiePath);
        cookie.setHttpOnly(true);
        headers.add(HttpHeaders.SET_COOKIE, Utils.getCookieHeader(cookie));
        return headers;
    }
}
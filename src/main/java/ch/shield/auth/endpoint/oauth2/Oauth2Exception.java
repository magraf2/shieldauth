package ch.shield.auth.endpoint.oauth2;

import ch.shield.auth.core.engine.EngineException;
import ch.shield.auth.endpoint.base.EndpointException;
import ch.shield.auth.endpoint.oauth2.dto.Error;
import ch.shield.auth.endpoint.oauth2.dto.GrantType;
import ch.shield.auth.endpoint.oauth2.dto.ResponseType;
import lombok.Getter;

import java.net.URI;

public class Oauth2Exception extends RuntimeException {
    private static final String OUT_GENERAL = "General Server Error";
    private static final String OUT_RESPONSE_TYPE = "Response type not supported";
    private static final String OUT_INVALID_CODE = "Invalid Code Received";
    private static final String OUT_NO_REDIRECT_URI_NOT_SUPPORTED = "Empty Redirect-URL not supported";
    private static final String OUT_INVALID_REDIRECT_URI = "Got unexpected redirect URI";
    private static final String OUT_INVALID_CLIENT_ID = "Got unexpected client ID";
    private static final String OUT_CLIENT_NOT_AUTHORIZED = "Could not authorize client";
    private static final String OUT_MALFORMED_URI = "Could not parse request URI";
    private static final String OUT_UNKNOWN_CLIENT_ID = "Client ID not allowed for this request type";

    @Getter
    private final Error error;
    @Getter
    private final URI errorUri;
    @Getter
    private final String outputMessage;

    private Oauth2Exception(String message, String outputMessage, Error error) {
        this(message, null, outputMessage, error);
    }

    private Oauth2Exception(String message, Throwable e, String outputMessage, Error error) {
        super(message, e);
        this.error = error;
        this.outputMessage = outputMessage;
        //TODO OAUTH2: Support errorURI
        this.errorUri = null;
    }

    static Oauth2Exception wrap(Exception e) {
        if (e instanceof Oauth2Exception) {
            return (Oauth2Exception) e;
        }
        if (e instanceof EngineException) {
            final var exception = (EngineException) e;
            return new Oauth2Exception(e.getMessage(), e, exception.getOutputMessage(), Error.SERVER_ERROR);
        }
        if (e instanceof EndpointException) {
            final var exception = (EndpointException) e;
            return new Oauth2Exception(e.getMessage(), e, exception.getOutputMessage(), Error.SERVER_ERROR);
        }
        return new Oauth2Exception(e.getMessage(), e, OUT_GENERAL, Error.SERVER_ERROR);
    }

    static Oauth2Exception unsupportedResponseType(ResponseType responseType) {
        final var message = String.format("Response Type '%s' is not supported", responseType.out());
        return new Oauth2Exception(message, OUT_RESPONSE_TYPE, Error.UNSUPPORTED_RESPONSE_TYPE);
    }

    static Oauth2Exception notFinished() {
        final var message = "Session is not yet finished";
        return new Oauth2Exception(message, OUT_GENERAL, Error.SERVER_ERROR);
    }

    static Oauth2Exception unsupportedGrantType(GrantType grantType) {
        final var message = String.format("Grant Type '%s' is not supported", grantType.out());
        return new Oauth2Exception(message, OUT_RESPONSE_TYPE, Error.UNSUPPORTED_GRANT_TYPE);
    }

    static Oauth2Exception flowNotKnown(String flowName) {
        final var message = String.format("Flow '%s' is not known", flowName);
        return new Oauth2Exception(message, OUT_INVALID_CODE, Error.INVALID_GRANT);
    }

    static Oauth2Exception optionalRedirectUriNotSupported() {
        return new Oauth2Exception(OUT_NO_REDIRECT_URI_NOT_SUPPORTED, OUT_NO_REDIRECT_URI_NOT_SUPPORTED, Error.INVALID_REQUEST);
    }

    static Oauth2Exception wrongRedirectUriForCode(URI expected, URI received) {
        final var message = String.format("Expected redirect URI '%s', got '%s'", expected, received);
        return new Oauth2Exception(message, OUT_INVALID_REDIRECT_URI, Error.INVALID_REQUEST);
    }

    static Oauth2Exception wrongClientIdForCode(String expected, String received) {
        final var message = String.format("Expected client ID '%s', got '%s'", expected, received);
        return new Oauth2Exception(message, OUT_INVALID_CLIENT_ID, Error.INVALID_REQUEST);
    }

    static Oauth2Exception clientAuthenticationFailed() {
        return new Oauth2Exception(OUT_CLIENT_NOT_AUTHORIZED, OUT_CLIENT_NOT_AUTHORIZED, Error.UNAUTHORIZED_CLIENT);
    }

    public static Oauth2Exception couldNotParseUri(String uri, Exception e) {
        final var message = String.format("Could not parse URI '%s': %s", uri, e.getMessage());
        return new Oauth2Exception(message, e, OUT_MALFORMED_URI, Error.INVALID_REQUEST);
    }

    static Oauth2Exception noUserSet() {
        final var message = "No user set during authentication";
        return new Oauth2Exception(message, OUT_GENERAL, Error.SERVER_ERROR);
    }

    static Oauth2Exception clientIdNotValidForFlow(String clientId, ResponseType responseType) {
        final var message = String.format("Could not find config for client '%s' and response type %s", clientId, responseType.out());
        return new Oauth2Exception(message, OUT_UNKNOWN_CLIENT_ID, Error.INVALID_CLIENT);
    }

    public static Oauth2Exception invalidToken(String token, Exception e) {
        final var message = String.format("Could not read token '%s': %s", token, e.getMessage());
        return new Oauth2Exception(message, OUT_INVALID_CODE, Error.INVALID_GRANT);
    }

    static Oauth2Exception tokenNotFound(String code) {
        final var message = String.format("Did not find token from code '%s'", code);
        return new Oauth2Exception(message, OUT_INVALID_CODE, Error.INVALID_GRANT);
    }

}



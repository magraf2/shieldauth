package ch.shield.auth.endpoint.oauth2.dto;


import ch.shield.auth.core.LoggerConstants;
import ch.shield.auth.endpoint.oauth2.Oauth2Exception;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.Charset;

@Getter
public class AuthorizationErrorResponse {
    private final static Logger LOG = LoggerFactory.getLogger(LoggerConstants.LOGGER_OAUTH2);
    private final Error error;
    private final String errorDescription;
    private final URI errorUri;
    private final String state;
    private final URI redirectUri;

    @Builder
    private AuthorizationErrorResponse(@NonNull final Oauth2Exception exception, @NonNull final URI redirectUri, final String state) {
        this.error = exception.getError();
        this.errorDescription = exception.getOutputMessage();
        this.errorUri = exception.getErrorUri();
        this.redirectUri = redirectUri;
        this.state = state;
    }

    public ResponseEntity<NoRedirectErrorResponse> write() {
        final var headers = getHeaders();
        return new ResponseEntity<>(null, headers, HttpStatus.FOUND);
    }

    private HttpHeaders getHeaders() {
        final var redirectUrlBuilder = UriComponentsBuilder
                .fromUri(redirectUri)
                .queryParam("error", error.toString());
        if (errorDescription != null) {
            redirectUrlBuilder.queryParam("error_description", URLEncoder.encode(errorDescription, Charset.defaultCharset()));
        }
        if (errorUri != null) {
            redirectUrlBuilder.queryParam("error_uri", URLEncoder.encode(errorUri.toString(), Charset.defaultCharset()));
        }
        if (state != null) {
            redirectUrlBuilder.queryParam("state", state);
        }
        final var redirectUrl = redirectUrlBuilder.build().toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(redirectUrl);
        return headers;
    }
}

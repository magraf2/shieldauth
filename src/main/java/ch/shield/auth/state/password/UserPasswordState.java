package ch.shield.auth.state.password;

import ch.shield.auth.core.Renderable;
import ch.shield.auth.core.repository.User;
import ch.shield.auth.core.state.Context;
import ch.shield.auth.core.state.StateInitialization;
import ch.shield.auth.state.exceptions.StateInitializationException;
import ch.shield.auth.core.state.result.*;
import ch.shield.auth.state.base.StateBase;
import ch.shield.auth.state.exceptions.CredentialLockedException;
import ch.shield.auth.state.exceptions.CredentialMissingException;
import ch.shield.auth.state.exceptions.UserLockedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An auth state implementing an username and password check
 *
 * <p>The state takes the following inputs:<ul>
 *     <li>{@value INPUT_USERID}: The user-id</li>
 *     <li>{@value INPUT_PASSWORD}: The password</li>
 * </ul></p>
 * <p>The state uses the gui {@value GUI_NAME} with the following output arguments:<ul>
 *     <li>{@value ch.shield.auth.core.Renderable#MESSAGE}: Can be {@value MESSAGE_ENTER_RESPONSE}</li>
 *     <li>{@value ch.shield.auth.core.Renderable#EXCEPTION}: Can be {@value EXCEPTION_INVALID_RESPONSE} or {@value EXCEPTION_NO_INPUT}</li>
 *  <p>The state can throw the following {@link StateFinalException}s:<ul>
 *     <li>{@link UserLockedException}</b></li>
 *     <li>{@link CredentialMissingException}</b></li>
 *     <li>{@link CredentialLockedException}</b></li>
 * </ul></p>
 *  <p>The state can result in the following transitions:<ul>
 *     <li>{@value TRANSITION_OK}</li>
 * </ul></p>
 */
public class UserPasswordState extends StateBase {
    private final static Logger LOG = LoggerFactory.getLogger("PasswordState");
    private static final String INPUT_PASSWORD = "password";
    private static final String INPUT_USERID = "userid";
    private static final String GUI_NAME = "user_password";
    private static final String MESSAGE_ENTER_RESPONSE = "Please enter UserId and Password";
    private static final String EXCEPTION_NO_INPUT = "UserId or Password missing";
    private static final String EXCEPTION_INVALID_RESPONSE = "UserId or Password wrong";

    @Override
    public void initialize(StateInitialization stateInitialization) {
        super.initialize(stateInitialization);
        if(!stateInitialization.getInputs().contains(INPUT_USERID)){
            throw StateInitializationException.inputNotGiven(INPUT_USERID);
        }
        if(!stateInitialization.getInputs().contains(INPUT_PASSWORD)){
            throw StateInitializationException.inputNotGiven(INPUT_PASSWORD);
        }
    }

    @Override
    public StateResult execute(final Context context) {
        //Check Input first
        final var userId = context.getInput(INPUT_USERID);
        final var password = context.getInput(INPUT_PASSWORD);
        if(!userId.isPresent() || !password.isPresent()){
            return handleNoInput(context);
        }

        final var user = context.getRepository().getUserByUserId(userId.get());
        if (!user.isPresent()) {
            return userIdOrPasswordWrong();
        }
        if (user.get().isLocked()) {
            throw new UserLockedException();
        }

        final var credential = loadCredential(user.get());
        return checkResult(credential,password.get());
    }

    private StateResult handleNoInput(Context context){
        final var lastState = context.getLastState();
        if (!lastState.isPresent() || lastState.get() == this) {
            return RenderableStateResult.builder()
                    .guiName(GUI_NAME)
                    .guiAttribute(Renderable.MESSAGE, MESSAGE_ENTER_RESPONSE)
                    .build();
        }
        return RenderableStateResult.builder()
                .guiName(GUI_NAME)
                .guiAttribute(Renderable.EXCEPTION, EXCEPTION_NO_INPUT)
                .build();
    }

    private static PasswordCredential loadCredential(final User user){
        final var credential = user.getProperties(PasswordCredential.class).stream().findFirst().orElseThrow(CredentialMissingException::new);
        if (!credential.isValid()) {
            throw new CredentialLockedException();
        }
        return credential;
    }

    private static StateResult checkResult(PasswordCredential credential, String response) {
        if (credential.check(response)) {
            return TransitionStateResult.builder().transition(TRANSITION_OK).build();
        }
        LOG.info("Response was invalid.");
        return  userIdOrPasswordWrong();
    }

    private static StateResult userIdOrPasswordWrong(){
        return RenderableStateResult.builder()
                .guiName(GUI_NAME)
                .guiAttribute(Renderable.EXCEPTION, EXCEPTION_INVALID_RESPONSE)
                .build();
    }
}

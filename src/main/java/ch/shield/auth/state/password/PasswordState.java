package ch.shield.auth.state.password;

import ch.shield.auth.core.Renderable;
import ch.shield.auth.core.state.Context;
import ch.shield.auth.core.state.StateInitialization;
import ch.shield.auth.state.exceptions.StateInitializationException;
import ch.shield.auth.core.state.result.*;
import ch.shield.auth.state.base.StateBase;
import ch.shield.auth.state.exceptions.CredentialLockedException;
import ch.shield.auth.state.exceptions.CredentialMissingException;
import ch.shield.auth.state.exceptions.UserNotSetException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An auth state implementing a password check
 * This is a 2nd factor, a user needs to be set by a state before
 *
 * <p>The state takes the following inputs:<ul>
 *     <li>{@value INPUT_PASSWORD}: The password</li>
 * </ul></p>
 * <p>The state uses the gui {@value GUI_NAME} with the following output arguments:<ul>
 *     <li>{@value ch.shield.auth.core.Renderable#MESSAGE}: Can be {@value MESSAGE_ENTER_RESPONSE}</li>
 *     <li>{@value ch.shield.auth.core.Renderable#EXCEPTION}: Can be {@value EXCEPTION_INVALID_RESPONSE} or {@value EXCEPTION_NO_INPUT}</li>
 *  <p>The state can throw the following {@link StateFinalException}s:<ul>
 *     <li>{@link UserNotSetException}</b></li>
 *     <li>{@link CredentialMissingException}</b></li>
 *     <li>{@link CredentialLockedException}</b></li>
 * </ul></p>
 *  <p>The state can result in the following transitions:<ul>
 *     <li>{@value TRANSITION_OK}</li>
 * </ul></p>
 */
public class PasswordState extends StateBase {
    private final static Logger LOG = LoggerFactory.getLogger("PasswordState");
    private static final String INPUT_PASSWORD = "password";
    private static final String GUI_NAME = "password";
    private static final String MESSAGE_ENTER_RESPONSE = "Please enter Password";
    private static final String EXCEPTION_NO_INPUT = "No Response in Request";
    private static final String EXCEPTION_INVALID_RESPONSE = "Response was invalid";

    @Override
    public void initialize(StateInitialization stateInitialization) {
        super.initialize(stateInitialization);
        if(!stateInitialization.getInputs().contains(INPUT_PASSWORD)){
            throw StateInitializationException.inputNotGiven(INPUT_PASSWORD);
        }
    }

    @Override
    public StateResult execute(final Context context) {
        final var credential = loadCredential(context);
        final var input = context.getInput(INPUT_PASSWORD);
        if (!input.isPresent()) {
            return handleNoInput(context);
        }
        return checkResult(credential,input.get());
    }

    private static PasswordCredential loadCredential(final Context context){
        final var user = context.getUser().orElseThrow(UserNotSetException::new);
        final var challengeCredential = user.getProperties(PasswordCredential.class).stream().findFirst().orElseThrow(CredentialMissingException::new);
        if (!challengeCredential.isValid()) {
            throw new CredentialLockedException();
        }
        return challengeCredential;
    }

    private StateResult handleNoInput(Context context){
        final var lastState = context.getLastState();
        if (!lastState.isPresent() || lastState.get() == this) {
            return RenderableStateResult.builder()
                    .guiName(GUI_NAME)
                    .guiAttribute(Renderable.MESSAGE, MESSAGE_ENTER_RESPONSE)
                    .build();
        }
        return RenderableStateResult.builder()
                .guiName(GUI_NAME)
                .guiAttribute(Renderable.EXCEPTION, EXCEPTION_NO_INPUT)
                .build();
    }

    private static StateResult checkResult(PasswordCredential credential, String response) {
        if (credential.check(response)) {
            return TransitionStateResult.builder().transition(TRANSITION_OK).build();
        }
        LOG.info("Response was invalid.");
        return RenderableStateResult.builder()
                .guiName(GUI_NAME)
                .guiAttribute(Renderable.EXCEPTION, EXCEPTION_INVALID_RESPONSE)
                .build();
    }
}

package ch.shield.auth.state.password;

import ch.shield.auth.core.repository.Property;

public interface PasswordCredential extends Property {
    boolean check(String password);

    boolean isValid();

    void update(String newPassword);
}

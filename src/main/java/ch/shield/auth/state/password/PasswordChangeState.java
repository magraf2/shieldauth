package ch.shield.auth.state.password;

import ch.shield.auth.core.Renderable;
import ch.shield.auth.core.state.Context;
import ch.shield.auth.core.state.result.RenderableStateResult;
import ch.shield.auth.state.exceptions.StateInitializationException;
import ch.shield.auth.core.state.result.StateFinalException;
import ch.shield.auth.core.state.StateInitialization;
import ch.shield.auth.core.state.result.StateResult;
import ch.shield.auth.core.state.result.TransitionStateResult;
import ch.shield.auth.state.base.StateBase;
import ch.shield.auth.state.exceptions.CredentialLockedException;
import ch.shield.auth.state.exceptions.CredentialMissingException;
import ch.shield.auth.state.exceptions.UserNotSetException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * An auth state to change a password
 * This is a 2nd factor, a user needs to be set by a state before
 *
 * <p>The state takes the following inputs:<ul>
 *     <li>{@value INPUT_PASSWORD}: The password</li>
 *     <li>{@value INPUT_PASSWORD_2}: The password repeat (optional)</li>
 *     <li>{@value INPUT_OLD_PASSWORD}: The old password (optional)</li>
 * </ul></p>
 * <p>The state uses the gui {@value GUI_NAME} with the following output arguments:<ul>
 *     <li>{@value ch.shield.auth.core.Renderable#MESSAGE}: Can be {@value MESSAGE_ENTER_RESPONSE}</li>
 *     <li>{@value ch.shield.auth.core.Renderable#EXCEPTION}: Can be {@value EXCEPTION_INVALID_OLD_PASSWORD}, {@value EXCEPTION_INVALID_PASSWORD_2} or {@value EXCEPTION_NO_INPUT}</li>
 *  <p>The state can throw the following {@link StateFinalException}s:<ul>
 *     <li>{@link UserNotSetException}</b></li>
 *     <li>{@link CredentialMissingException}</b></li>
 *     <li>{@link CredentialLockedException}</b></li>
 * </ul></p>
 *  <p>The state can result in the following transitions:<ul>
 *     <li>{@value TRANSITION_OK}</li>
 * </ul></p>
 */public class PasswordChangeState extends StateBase {
    private final static Logger LOG = LoggerFactory.getLogger("PasswordState");
    private static final String INPUT_OLD_PASSWORD = "old_password";
    private static final String INPUT_PASSWORD = "password";
    private static final String INPUT_PASSWORD_2 = "password_2";
    private static final String GUI_NAME = "password_change";
    private static final String MESSAGE_ENTER_RESPONSE = "Please enter new Password";
    private static final String EXCEPTION_NO_INPUT = "Input missing";
    private static final String EXCEPTION_INVALID_OLD_PASSWORD = "Old password was invalid";
    private static final String EXCEPTION_INVALID_PASSWORD_2 = "Password repeat not equal to new password";
    private boolean inputOldPasswordRequired;
    private boolean inputPasswordTwice;

    @Override
    public void initialize(StateInitialization stateInitialization) {
        super.initialize(stateInitialization);
        if(!stateInitialization.getInputs().contains(INPUT_PASSWORD)){
            throw StateInitializationException.inputNotGiven(INPUT_PASSWORD);
        }
        if(!stateInitialization.getInputs().contains(INPUT_OLD_PASSWORD)){
            inputOldPasswordRequired = true;
        }
        if(!stateInitialization.getInputs().contains(INPUT_PASSWORD_2)){
            inputPasswordTwice = true;
        }
    }

    @Override
    public StateResult execute(final Context context) {
        final var password =  context.getInput(INPUT_PASSWORD);
        if(!password.isPresent()) {
            return handleNoInput(context);

        }

        if(inputOldPasswordRequired) {
            final var oldPassword = context.getInput(INPUT_OLD_PASSWORD);
            if(!oldPassword.isPresent()) {
                return handleNoInput(context);
            }
            final var user = context.getUser().orElseThrow(UserNotSetException::new);
            final var credential = user.getProperties(PasswordCredential.class).stream().findFirst().orElseThrow(CredentialMissingException::new);
            if (!credential.isValid()) {
                throw new CredentialLockedException();
            }
            if(!credential.check(oldPassword.get())) {
                return RenderableStateResult.builder()
                        .guiName(GUI_NAME)
                        .guiAttribute(Renderable.EXCEPTION, EXCEPTION_INVALID_OLD_PASSWORD)
                        .build();
            }
        }

        if(inputPasswordTwice) {
            final var password2 = context.getInput(INPUT_PASSWORD_2);
            if(!password2.isPresent()){
                return handleNoInput(context);
            }
            if(!password2.get().equals(password.get())){
                return RenderableStateResult.builder()
                        .guiName(GUI_NAME)
                        .guiAttribute(Renderable.EXCEPTION, EXCEPTION_INVALID_PASSWORD_2)
                        .build();
            }
        }

        final var user = context.getUser().orElseThrow(UserNotSetException::new);
        final var credential = user.getProperties(PasswordCredential.class).stream().findFirst().orElse(user.createProperty(PasswordCredential.class));
        credential.update(password.get());
        LOG.info("Updated password");
        return TransitionStateResult.builder().transition(TRANSITION_OK).build();
    }

    private StateResult handleNoInput(Context context){
        final var lastState = context.getLastState();
        if (!lastState.isPresent() || lastState.get() == this) {
            return RenderableStateResult.builder()
                    .guiName(GUI_NAME)
                    .guiAttribute(Renderable.MESSAGE, MESSAGE_ENTER_RESPONSE)
                    .build();
        }
        return RenderableStateResult.builder()
                .guiName(GUI_NAME)
                .guiAttribute(Renderable.EXCEPTION, EXCEPTION_NO_INPUT)
                .build();
    }
}
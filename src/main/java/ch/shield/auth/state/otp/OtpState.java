package ch.shield.auth.state.otp;

import ch.shield.auth.core.Renderable;
import ch.shield.auth.core.state.Context;
import ch.shield.auth.state.exceptions.StateInitializationException;
import ch.shield.auth.core.state.result.*;
import ch.shield.auth.core.state.StateInitialization;
import ch.shield.auth.state.base.StateBase;
import ch.shield.auth.state.exceptions.CredentialLockedException;
import ch.shield.auth.state.exceptions.CredentialMissingException;
import ch.shield.auth.state.exceptions.UserNotSetException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * An auth state implementing a one-time-password check
 * This is a 2nd factor, a user needs to be set by a state before
 *
 * <p>The state takes the following inputs:<ul>
 *     <li>{@value INPUT_OTP}: The one-time-password</li>
 * </ul></p>
 * <p>The state uses the gui {@value GUI_NAME} with the following output arguments:<ul>
 *     <li>{@value ch.shield.auth.core.Renderable#MESSAGE}: Can be {@value MESSAGE_ENTER_RESPONSE}</li>
 *     <li>{@value ch.shield.auth.core.Renderable#EXCEPTION}: Can be {@value EXCEPTION_INVALID_RESPONSE} or {@value EXCEPTION_NO_INPUT}</li>
 *  <p>The state can throw the following {@link StateFinalException}s:<ul>
 *     <li>{@link UserNotSetException}</b></li>
 *     <li>{@link CredentialMissingException}</b></li>
 *     <li>{@link CredentialLockedException}</b></li>
 * </ul></p>
 *  <p>The state can result in the following transitions:<ul>
 *     <li>{@value TRANSITION_OK}</li>
 * </ul></p>
 */
public class OtpState extends StateBase {
    private final static Logger LOG = LoggerFactory.getLogger("OtpState");
    private static final String INPUT_OTP = "otp";
    private static final String MESSAGE_ENTER_RESPONSE = "Please enter OTP";
    private static final String EXCEPTION_NO_INPUT = "No Response in Request";
    private static final String EXCEPTION_INVALID_RESPONSE = "Response was invalid";
    private static final String GUI_NAME = "otp";

    @Override
    public void initialize(StateInitialization stateInitialization) {
        super.initialize(stateInitialization);
        if(!stateInitialization.getInputs().contains(INPUT_OTP)){
            throw StateInitializationException.inputNotGiven(INPUT_OTP);
        }
    }

    @Override
    public StateResult execute(final Context context) {
        final var credential = loadCredential(context);
        final var response = context.getInput(INPUT_OTP);
        if(!response.isPresent()){
            return handleNoInput(context);
        }
        return checkResponse(credential, response.get());
    }

    private static OtpCredential loadCredential(final Context context){
        final var user = context.getUser().orElseThrow(UserNotSetException::new);
        final var credential = user.getProperties(OtpCredential.class).stream().findFirst().orElseThrow(CredentialMissingException::new);
        if (!credential.isValid()) {
            throw new CredentialLockedException();
        }
        return credential;
    }

    private StateResult handleNoInput(Context context){
        final var lastState = context.getLastState();
        if (!lastState.isPresent() || lastState.get() == this) {
            return RenderableStateResult.builder()
                    .guiName(GUI_NAME)
                    .guiAttribute(Renderable.MESSAGE, MESSAGE_ENTER_RESPONSE)
                    .build();
        }
        return RenderableStateResult.builder()
                .guiName(GUI_NAME)
                .guiAttribute(Renderable.EXCEPTION, EXCEPTION_NO_INPUT)
                .build();
    }

    private static StateResult checkResponse(OtpCredential credential, String response) {
        if (credential.check(response)) {
            return TransitionStateResult.builder().transition(TRANSITION_OK).build();
        }
        LOG.info("Response was invalid.");
        return RenderableStateResult.builder()
                .guiName(GUI_NAME)
                .guiAttribute(Renderable.EXCEPTION, EXCEPTION_INVALID_RESPONSE)
                .build();
    }
}

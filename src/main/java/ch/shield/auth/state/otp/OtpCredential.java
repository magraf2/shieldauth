package ch.shield.auth.state.otp;

import ch.shield.auth.core.repository.Property;

public interface OtpCredential extends Property {
    boolean check(String otp);

    String getName();

    @SuppressWarnings("SameReturnValue")
    boolean isValid();
}

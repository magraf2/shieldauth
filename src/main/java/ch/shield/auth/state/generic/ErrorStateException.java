package ch.shield.auth.state.generic;

import ch.shield.auth.core.state.result.StateFinalException;

public class ErrorStateException extends StateFinalException {
    public ErrorStateException(final String message, final int statusCode) {
        super(message,null, statusCode, message);
    }
}

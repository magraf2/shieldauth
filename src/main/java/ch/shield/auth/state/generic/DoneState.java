package ch.shield.auth.state.generic;

import ch.shield.auth.core.state.Context;
import ch.shield.auth.core.state.result.StateResult;
import ch.shield.auth.core.state.result.SuccessStateResult;
import ch.shield.auth.state.base.StateBase;

public class DoneState extends StateBase {
    @Override
    public StateResult execute(Context context) {
        return SuccessStateResult.builder().build();
    }
}

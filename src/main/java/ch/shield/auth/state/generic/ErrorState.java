package ch.shield.auth.state.generic;

import ch.shield.auth.core.state.Context;
import ch.shield.auth.core.state.result.StateFinalException;
import ch.shield.auth.core.state.result.StateResult;
import ch.shield.auth.state.base.StateBase;

/**
 * An auth state that always return an exception.
 *
 * <p>The state takes the following inputs:<ul>
 *     <li>{@value INPUT_ERROR_MESSAGE}: The error message to be shown</li>
 *     <li>{@value INPUT_STATUS_CODE}: The status code to be shown</li>
 * </ul></p>
 *  <p>The state can throw the following {@link StateFinalException}s:<ul>
 *     <li>{@link ErrorStateException}</b></li>
 * </ul></p>
 */public class ErrorState extends StateBase {
    private static final String INPUT_ERROR_MESSAGE = "error";
    private static final String INPUT_STATUS_CODE = "statusCode";
    private static final String GENERAL_SERVER_ERROR = "General Server Error";

    @Override
    public StateResult execute(Context context) {
        final var message = context.getInput(INPUT_ERROR_MESSAGE).orElse(GENERAL_SERVER_ERROR);
        final var statusCodeStr = context.getInput(INPUT_STATUS_CODE).orElse("500");
        final var statusCode = Integer.parseInt(statusCodeStr);
        throw new ErrorStateException(message, statusCode);
    }
}

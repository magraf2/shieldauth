package ch.shield.auth.state.exceptions;

import ch.shield.auth.core.state.result.StateFinalException;

public class CredentialMissingException extends StateFinalException {
    private static final String MESSAGE = "Credential not defined";

    public CredentialMissingException() {
        super(MESSAGE,null, 401, MESSAGE);
    }
}

package ch.shield.auth.state.exceptions;

public class StateInitializationException extends RuntimeException {
    private StateInitializationException(String message, Throwable cause) {
        super(message, cause);
    }

    public static StateInitializationException inputNotGiven(String inputName) {
        final var message = String.format("The state needs the input %s, but this is not defined", inputName);
        return new StateInitializationException(message, null);
    }
}

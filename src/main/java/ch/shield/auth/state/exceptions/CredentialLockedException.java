package ch.shield.auth.state.exceptions;

import ch.shield.auth.core.state.result.StateFinalException;

public class CredentialLockedException extends StateFinalException {
    private static final String MESSAGE = "Credential is locked";

    public CredentialLockedException() {
        super(MESSAGE,null, 401, MESSAGE);
    }
}

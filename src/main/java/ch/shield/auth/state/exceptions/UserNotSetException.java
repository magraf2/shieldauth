package ch.shield.auth.state.exceptions;

import ch.shield.auth.core.state.result.StateFinalException;

public class UserNotSetException extends StateFinalException {
    private static final String SERVER_ERROR = "Internal Server Error";
    private static final String MESSAGE = "No user found. User should have been set by a previous state";

    public UserNotSetException() {
        super(MESSAGE,null, 500, SERVER_ERROR);
    }
}

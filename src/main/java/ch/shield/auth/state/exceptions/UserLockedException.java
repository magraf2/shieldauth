package ch.shield.auth.state.exceptions;

import ch.shield.auth.core.state.result.StateFinalException;

public class UserLockedException extends StateFinalException {
    private static final String MESSAGE = "User is locked";

    public UserLockedException() {
        super(MESSAGE,null, 401, MESSAGE);
    }
}

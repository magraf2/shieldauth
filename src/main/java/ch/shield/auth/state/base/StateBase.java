package ch.shield.auth.state.base;

import ch.shield.auth.core.state.*;
import ch.shield.auth.core.state.result.StateResult;
import ch.shield.auth.core.state.result.TransitionStateResult;
import lombok.Getter;

/**
 * A base class implementing the state interface
 */
public class StateBase implements State {
    protected static final String TRANSITION_OK = "ok";

    @Getter
    private String name;

    @Override
    public void initialize(StateInitialization stateInitialization) {
        name = stateInitialization.getName();
    }

    @Override
    public StateResult execute(Context context) {
        return TransitionStateResult.builder().transition(TRANSITION_OK).build();
    }
}

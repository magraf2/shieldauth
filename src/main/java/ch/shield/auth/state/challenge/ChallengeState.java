package ch.shield.auth.state.challenge;

import ch.shield.auth.core.Renderable;
import ch.shield.auth.core.state.Context;
import ch.shield.auth.core.state.StateInitialization;
import ch.shield.auth.state.exceptions.StateInitializationException;
import ch.shield.auth.core.state.result.*;
import ch.shield.auth.state.base.*;
import ch.shield.auth.state.exceptions.CredentialLockedException;
import ch.shield.auth.state.exceptions.CredentialMissingException;
import ch.shield.auth.state.exceptions.UserNotSetException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An auth state implementing a challenge response authentication.
 * This is a 2nd factor, a user needs to be set by a state before
 *
 * <p>The state takes the following inputs:<ul>
 *     <li>{@value INPUT_RESPONSE}: The response to the challenge</li>
 * </ul></p>
 * <p>The state uses the gui {@value GUI_NAME} with the following output arguments:<ul>
 *     <li>{@value OUTPUT_CHALLENGE}: The generated challenge</li>
 *     <li>{@value ch.shield.auth.core.Renderable#MESSAGE}: Can be {@value MESSAGE_ENTER_RESPONSE}</li>
 *     <li>{@value ch.shield.auth.core.Renderable#EXCEPTION}: Can be {@value EXCEPTION_INVALID_RESPONSE} or {@value EXCEPTION_NO_INPUT}</li>
 *  <p>The state can throw the following {@link StateFinalException}s:<ul>
 *     <li>{@link UserNotSetException}</b></li>
 *     <li>{@link CredentialMissingException}</b></li>
 *     <li>{@link CredentialLockedException}</b></li>
 * </ul></p>
 *  <p>The state can result in the following transitions:<ul>
 *     <li>{@value TRANSITION_OK}</li>
 * </ul></p>
 */
public class ChallengeState extends StateBase {
    private final static Logger LOG = LoggerFactory.getLogger("ChallengeState");
    private static final String ATTRIBUTE_CHALLENGE = "challenge";
    private static final String OUTPUT_CHALLENGE = "challenge";
    private static final String INPUT_RESPONSE = "response";
    private static final String GUI_NAME = "challenge";
    private static final String MESSAGE_ENTER_RESPONSE = "Please enter Challenge-Response";
    private static final String EXCEPTION_NO_INPUT = "No Response in Request";
    private static final String EXCEPTION_INVALID_RESPONSE = "Response was invalid";

    @Override
    public void initialize(StateInitialization stateInitialization) {
        super.initialize(stateInitialization);
        if(!stateInitialization.getInputs().contains(INPUT_RESPONSE)){
            throw StateInitializationException.inputNotGiven(INPUT_RESPONSE);
        }
    }

    @Override
    public StateResult execute(final Context context) {
        final var credential = loadCredential(context);
        final var challenge = context.getAttribute(ATTRIBUTE_CHALLENGE, String.class);
        if(!challenge.isPresent()){
            return noChallengeGenerated(credential);
        }
        final var response = context.getInput(INPUT_RESPONSE);
        if(!response.isPresent()){
            return RenderableStateResult.builder()
                    .guiName(GUI_NAME)
                    .guiAttribute(Renderable.EXCEPTION, EXCEPTION_NO_INPUT)
                    .build();
        }
        return checkResponse(credential, challenge.get(), response.get());
    }

    private static ChallengeCredential loadCredential(final Context context){
        final var user = context.getUser().orElseThrow(UserNotSetException::new);
        final var challengeCredential = user.getProperties(ChallengeCredential.class).stream().findFirst().orElseThrow(CredentialMissingException::new);
        if (!challengeCredential.isValid()) {
            throw new CredentialLockedException();
        }
        return challengeCredential;
    }

    private static RenderableStateResult noChallengeGenerated(ChallengeCredential credential) {
        final var generatedChallenge = credential.generateChallenge();
        LOG.info("Generated challenge '" + generatedChallenge + "'");
        return RenderableStateResult.builder()
                .guiName(GUI_NAME)
                .guiAttribute(OUTPUT_CHALLENGE, generatedChallenge)
                .guiAttribute(Renderable.MESSAGE, MESSAGE_ENTER_RESPONSE)
                .sessionAttribute(ATTRIBUTE_CHALLENGE, generatedChallenge)
                .build();
    }

    private static StateResult checkResponse(ChallengeCredential credential, String challenge, String response) {
        if (credential.check(challenge, response)) {
            return TransitionStateResult.builder().transition(TRANSITION_OK).build();
        }
        final var generatedChallenge = credential.generateChallenge();
        LOG.info("Response was invalid. Generated new challenge '" + generatedChallenge + "'");
        return RenderableStateResult.builder()
                .guiName(GUI_NAME)
                .guiAttribute(OUTPUT_CHALLENGE, generatedChallenge)
                .guiAttribute(Renderable.EXCEPTION, EXCEPTION_INVALID_RESPONSE)
                .sessionAttribute(ATTRIBUTE_CHALLENGE, generatedChallenge)
                .build();
    }
}

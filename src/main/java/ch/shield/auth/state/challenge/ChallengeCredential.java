package ch.shield.auth.state.challenge;

import ch.shield.auth.core.repository.Property;

public interface ChallengeCredential extends Property {
    String getName();

    boolean check(String challenge, String response);

    String generateChallenge();

    boolean isValid();
}

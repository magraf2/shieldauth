package ch.shield.auth.core;

public class LoggerConstants {
    public static final String LOGGER_CONFIG = "Config";
    public final static String LOGGER_ENGINE = "Engine";
    public static final String LOGGER_OAUTH2 = "Endpoint.Oauth2";
    public static final String LOGGER_HTML = "Endpoint.Html";
    public static final String LOGGER_JSON = "Endpoint.Json";
    public static final String REQUEST_ID = "Request";
    public static final String SESSION_ID = "Session";

    private LoggerConstants() {
    }
}

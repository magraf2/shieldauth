package ch.shield.auth.core;

import java.util.List;
import java.util.Map;

public interface Renderable {
    String EXCEPTION = "errorMessage";
    String MESSAGE = "message";

    String getName();

    Map<String, Object> getAttributes();

    Map<String, List<String>> getHeaders();

    int getStatusCode();
}

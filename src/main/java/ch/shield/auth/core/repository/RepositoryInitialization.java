package ch.shield.auth.core.repository;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import java.util.Properties;

@Data
@Builder
public class RepositoryInitialization {
    @NonNull
    private final String name;
    @NonNull
    private final Properties properties;
}

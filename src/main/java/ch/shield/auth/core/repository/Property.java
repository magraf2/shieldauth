package ch.shield.auth.core.repository;

public interface Property {
    User getUser();

    long getId();
}

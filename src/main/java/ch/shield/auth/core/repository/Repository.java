package ch.shield.auth.core.repository;

import java.util.List;
import java.util.Optional;

public interface Repository {
    Optional<User> getUserByUserId(String userId);

    <T extends Property> List<T> getPropertyByValue(Class<T> type, String value);

    User createUser(String userId);

    @SuppressWarnings({"EmptyMethod", "unused"})
    void initialize(RepositoryInitialization repositoryInitialization);
}

package ch.shield.auth.core.repository;

import java.util.List;

public interface User {
    String getId();

    boolean isLocked();

    <T extends Property> List<T> getProperties(Class<T> type);

    <T extends Property> T createProperty(Class<T> type);

    void removeProperty(Property property);
}
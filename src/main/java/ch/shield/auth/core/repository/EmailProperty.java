package ch.shield.auth.core.repository;

public interface EmailProperty extends Property {
    String getEmail();

    void setEmail(String email);

    String getUsage();

    void setUsage(String email);

}

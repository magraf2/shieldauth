package ch.shield.auth.core.engine;

import lombok.Getter;

public class EngineException extends RuntimeException {
    @Getter
    private final int statusCode;
    @Getter
    private final String outputMessage;

    protected EngineException(final String message, final Throwable cause, final int statusCode, final String outputMessage) {
        super(message, cause);
        this.statusCode = statusCode;
        this.outputMessage = outputMessage;
    }

}

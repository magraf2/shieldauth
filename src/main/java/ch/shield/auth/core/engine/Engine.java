package ch.shield.auth.core.engine;

import javax.validation.constraints.NotNull;
import java.util.UUID;

public interface Engine {
    UUID init(@NotNull SessionAttributes sessionAttributes);

    EngineResult request(@NotNull EngineRequest request);

    boolean isFinished(@NotNull UUID sessionId);

    <T extends SessionAttributes> T getAttributes(@NotNull UUID sessionId, @NotNull Class<T> type);
}

package ch.shield.auth.core.engine;

import java.util.Map;
import java.util.UUID;

public interface EngineRequest {
    UUID getSessionId();

    Map<String, String[]> getHeaders();

    Map<String, String[]> getInputs();
}

package ch.shield.auth.core.engine;

import ch.shield.auth.core.repository.User;

import java.util.Map;
import java.util.Optional;

public interface EngineResult {
    boolean isFinished();

    Map<String, Object> getSessionAttributes();

    Optional<User> getUser();

    Optional<String> getGuiName();

    Map<String, Object> getGuiAttributes();

    Optional<Integer> getStatusCode();
}
package ch.shield.auth.core.engine;

import ch.shield.auth.core.config.Flow;

import java.net.URI;

public interface SessionAttributes {
    Flow getFlow();

    URI getFinalEndpoint();
}
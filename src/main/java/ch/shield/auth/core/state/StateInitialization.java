package ch.shield.auth.core.state;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.Singular;

import java.util.List;
import java.util.Properties;

@Data
@Builder
public class StateInitialization {
    @NonNull
    private final String name;
    @NonNull
    private final Properties properties;
    @NonNull
    @Singular
    private final List<String> inputs;
}

package ch.shield.auth.core.state.result;


import ch.shield.auth.core.repository.User;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Singular;

import java.util.Map;

@Getter
public class TransitionStateResult extends StateResult {
    private final String transition;

    @Builder
    public TransitionStateResult(@Singular @NonNull final Map<String, Object> sessionAttributes, final User user, @NonNull final String transition) {
        super(sessionAttributes, user);
        this.transition = transition;
    }
}
package ch.shield.auth.core.state.result;

import ch.shield.auth.core.repository.User;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Singular;

import java.util.Map;
import java.util.Optional;

/**
 * A StateResult that shall be rendered
 */
@Getter
public class RenderableStateResult extends StateResult {
    /**
     * Attributes to be shown in the GUI
     */
    private final Map<String, Object> guiAttributes;
    /**
     * The name of the GUI to be shown
     */
    private final String guiName;
    /**
     * The HTTP-Status-Code
     */
    private final int statusCode;

    @Builder
    public RenderableStateResult(@Singular @NonNull final Map<String, Object> sessionAttributes, final User user, @Singular @NonNull final Map<String, Object> guiAttributes, @NonNull final String guiName, final Integer statusCode) {
        super(sessionAttributes, user);
        this.guiAttributes = guiAttributes;
        this.guiName = guiName;
        this.statusCode = Optional.ofNullable(statusCode).orElse(200);
    }
}
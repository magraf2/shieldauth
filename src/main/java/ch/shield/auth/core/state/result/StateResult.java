package ch.shield.auth.core.state.result;

import ch.shield.auth.core.repository.User;
import lombok.Builder;
import lombok.Getter;

import java.util.Map;
import java.util.Optional;

/**
 * The result of a state. Classes should always extends {@link RenderableStateResult} or {@link TransitionStateResult} never this state directly
 */
@Builder
@Getter
public abstract class StateResult {
    /**
     * Attributes that have been changed in this state.
     * Set to null if the attribute shall be deleted
     */
    private final Map<String, Object> sessionAttributes;

    /**
     * The user if the user has changed in this state
     */
    private final User user;

    public Optional<User> getUser(){
        return Optional.ofNullable(user);
    }

    StateResult(final Map<String, Object> sessionAttributes, final User user) {
        this.user = user;
        this.sessionAttributes = sessionAttributes;
    }
}
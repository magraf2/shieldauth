package ch.shield.auth.core.state;

import ch.shield.auth.core.state.result.StateResult;

public interface State {
    String getName();

    StateResult execute(Context context);

    void initialize(StateInitialization stateInitialization);
}

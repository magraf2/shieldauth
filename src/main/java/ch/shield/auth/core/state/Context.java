package ch.shield.auth.core.state;

import ch.shield.auth.core.repository.Repository;
import ch.shield.auth.core.repository.User;

import java.util.Optional;

/**
 * The context of a current authentication
 */
public interface Context {
    Optional<String> getInput(String name);

    Optional<User> getUser();

    Repository getRepository();

    <T> Optional<T> getAttribute(String attributeChallenge, Class<T> type);

    Optional<State> getLastState();

    Optional<String> getLastTransition();
}

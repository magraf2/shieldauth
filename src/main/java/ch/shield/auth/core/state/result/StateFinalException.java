package ch.shield.auth.core.state.result;

import lombok.Getter;

//FIXME rename StateException and document
@Getter
public class StateFinalException extends RuntimeException {
    private final int statusCode;
    private final String outputMessage;

    protected StateFinalException(final String message, final Throwable cause, final int statusCode, final String outputMessage) {
        super(message, cause);
        this.statusCode = statusCode;
        this.outputMessage = outputMessage;
    }
}

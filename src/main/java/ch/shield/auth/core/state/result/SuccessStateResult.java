package ch.shield.auth.core.state.result;

import ch.shield.auth.core.repository.User;
import lombok.Builder;
import lombok.NonNull;
import lombok.Singular;

import java.util.Map;

/**
 * A StateResult that indicates that an authentication flow has finished successful
 */
public class SuccessStateResult extends StateResult {
    @Builder
    public SuccessStateResult(@Singular @NonNull final Map<String, Object> sessionAttributes, final User user) {
        super(sessionAttributes, user);
    }
}
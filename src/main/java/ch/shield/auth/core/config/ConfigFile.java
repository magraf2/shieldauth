package ch.shield.auth.core.config;

import ch.shield.auth.core.repository.Repository;
import ch.shield.auth.core.state.State;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface ConfigFile {
    State getNextState(@NotNull final Flow flow, final State lastState, final String transition);

    <T extends Flow> List<T> getFlows(@NotNull Class<T> type);

    <T extends Flow> Optional<T> getFlow(@NotNull String name, @NotNull Class<T> type);

    Repository getRepository(@NotNull Flow flow);

    Map<String, String> getInputs(@NotNull State state);
}

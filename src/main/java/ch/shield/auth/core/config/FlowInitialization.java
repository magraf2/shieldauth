package ch.shield.auth.core.config;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import java.util.Properties;

@Data
@Builder
public class FlowInitialization {
    @NonNull
    private final String name;
    @NonNull
    private final Properties properties;
}

package ch.shield.auth.core.config;

public interface Flow {
    String getName();

    void initialize(FlowInitialization flowInitialization);
}

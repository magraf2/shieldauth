package ch.shield.auth.repository.jdbc;

import ch.shield.auth.core.repository.Property;
import ch.shield.auth.core.repository.Repository;
import ch.shield.auth.core.repository.RepositoryInitialization;
import ch.shield.auth.core.repository.User;
import ch.shield.auth.repository.jdbc.db.PropertyRepository;
import ch.shield.auth.repository.jdbc.db.UserEntity;
import ch.shield.auth.repository.jdbc.db.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class JdbcRepository implements Repository {
    private final UserRepository userRepository;
    private final PropertyRepository propertyRepository;
    private final WrapperFactory wrapperFactory;

    @Autowired
    public JdbcRepository(UserRepository userRepository, PropertyRepository propertyRepository, WrapperFactory wrapperFactory) {
        this.userRepository = userRepository;
        this.propertyRepository = propertyRepository;
        this.wrapperFactory = wrapperFactory;
    }

    @Override
    public void initialize(RepositoryInitialization repositoryInitialization) {
    }

    @Override
    public Optional<User> getUserByUserId(String userId) {
        final var userEntity = userRepository.findByUserId(userId);
        if (userEntity == null) {
            return Optional.empty();
        }
        final var wrapper = wrapperFactory.wrapUser(userEntity);
        return Optional.of(wrapper);
    }

    @Override
    public <T extends Property> List<T> getPropertyByValue(Class<T> type, String value) {
        final var properties = propertyRepository.findByTypeAndValue(type.getName(), value);
        return properties.stream()
                .map(wrapperFactory::wrapProperty)
                .map(type::cast)
                .collect(Collectors.toList());
    }

    @Override
    public User createUser(String userId) {
        if (getUserByUserId(userId).isPresent()) {
            throw JdbcRepositoryException.userExists(userId);
        }
        final var userEntity = new UserEntity();
        userEntity.setUserId(userId);
        userRepository.save(userEntity);
        return wrapperFactory.wrapUser(userEntity);
    }
}
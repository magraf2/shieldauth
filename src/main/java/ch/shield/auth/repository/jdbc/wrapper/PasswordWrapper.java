package ch.shield.auth.repository.jdbc.wrapper;

import ch.shield.auth.repository.jdbc.db.PropertyEntity;
import ch.shield.auth.repository.jdbc.db.PropertyRepository;
import ch.shield.auth.state.password.PasswordCredential;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.security.SecureRandom;
import java.time.LocalDateTime;

@SuppressWarnings("unused")
public class PasswordWrapper extends PropertyWrapper implements PasswordCredential {
    private PasswordEncoder passwordEncoder;
    private PropertyRepository propertyRepository;

    public PasswordWrapper(PropertyEntity propertyEntity) {
        super(propertyEntity);
    }

    @Autowired
    public void setPropertyRepository(PropertyRepository propertyRepository) {
        this.propertyRepository = propertyRepository;
    }

    @Value("${repository.jdbc.password.salt:16}")
    public void setSaltStrength(int saltStrength) {
        passwordEncoder = new BCryptPasswordEncoder(saltStrength, new SecureRandom());
    }

    @Override
    public boolean isValid() {
        //TODO Keep track of failed authentications, lock credentials
        return true;
    }

    @Override
    public boolean check(String password) {
        return passwordEncoder.matches(password, propertyEntity.getValue());
    }


    @Override
    public void update(String newPassword) {
        String saltedHash = passwordEncoder.encode(newPassword);
        propertyEntity.setValue(saltedHash);
        propertyEntity.setModified(LocalDateTime.now());
        propertyRepository.save(propertyEntity);
    }
}

package ch.shield.auth.repository.jdbc.db;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity(name = "propertyAddition")
@Getter
@NoArgsConstructor
public class PropertyAdditionEntity {
    @Id
    @GeneratedValue
    @NotNull
    private Long id;

    @NotNull
    @Setter
    @ManyToOne
    private PropertyEntity property;

    @NotNull
    private String key;

    @Setter
    @NotNull
    private String value;

    public PropertyAdditionEntity(String key, String value) {
        this.key = key;
        this.value = value;
    }
}

package ch.shield.auth.repository.jdbc;

import ch.shield.auth.core.repository.Property;
import ch.shield.auth.core.repository.User;
import ch.shield.auth.repository.jdbc.db.PropertyEntity;
import ch.shield.auth.repository.jdbc.db.UserEntity;
import ch.shield.auth.repository.jdbc.wrapper.UserWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AssignableTypeFilter;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class WrapperFactoryImpl implements WrapperFactory {
    private final static List<Class<?>> wrapperClasses;

    static {
        final var packageName = WrapperFactoryImpl.class.getPackageName();
        final var scanner = new ClassPathScanningCandidateComponentProvider(false);
        scanner.addIncludeFilter(new AssignableTypeFilter(Property.class));
        final var classes = scanner.findCandidateComponents(packageName);
        wrapperClasses = classes.stream()
                .map(WrapperFactoryImpl::load)
                .collect(Collectors.toList());
    }

    private final AutowireCapableBeanFactory autowireCapableBeanFactory;

    @Autowired
    public WrapperFactoryImpl(AutowireCapableBeanFactory autowireCapableBeanFactory) {
        this.autowireCapableBeanFactory = autowireCapableBeanFactory;
    }

    private static Class<?> load(BeanDefinition beanDefinition) {
        final String name = beanDefinition.getBeanClassName();
        try {
            final var classLoader = WrapperFactoryImpl.class.getClassLoader();
            return classLoader.loadClass(name);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Cannot load class " + name);
        }
    }

    @Override
    public User wrapUser(UserEntity userEntity) {
        final var wrapper = new UserWrapper(userEntity);
        autowireCapableBeanFactory.autowireBean(wrapper);
        autowireCapableBeanFactory.initializeBean(wrapper, null);
        return wrapper;
    }

    @Override
    public Property wrapProperty(PropertyEntity propertyEntity) {
        try {
            final var wrapperClass = getWrapperClass(propertyEntity.getType());
            final var constructor = wrapperClass.getConstructor(PropertyEntity.class);
            final var wrapper = (Property) constructor.newInstance(propertyEntity);
            autowireCapableBeanFactory.autowireBean(wrapper);
            autowireCapableBeanFactory.initializeBean(wrapper, null);
            return wrapper;
        } catch (InstantiationException | NoSuchMethodException | IllegalAccessException | InvocationTargetException | ClassNotFoundException e) {
            throw JdbcRepositoryException.wrappingException(propertyEntity.getType(), e);
        }
    }


    private Class<?> getWrapperClass(final String typeName) throws ClassNotFoundException {
        final var classLoader = WrapperFactoryImpl.class.getClassLoader();
        final var type = classLoader.loadClass(typeName);
        return wrapperClasses.stream()
                .filter(type::isAssignableFrom)
                .findFirst()
                .orElseThrow(() -> JdbcRepositoryException.noClassDefined(typeName));
    }
}

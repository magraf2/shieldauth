package ch.shield.auth.repository.jdbc.wrapper;

import ch.shield.auth.endpoint.oauth2.Oauth2Token;
import ch.shield.auth.endpoint.oauth2.TokenData;
import ch.shield.auth.repository.base.JWEGenerator;
import ch.shield.auth.repository.jdbc.JdbcRepositoryException;
import ch.shield.auth.repository.jdbc.db.PropertyAdditionEntity;
import ch.shield.auth.repository.jdbc.db.PropertyEntity;
import ch.shield.auth.repository.jdbc.db.PropertyRepository;
import ch.shield.auth.repository.jdbc.db.RoleEntity;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.URI;
import java.net.URISyntaxException;
import java.security.interfaces.RSAPublicKey;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
public class Oauth2TokenWrapper extends PropertyWrapper implements Oauth2Token {
    private PropertyRepository propertyRepository;

    public Oauth2TokenWrapper(PropertyEntity propertyEntity) {
        super(propertyEntity);
    }

    @Autowired
    public void setPropertyRepository(PropertyRepository propertyRepository) {
        this.propertyRepository = propertyRepository;
    }

    @Override
    public URI getRedirectUri() {
        final var value = getAdditionValue("redirectUri");
        try {
            return new URI(value);
        } catch (URISyntaxException e) {
            throw JdbcRepositoryException.readFormatException(e);
        }
    }

    @Override
    public String getClientId() {
        return getAdditionValue("clientId");
    }

    @Override
    public LocalDateTime getExpiresAt() {
        final var value = getAdditionValue("expiresAt");
        final var longValue = Long.parseLong(value);
        return LocalDateTime.ofEpochSecond(longValue, 0, ZoneOffset.UTC);
    }

    @Override
    public List<String> getScopes() {
        return getAdditionValues("scope");
    }

    @Override
    public UUID getCode() {
        return UUID.fromString(propertyEntity.getValue());
    }

    @Override
    public String computeAccessToken(final String issuer, final RSAPublicKey key) {
        //TODO Oauth2: Support additional claims
        final var audiences = getAdditionValues("audience");
        return JWEGenerator.builder()
                .audiences(audiences)
                .expirationTime(getExpiresAt())
                .issuedAt(LocalDateTime.now())
                .issuer(issuer)
                .key(key)
                .notBefore(LocalDateTime.now())
                .subject(getUser().getId())
                .build();
    }


    @Override
    public void update(TokenData tokenData) {
        propertyEntity.setValue(tokenData.getCode().toString());
        propertyEntity.setModified(LocalDateTime.now());
        propertyEntity.cleanAdditions();
        propertyEntity.addAddition(new PropertyAdditionEntity("redirectUri", tokenData.getRedirectUri().toString()));
        propertyEntity.addAddition(new PropertyAdditionEntity("clientId", tokenData.getClientId()));
        propertyEntity.addAddition(new PropertyAdditionEntity("expiresAt", Long.toString(tokenData.getExpiresAt().toEpochSecond(ZoneOffset.UTC))));
        tokenData.getAudiences().stream()
                .map(x -> new PropertyAdditionEntity("audiences", x))
                .forEach(propertyEntity::addAddition);
        final var roles = propertyEntity.getUser().getRoles().stream()
                .map(RoleEntity::getName)
                .collect(Collectors.toList());
        if(tokenData.getWantedScopes().size() > 0) {
            tokenData.getWantedScopes().stream()
                    .filter(roles::contains)
                    .map(x -> new PropertyAdditionEntity("scope", x))
                    .forEach(propertyEntity::addAddition);
        }
        else {
            roles.stream()
                    .map(x -> new PropertyAdditionEntity("scope", x))
                    .forEach(propertyEntity::addAddition);
        }
        propertyRepository.save(propertyEntity);
    }
}

package ch.shield.auth.repository.jdbc;

import ch.shield.auth.core.repository.Property;
import ch.shield.auth.core.repository.User;
import ch.shield.auth.repository.jdbc.db.PropertyEntity;
import ch.shield.auth.repository.jdbc.db.UserEntity;

public interface WrapperFactory {
    User wrapUser(UserEntity userEntity);

    Property wrapProperty(PropertyEntity propertyEntity);
}

package ch.shield.auth.repository.jdbc.db;

import ch.shield.auth.core.repository.Property;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Entity(name = "property")
@Getter
@NoArgsConstructor
public class PropertyEntity {
    @Id
    @GeneratedValue
    @NotNull
    private Long id;

    @NotNull
    private String type;

    @NotNull
    @ManyToOne
    private UserEntity user;

    @Setter
    @NotNull
    private String value;

    @NotNull
    private LocalDateTime created;

    @Setter
    @NotNull
    private LocalDateTime modified;

    @OneToMany(mappedBy = "property", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private Set<PropertyAdditionEntity> additions;

    public PropertyEntity(Class<? extends Property> type, UserEntity user) {
        this.type = type.getName();
        this.user = user;
        this.value = "";
        this.created = LocalDateTime.now();
        this.modified = LocalDateTime.now();
        this.additions = new HashSet<>();
    }

    public void addAddition(PropertyAdditionEntity addition) {
        addition.setProperty(this);
        additions.add(addition);
    }

    public void cleanAdditions() {
        additions.clear();
    }

    public void cleanAdditions(String key) {
        final var toRemove = additions.stream().filter(x -> key.equals(x.getKey())).collect(Collectors.toList());
        additions.removeAll(toRemove);
    }
}

package ch.shield.auth.repository.jdbc.wrapper;

import ch.shield.auth.core.repository.Property;
import ch.shield.auth.core.repository.User;
import ch.shield.auth.repository.jdbc.JdbcRepositoryException;
import ch.shield.auth.repository.jdbc.WrapperFactory;
import ch.shield.auth.repository.jdbc.db.PropertyAdditionEntity;
import ch.shield.auth.repository.jdbc.db.PropertyEntity;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

class PropertyWrapper implements Property {
    final static String NAME_PROPERTY = "name";
    final PropertyEntity propertyEntity;
    private WrapperFactory wrapperFactory;


    PropertyWrapper(PropertyEntity propertyEntity) {
        this.propertyEntity = propertyEntity;
    }

    List<String> getAdditionValues(String key) {
        return propertyEntity.getAdditions().stream()
                .filter(x -> key.equals(x.getKey()))
                .map(PropertyAdditionEntity::getValue)
                .collect(Collectors.toList());
    }

    Optional<String> getOptionalAdditionValue(String key) {
        return propertyEntity.getAdditions().stream()
                .filter(x -> key.equals(x.getKey()))
                .map(PropertyAdditionEntity::getValue)
                .findFirst();
    }

    String getAdditionValue(String key) {
        return getOptionalAdditionValue(key).orElseThrow(() -> JdbcRepositoryException.neededAdditionNotFound(key));
    }

    @Autowired
    public void setWrapperFactory(WrapperFactory wrapperFactory) {
        this.wrapperFactory = wrapperFactory;
    }

    @Override
    public User getUser() {
        return wrapperFactory.wrapUser(propertyEntity.getUser());
    }

    @Override
    public long getId() {
        return propertyEntity.getId();
    }
}

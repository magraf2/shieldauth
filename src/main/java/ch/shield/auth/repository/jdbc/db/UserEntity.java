package ch.shield.auth.repository.jdbc.db;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity(name = "user")
@Getter
@NoArgsConstructor
public class UserEntity {
    @Id
    @GeneratedValue
    @NotNull
    private Long id;

    @NotNull
    @Setter
    private String userId;

    @Setter
    private boolean locked;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<RoleEntity> roles;
}

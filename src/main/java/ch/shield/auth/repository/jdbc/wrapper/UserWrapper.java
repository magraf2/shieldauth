package ch.shield.auth.repository.jdbc.wrapper;

import ch.shield.auth.core.repository.Property;
import ch.shield.auth.core.repository.User;
import ch.shield.auth.repository.jdbc.WrapperFactory;
import ch.shield.auth.repository.jdbc.db.PropertyEntity;
import ch.shield.auth.repository.jdbc.db.PropertyRepository;
import ch.shield.auth.repository.jdbc.db.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

public class UserWrapper implements User {
    private final UserEntity userEntity;
    private PropertyRepository propertyRepository;
    private WrapperFactory wrapperFactory;

    public UserWrapper(UserEntity userEntity) {
        this.userEntity = userEntity;
    }

    @Autowired
    public void setPropertyRepository(PropertyRepository propertyRepository) {
        this.propertyRepository = propertyRepository;
    }

    @Autowired
    public void serWrapperFactory(WrapperFactory wrapperFactory) {
        this.wrapperFactory = wrapperFactory;
    }

    @Override
    public String getId() {
        return userEntity.getUserId();
    }

    @Override
    public boolean isLocked() {
        return userEntity.isLocked();
    }

    @Override
    public <T extends Property> List<T> getProperties(Class<T> type) {
        final var propertyEntities = propertyRepository.findByUserAndType(userEntity, type.getName());
        return propertyEntities.stream()
                .map(wrapperFactory::wrapProperty)
                .map(type::cast)
                .collect(Collectors.toList());
    }

    @Override
    public <T extends Property> T createProperty(Class<T> type) {
        final var propertyEntity = new PropertyEntity(type, userEntity);
        propertyRepository.save(propertyEntity);
        final var wrapper = wrapperFactory.wrapProperty(propertyEntity);
        return type.cast(wrapper);
    }

    @Override
    public void removeProperty(Property property) {
        final var propertyEntity = propertyRepository.findById(property.getId());
        propertyEntity.ifPresent(propertyRepository::delete);
    }
}

package ch.shield.auth.repository.jdbc.wrapper;

import ch.shield.auth.core.repository.EmailProperty;
import ch.shield.auth.repository.jdbc.db.PropertyAdditionEntity;
import ch.shield.auth.repository.jdbc.db.PropertyEntity;
import ch.shield.auth.repository.jdbc.db.PropertyRepository;
import org.springframework.beans.factory.annotation.Autowired;

@SuppressWarnings("unused")
public class EmailWrapper extends PropertyWrapper implements EmailProperty {
    private final static String KEY_USAGE = "usage";
    private PropertyRepository propertyRepository;

    public EmailWrapper(PropertyEntity propertyEntity) {
        super(propertyEntity);
    }

    @Autowired
    public void setPropertyRepository(PropertyRepository propertyRepository) {
        this.propertyRepository = propertyRepository;
    }

    @Override
    public String getEmail() {
        return propertyEntity.getValue();
    }

    @Override
    public void setEmail(String eMail) {
        propertyEntity.setValue(eMail);
        propertyRepository.save(propertyEntity);
    }

    @Override
    public String getUsage() {
        return getOptionalAdditionValue(KEY_USAGE).orElse(null);
    }

    @Override
    public void setUsage(String usage) {
        propertyEntity.cleanAdditions(KEY_USAGE);
        propertyEntity.addAddition(new PropertyAdditionEntity(KEY_USAGE, usage));
        propertyRepository.save(propertyEntity);
    }
}

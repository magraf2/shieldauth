package ch.shield.auth.repository.jdbc.db;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PropertyRepository extends CrudRepository<PropertyEntity, Long> {
    List<PropertyEntity> findByTypeAndValue(String name, String value);

    List<PropertyEntity> findByUserAndType(UserEntity userWrapper, String name);
}

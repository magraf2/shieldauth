package ch.shield.auth.repository.jdbc;

public class JdbcRepositoryException extends RuntimeException {

    private JdbcRepositoryException(String message, Throwable cause) {
        super(message, cause);
    }

    static JdbcRepositoryException wrappingException(String type, ReflectiveOperationException e) {
        return new JdbcRepositoryException("Cannot wrap type " + type + ": " + e.getMessage(), e);
    }

    static JdbcRepositoryException noClassDefined(String type) {
        return new JdbcRepositoryException("This repository does not support property of type " + type + ".", null);
    }

    public static JdbcRepositoryException readFormatException(Exception e) {
        return new JdbcRepositoryException("Could not read value from database: " + e.getMessage(), e);
    }

    public static JdbcRepositoryException neededAdditionNotFound(String key) {
        return new JdbcRepositoryException("No addition '" + key + "' found, but such an addition is needed", null);
    }

    static JdbcRepositoryException userExists(String userId) {
        return new JdbcRepositoryException("A user with id  '" + userId + "' already exists", null);
    }
}

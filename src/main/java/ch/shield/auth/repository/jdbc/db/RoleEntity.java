package ch.shield.auth.repository.jdbc.db;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity(name = "role")
@Getter
@NoArgsConstructor
public class RoleEntity {
    @Id
    @GeneratedValue
    @NotNull
    private Long id;

    @NotNull
    @Setter
    private String name;
}

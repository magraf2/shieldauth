package ch.shield.auth.repository.jdbc.wrapper;

import ch.shield.auth.repository.base.TotpValidator;
import ch.shield.auth.repository.jdbc.db.PropertyEntity;
import ch.shield.auth.state.otp.OtpCredential;
import org.springframework.beans.factory.annotation.Value;

import java.time.LocalDateTime;

@SuppressWarnings("unused")
public class TotpWrapper extends PropertyWrapper implements OtpCredential {
    private int tolerance;
    private int digits;

    public TotpWrapper(PropertyEntity propertyEntity) {
        super(propertyEntity);
    }

    @Value("${repository.jdbc.totp.tolerance:3}")
    public void setTolerance(int tolerance) {
        this.tolerance = tolerance;
    }

    @Value("${repository.jdbc.totp.digits:8}")
    public void setDigits(int digits) {
        this.digits = digits;
    }

    @Override
    public String getName() {
        return getAdditionValue(NAME_PROPERTY);
    }

    @Override
    public boolean isValid() {
        //TODO Keep track of failed authentications, lock credentials
        return true;
    }

    @Override
    public boolean check(String otp) {
        TotpValidator validator = new TotpValidator();
        validator.setDigits(digits);
        validator.setSharedSecretKey(propertyEntity.getValue());
        validator.setTime(LocalDateTime.now());
        validator.setTolerance(tolerance);
        return validator.validate(otp);
    }
}

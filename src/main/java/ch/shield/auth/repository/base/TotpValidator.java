package ch.shield.auth.repository.base;

import com.google.common.base.Charsets;
import lombok.Data;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Data
public class TotpValidator {
    public final static String SHA1 = "HmacSHA1";
    public final static String SHA256 = "HmacSHA256";
    public final static String SHA512 = "HmacSHA512";
    public int timeStep = 30;
    private int digits = 8;
    private String sharedSecretKey;
    private LocalDateTime time;
    private int tolerance = 2;
    private String algorithm = SHA1;

    private static byte[] convertToByteArray(final long i) {
        final ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.putLong(0, i);
        return buffer.array();
    }

    private static int dynamicTruncate(byte[] hs) {
        //Convert byte string to 31 bit string
        final int offset = hs[hs.length - 1] & 0xf;
        return ((hs[offset] & 0x7f) << 24) | ((hs[offset + 1] & 0xff) << 16) | ((hs[offset + 2] & 0xff) << 8) | (hs[offset + 3] & 0xff);
    }

    public boolean validate(final String otp) {
        final byte[] byteKey = sharedSecretKey.getBytes(Charsets.US_ASCII);
        final long tc = time.toEpochSecond(ZoneOffset.UTC) / timeStep;
        final int otpAsInt = Integer.parseInt(otp);

        for (long counter = tc - tolerance; counter <= tc + tolerance; counter++) {
            final int totp = computeHotp(byteKey, counter);
            if (totp == otpAsInt) {
                return true;
            }
        }
        return false;
    }

    public int generate() {
        final byte[] byteKey = sharedSecretKey.getBytes(Charsets.US_ASCII);
        final long tc = time.toEpochSecond(ZoneOffset.UTC) / timeStep;
        return computeHotp(byteKey, tc);
    }

    private int computeHotp(final byte[] keyBytes, final long counter) {
        // Compute HOTP as in section 5.3 of RFC 4226.
        final byte[] msg = convertToByteArray(counter);
        final byte[] hs = computeHmac(keyBytes, msg);
        final int sNum = dynamicTruncate(hs);
        return sNum % ((int) Math.pow(10, digits));
    }

    private byte[] computeHmac(final byte[] keyBytes, final byte[] text) {
        try {
            final Mac hmac = Mac.getInstance(algorithm);
            final SecretKeySpec macKey = new SecretKeySpec(keyBytes, "RAW");
            hmac.init(macKey);
            return hmac.doFinal(text);
        } catch (GeneralSecurityException e) {
            throw new RuntimeException("Cannot generate HOTP value: " + e.getMessage(), e);
        }
    }
}

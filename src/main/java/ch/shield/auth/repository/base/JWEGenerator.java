package ch.shield.auth.repository.base;

import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.RSAEncrypter;
import lombok.Builder;
import lombok.NonNull;
import lombok.Singular;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

import java.security.interfaces.RSAPublicKey;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Map;

public class JWEGenerator {
    @Builder
    static String generate(@NonNull final RSAPublicKey key, final String issuer, final String subject, @Singular final List<String> audiences, final LocalDateTime expirationTime, final LocalDateTime notBefore, final LocalDateTime issuedAt, final String id, @Singular final Map<String, String> additionalClaims) {
        final var jsonObject = new JSONObject();
        if (issuer != null) {
            jsonObject.appendField("iss", issuer);
        }
        if (subject != null) {
            jsonObject.appendField("sub", subject);
        }
        if (audiences != null && audiences.size() > 0) {
            final var jsonArray = new JSONArray();
            audiences.forEach(jsonArray::appendElement);
            jsonObject.appendField("aud", jsonArray);
        }
        if (expirationTime != null) {
            jsonObject.appendField("exp", expirationTime.toEpochSecond(ZoneOffset.UTC));
        }
        if (notBefore != null) {
            jsonObject.appendField("nbf", notBefore.toEpochSecond(ZoneOffset.UTC));
        }
        if (notBefore != null) {
            jsonObject.appendField("iat", issuedAt.toEpochSecond(ZoneOffset.UTC));
        }
        if (id != null) {
            jsonObject.appendField("jti", id);
        }
        if (additionalClaims != null) {
            additionalClaims.forEach(jsonObject::appendField);
        }
        final var payload = new Payload(jsonObject);

        try {
            final var header = new JWEHeader(JWEAlgorithm.RSA_OAEP_256, EncryptionMethod.A256GCM);
            final var jweObject = new JWEObject(header, payload);
            jweObject.encrypt(new RSAEncrypter(key));
            return jweObject.serialize();
        } catch (Exception e) {
            throw new RuntimeException("Cannot generate JWE:" + e.getMessage(), e);
        }
    }
}
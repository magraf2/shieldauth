package ch.shield.auth.config;

import ch.shield.auth.core.config.Flow;
import ch.shield.auth.core.state.State;

public class FlowInitializeException extends RuntimeException {

    private FlowInitializeException(String message) {
        super(message);
    }

    private FlowInitializeException(String message, Throwable e) {
        super(message, e);
    }

    static FlowInitializeException couldNotCast(String typeName, Class<?> baseType, ClassCastException e) {
        final var message = String.format("Could not cast instance of type '%s' to '%s'", typeName, baseType.getName());
        return new FlowInitializeException(message, e);
    }

    static FlowInitializeException couldNotCreate(String typeName, Exception e) {
        final var message = String.format("Could not create instance of for type '%s'", typeName);
        return new FlowInitializeException(message, e);
    }

    static FlowInitializeException classNotFound(String typeName, ClassNotFoundException e) {
        final var message = String.format("Could not load class'%s'", typeName);
        return new FlowInitializeException(message, e);
    }

    public static FlowInitializeException missingProperty(Class<? extends Flow> type, String property) {
        final var message = String.format("A config of type '%s' must have a property '%s' but it has not", type, property);
        return new FlowInitializeException(message);
    }

    public static FlowInitializeException invalidProperty(String property, String errorMessage) {
        final var message = String.format("The property '%s' is invalid: %s", property, errorMessage);
        return new FlowInitializeException(message);
    }

    static FlowInitializeException noRepository(Flow flow) {
        final var message = String.format("No repository is defined for config '%s'", flow.getName());
        return new FlowInitializeException(message);
    }

    static FlowInitializeException transitionNotDefined(State lastState, String lastTransition) {
        final var message = String.format("No transition from '%s' from start '%s' is defined", lastTransition, lastState.getName());
        return new FlowInitializeException(message);
    }
}



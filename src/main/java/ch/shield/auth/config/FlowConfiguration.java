package ch.shield.auth.config;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.Singular;

import java.util.Map;
import java.util.Properties;

@Data
@Builder
public class FlowConfiguration {
    @NonNull
    @Singular
    private final Map<String, String> properties;
    @NonNull
    private final String name;
    @NonNull
    private final String startState;
    @NonNull
    private final String repository;
    @NonNull
    private final String typeName;

    public Properties getProperties() {
        final var ret = new Properties();
        ret.putAll(properties);
        return ret;
    }
}

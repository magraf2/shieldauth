package ch.shield.auth.config;

import ch.shield.auth.config.xml.FileHandler;
import ch.shield.auth.core.LoggerConstants;
import ch.shield.auth.core.config.ConfigFile;
import ch.shield.auth.core.config.Flow;
import ch.shield.auth.core.config.FlowInitialization;
import ch.shield.auth.core.repository.Repository;
import ch.shield.auth.core.repository.RepositoryInitialization;
import ch.shield.auth.core.state.State;
import ch.shield.auth.core.state.StateInitialization;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class ConfigFileImpl implements ConfigFile {
    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(LoggerConstants.LOGGER_CONFIG);

    private final Map<State, Map<String, State>> transitions;
    private final Map<String, State> states;
    private final Map<String, Flow> flows;
    private final Map<Flow, Repository> repositories;
    private final AutowireCapableBeanFactory autowireCapableBeanFactory;
    private final Map<Flow, State> startStates;

    public ConfigFileImpl(@Value("${engine.configFile:flow.xml}") String configFile, @Autowired AutowireCapableBeanFactory autowireCapableBeanFactory) {
        this.autowireCapableBeanFactory = autowireCapableBeanFactory;
        final var fileReader = new FileHandler(configFile);
        fileReader.read();
        states = fileReader.getStates().stream()
                .map(this::createState)
                .collect(Collectors.collectingAndThen(
                        Collectors.toMap(State::getName, Function.identity()), Collections::unmodifiableMap));
        transitions = fileReader.getStates().stream()
                .collect(Collectors.collectingAndThen(
                        Collectors.toMap(x -> states.get(x.getName()), this::createTransitionsForState), Collections::unmodifiableMap));
        flows = fileReader.getFlows().stream()
                .map(this::createFlow)
                .collect(Collectors.collectingAndThen(
                        Collectors.toMap(Flow::getName, x -> x), Collections::unmodifiableMap));
        final var repositoriesTmp = fileReader.getRepositories().stream()
                .collect(Collectors.toMap(RepositoryConfiguration::getName, this::createRepository));
        repositories = fileReader.getFlows().stream()
                .collect(Collectors.collectingAndThen(
                        Collectors.toMap(x -> flows.get(x.getName()), x -> repositoriesTmp.get(x.getRepository())), Collections::unmodifiableMap));
        startStates = fileReader.getFlows().stream()
                .collect(Collectors.collectingAndThen(
                        Collectors.toMap(x -> flows.get(x.getName()), x -> states.get(x.getStartState())), Collections::unmodifiableMap));
    }

    private State createState(final StateConfiguration stateConfiguration) {
        final var bean = createBean(stateConfiguration.getTypeName(), State.class);
        final var stateInitialization = StateInitialization.builder()
                .name(stateConfiguration.getName())
                .properties(stateConfiguration.getProperties())
                .build();
        bean.initialize(stateInitialization);
        LOG.info("Created state '{}' of type '{}'", bean.getName(), bean.getClass());
        LOG.debug("State is {}", bean);
        return bean;
    }

    private Map<String, State> createTransitionsForState(final StateConfiguration stateConfiguration) {
        return stateConfiguration.getTransitions().entrySet().stream()
                .peek(x -> LOG.info("Added transition '{}' from state '{}' to state '{}'", x.getKey(), stateConfiguration.getName(), x.getValue()))
                .collect(Collectors.collectingAndThen(
                        Collectors.toMap(Map.Entry::getKey, x -> states.get(x.getValue())), Collections::unmodifiableMap));
    }

    private Repository createRepository(RepositoryConfiguration repositoryConfiguration) {
        final var typeName = repositoryConfiguration.getTypeName();
        final var repository = createBean(typeName, Repository.class);
        final var repositoryInitialization = RepositoryInitialization.builder()
                .name(repositoryConfiguration.getName())
                .properties(repositoryConfiguration.getProperties())
                .build();
        repository.initialize(repositoryInitialization);
        LOG.info("Created repository of type '{}'", repository.getClass());
        LOG.debug("Repository is {}", repository);
        return repository;
    }

    private Flow createFlow(FlowConfiguration flowConfiguration) {
        final var typeName = flowConfiguration.getTypeName();
        final var flow = createBean(typeName, Flow.class);
        final var flowInitialization = FlowInitialization.builder()
                .name(flowConfiguration.getName())
                .properties(flowConfiguration.getProperties())
                .build();
        flow.initialize(flowInitialization);
        LOG.info("Created config '{}' of type '{}'", flow.getName(), flow.getClass());
        LOG.debug("Flow is ", flow);
        return flow;
    }

    private <T> T createBean(String typeName, Class<T> baseType) {
        try {
            final var type = ConfigFileImpl.class.getClassLoader().loadClass(typeName);
            final var instance = autowireCapableBeanFactory.createBean(type);
            return baseType.cast(instance);
        } catch (ClassNotFoundException e) {
            throw FlowInitializeException.classNotFound(typeName, e);
        } catch (BeansException e) {
            throw FlowInitializeException.couldNotCreate(typeName, e);
        } catch (ClassCastException e) {
            throw FlowInitializeException.couldNotCast(typeName, baseType, e);
        } catch (Exception e) {
            throw FlowInitializeException.couldNotCreate(typeName, e);
        }
    }

    @Override
    public State getNextState(@NotNull final Flow flow, final State lastState, final String lastTransition) {
        if (lastState == null) {
            final var nextState = startStates.get(flow);
            LOG.info("No previous state, start with start state {} for config {}", nextState.getName(), flow.getName());
            return nextState;
        }
        if (lastTransition == null) {
            LOG.info("No transition set, continue with state {}", lastState.getName());
            return lastState;
        }
        final var nextState = transitions.get(lastState).get(lastTransition);
        if (nextState == null) {
            throw FlowInitializeException.transitionNotDefined(lastState, lastTransition);
        }
        LOG.info("Transition {} to state {}", lastTransition, nextState.getName());
        return nextState;
    }

    @Override
    public <T extends Flow> List<T> getFlows(@NotNull Class<T> type) {
        return flows.values().stream()
                .filter(x -> type.isAssignableFrom(x.getClass()))
                .map(type::cast)
                .collect(Collectors.toList());
    }

    @Override
    public <T extends Flow> Optional<T> getFlow(@NotNull String name, @NotNull Class<T> type) {
        final var flow = flows.get(name);
        if (flow == null) {
            return Optional.empty();
        }
        return Optional.of(type.cast(flow));
    }

    @Override
    public Repository getRepository(@NotNull Flow flow) {
        final var repository = repositories.get(flow);
        if (repository == null) {
            throw FlowInitializeException.noRepository(flow);
        }
        return repository;
    }
}

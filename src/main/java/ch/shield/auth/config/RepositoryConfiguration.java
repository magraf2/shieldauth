package ch.shield.auth.config;

import lombok.*;

import java.util.Map;
import java.util.Properties;

@Data
@RequiredArgsConstructor
@Builder
public class RepositoryConfiguration {
    @NonNull
    @Singular
    private final Map<String, String> properties;
    @NonNull
    private final String name;
    @NonNull
    private final String typeName;

    public Properties getProperties() {
        final var ret = new Properties();
        ret.putAll(properties);
        return ret;
    }
}

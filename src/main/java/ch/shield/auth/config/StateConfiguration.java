package ch.shield.auth.config;

import lombok.*;

import java.util.Map;
import java.util.Properties;

@Data
@RequiredArgsConstructor
@Builder
public class StateConfiguration {
    @NonNull
    private final String name;
    @NonNull
    private final String typeName;
    @NonNull
    @Singular
    private final Map<String, String> properties;
    @NonNull
    @Singular
    private final Map<String, String> transitions;

    public Properties getProperties() {
        final var ret = new Properties();
        ret.putAll(properties);
        return ret;
    }
}

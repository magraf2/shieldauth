package ch.shield.auth.config.xml;

public class XmlFileReadException extends RuntimeException {

    private XmlFileReadException(String message) {
        super(message);
    }

    private XmlFileReadException(String message, Throwable e) {
        super(message, e);
    }

    static XmlFileReadException fileException(Throwable e, String filename) {
        final var message = String.format("Could not read config %s: %s", filename, e.getMessage());
        return new XmlFileReadException(message, e);
    }

    static XmlFileReadException fileNotFound(String filename) {
        final var message = String.format("Could not read config %s as it does not exist", filename);
        return new XmlFileReadException(message);
    }
}



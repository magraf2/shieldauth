package ch.shield.auth.config.xml;

import ch.shield.auth.config.ConfigFileImpl;
import ch.shield.auth.config.FlowConfiguration;
import ch.shield.auth.config.RepositoryConfiguration;
import ch.shield.auth.config.StateConfiguration;
import ch.shield.auth.core.LoggerConstants;
import lombok.Getter;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;

public class FileHandler extends DefaultHandler {
    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(LoggerConstants.LOGGER_CONFIG);
    private final static String TAG_CONFIG = "config";
    private final static String TAG_FLOW = "flow";
    private final static String TAG_STATE = "state";
    private final static String TAG_REPOSITORY = "repository";
    private final static String TAG_PROPERTY = "property";
    private final static String TAG_TRANSITION = "transition";
    private final static String ATTRIBUTE_NAME = "name";
    private final static String ATTRIBUTE_TARGET = "target";
    private final static String ATTRIBUTE_VALUE = "value";
    private final static String ATTRIBUTE_CLASS = "class";
    private static final String ATTRIBUTE_START_STATE = "startState";
    private static final String ATTRIBUTE_REPOSITORY = "repository";
    private final String configFile;
    @Getter
    private final List<StateConfiguration> states = new LinkedList<>();
    @Getter
    private final List<FlowConfiguration> flows = new LinkedList<>();
    @Getter
    private final List<RepositoryConfiguration> repositories = new LinkedList<>();
    private StateConfiguration.StateConfigurationBuilder stateBuilder = null;
    private FlowConfiguration.FlowConfigurationBuilder flowBuilder = null;
    private RepositoryConfiguration.RepositoryConfigurationBuilder repositoryBuilder = null;
    private boolean isStarted = false;
    private boolean isFinished = false;

    public FileHandler(String configFile) {
        this.configFile = configFile;
    }

    private static File getFile(String filename) {
        final var diskFile = new File(filename);
        if (diskFile.exists()) {
            return diskFile;
        }

        final var classLoader = ConfigFileImpl.class.getClassLoader();
        final var resource = classLoader.getResource(filename);
        if (resource == null) {
            throw XmlFileReadException.fileNotFound(filename);
        }
        try {
            final var fileUri = resource.toURI();
            final var fileCl = new File(fileUri);
            if (!fileCl.exists()) {
                throw XmlFileReadException.fileNotFound(filename);
            }
            return fileCl;
        } catch (URISyntaxException e) {
            throw XmlFileReadException.fileException(e, filename);
        }
    }

    public void read() {
        try {
            final var inputFile = getFile(configFile);
            LOG.info("Read config file {}", inputFile.getAbsolutePath());
            final var factory = SAXParserFactory.newInstance();
            final var saxParser = factory.newSAXParser();
            saxParser.parse(inputFile, this);
            LOG.info("Read {} repositories, {} flows and {} states", repositories.size(), flows.size(), states.size());
        } catch (IOException | ParserConfigurationException | SAXException e) {
            throw XmlFileReadException.fileException(e, configFile);
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        final var name = attributes.getValue(ATTRIBUTE_NAME);
        final var typeName = attributes.getValue(ATTRIBUTE_CLASS);
        final var startState = attributes.getValue(ATTRIBUTE_START_STATE);
        final var repository = attributes.getValue(ATTRIBUTE_REPOSITORY);
        final var target = attributes.getValue(ATTRIBUTE_TARGET);
        final var value = attributes.getValue(ATTRIBUTE_VALUE);

        if (isFinished) {
            throw new SAXException("There are multiple '" + TAG_CONFIG + "' elements.");
        }

        if (stateBuilder != null) {
            if (TAG_TRANSITION.equals(qName)) {
                stateBuilder.transition(name, target);
                return;
            }
            if (TAG_PROPERTY.equals(qName)) {
                stateBuilder.property(name, value);
                return;
            }
            throw new SAXException("Unknown element '" + qName + "' in '" + TAG_STATE + "'");
        }

        if (flowBuilder != null) {
            if (TAG_PROPERTY.equals(qName)) {
                flowBuilder.property(name, value);
                return;
            }
            throw new SAXException("Unknown element '" + qName + "' in '" + TAG_FLOW + "'");
        }

        if (repositoryBuilder != null) {
            if (TAG_PROPERTY.equals(qName)) {
                repositoryBuilder.property(name, value);
                return;
            }
            throw new SAXException("Unknown element '" + qName + "' in '" + TAG_REPOSITORY + "'");
        }

        if (isStarted) {
            if (TAG_STATE.equals(qName)) {
                stateBuilder = StateConfiguration.builder().name(name).typeName(typeName);
                return;
            }

            if (TAG_REPOSITORY.equals(qName)) {
                repositoryBuilder = RepositoryConfiguration.builder().name(name).typeName(typeName);
                return;
            }

            if (TAG_FLOW.equals(qName)) {
                flowBuilder = FlowConfiguration.builder().name(name).typeName(typeName).repository(repository).startState(startState);
                return;
            }
            throw new SAXException("Unknown element '" + qName + "' in '" + TAG_CONFIG + "'");
        }

        if (TAG_CONFIG.equals(qName)) {
            isStarted = true;
            return;
        }
        throw new SAXException("Root element must be '" + TAG_CONFIG + "' not '" + qName + "'");
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        if (TAG_CONFIG.equals(qName)) {
            isFinished = true;
            return;
        }
        if (TAG_FLOW.equals(qName)) {
            final var flow = flowBuilder.build();
            if (LOG.isDebugEnabled()) {
                LOG.debug("Read in config: " + flow.toString());
            }
            flowBuilder = null;
            flows.add(flow);
            return;
        }
        if (TAG_STATE.equals(qName)) {
            final var state = stateBuilder.build();
            if (LOG.isDebugEnabled()) {
                LOG.debug("Read in state: " + state.toString());
            }
            stateBuilder = null;
            states.add(state);
            return;
        }
        if (TAG_REPOSITORY.equals(qName)) {
            final var repository = repositoryBuilder.build();
            if (LOG.isDebugEnabled()) {
                LOG.debug("Read in repository: " + repository.toString());
            }
            repositoryBuilder = null;
            repositories.add(repository);
            return;
        }
        LOG.debug("Closed tag '" + qName + "'");
    }
}

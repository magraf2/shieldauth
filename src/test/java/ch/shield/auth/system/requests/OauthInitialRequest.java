package ch.shield.auth.system.requests;

import ch.shield.auth.system.utils.HttpRequestFactory;
import lombok.Builder;
import lombok.NonNull;
import org.junit.Assert;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;


public class OauthInitialRequest {
    private static final String PATH = "/auth/oauth2/authenticate";
    private final ResponseEntity<String> response;

    @Builder
    private OauthInitialRequest(@NonNull final String responseType, @NonNull final String clientId, @NonNull final String redirectUri, final int port) {
        final var url = UriComponentsBuilder.newInstance()
                .scheme("http")
                .host("localhost")
                .port(port)
                .path(PATH)
                .queryParam("response_type", responseType)
                .queryParam("client_id", clientId)
                .queryParam("redirect_uri", redirectUri)
                .fragment(null)
                .build().toString();
        final var template = new RestTemplate(new HttpRequestFactory());
        response = template.getForEntity(url, String.class);
    }

    public JsonAuthRequest followJsonRedirect() {
        Assert.assertEquals(302, response.getStatusCodeValue());
        final var locationHeader = response.getHeaders().getLocation();
        Assert.assertNotNull(locationHeader);
        Assert.assertEquals("/auth/authenticate", locationHeader.getPath());
        final var loginCookie = getLoginCookie();
        return new JsonAuthRequest(locationHeader, loginCookie);
    }

    private String getLoginCookie() {
        final var setCookieHeader = response.getHeaders().get("Set-Cookie");
        Assert.assertNotNull(setCookieHeader);
        return setCookieHeader.stream()
                .map(x -> x.split(";")[0])
                .map(x -> x.split("="))
                .filter(x -> x[0].equals("login"))
                .map(x -> x[1])
                .findFirst()
                .orElseThrow(() -> new AssertionError("No login Cookie found"));
    }
}

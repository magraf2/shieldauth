package ch.shield.auth.system.requests;

import ch.shield.auth.repository.base.TotpValidator;
import ch.shield.auth.system.utils.HttpRequestFactory;
import ch.shield.auth.system.utils.Utils;
import lombok.Builder;
import org.json.JSONException;
import org.junit.Assert;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.Map;

public class JsonAuthRequest {
    private final String loginCookie;
    private final URI uri;
    private ResponseEntity<String> response;

    @Builder
    public JsonAuthRequest(URI uri, String loginCookie) {
        this.loginCookie = loginCookie;
        this.uri = uri;
        final var entity = Utils.getJSONEntity(loginCookie);
        final var template = new RestTemplate(new HttpRequestFactory());
        response = template.exchange(uri, HttpMethod.GET, entity, String.class);

        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertEquals("{\"message\":\"Please enter username and password\"}", response.getBody());
    }

    private static String generateTotp(String totpKey) {
        final var validator = new TotpValidator();
        validator.setDigits(8);
        validator.setSharedSecretKey(totpKey);
        validator.setTime(LocalDateTime.now());
        return Integer.toString(validator.generate());
    }

    public void sendUsernamePassword(String username, String password) throws JSONException {
        final var entity = Utils.getJSONEntity(Map.of("userid", username, "password", password), loginCookie);
        final var template = new RestTemplate(new HttpRequestFactory());
        response = template.exchange(uri, HttpMethod.POST, entity, String.class);

        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertEquals("{\"message\":\"Please enter OTP\"}", response.getBody());
    }

    public void sendTotpToken(String totpKey) throws JSONException {
        final var totp = generateTotp(totpKey);
        final var entity = Utils.getJSONEntity(Map.of("otp", totp), loginCookie);
        final var template = new RestTemplate(new HttpRequestFactory());
        response = template.exchange(uri, HttpMethod.POST, entity, String.class);
    }

    public OauthFinalRequest followOauthRedirect() {
        Assert.assertEquals(302, response.getStatusCodeValue());
        final var locationHeader = response.getHeaders().getLocation();
        Assert.assertNotNull(locationHeader);
        Assert.assertEquals("/auth/oauth2/final", locationHeader.getPath());
        return new OauthFinalRequest(locationHeader, loginCookie);
    }
}

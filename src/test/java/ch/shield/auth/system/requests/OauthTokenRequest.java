package ch.shield.auth.system.requests;

import ch.shield.auth.system.utils.HttpRequestFactory;
import ch.shield.auth.system.utils.Utils;
import com.nimbusds.jose.JWEObject;
import lombok.Builder;
import lombok.NonNull;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.Assert;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.text.ParseException;
import java.util.Map;

public class OauthTokenRequest {
    private static final String PATH = "/auth/oauth2/token";
    private final ResponseEntity<String> response;

    @Builder
    private OauthTokenRequest(@NonNull final String grantType, @NonNull final String clientId, @NonNull final String redirectUri, final int port, @NonNull final String code, @NonNull final String clientSecret) {
        final var entity = Utils.getEntity(Map.of("grant_type", grantType, "code", code, "redirect_uri", redirectUri, "client_id", clientId, "client_secret", clientSecret));
        final var uri = UriComponentsBuilder.newInstance()
                .scheme("http")
                .host("localhost")
                .port(port)
                .path(PATH)
                .build().toString();
        final var template = new RestTemplate(new HttpRequestFactory());
        response = template.exchange(uri, HttpMethod.POST, entity, String.class);
    }

    public JWEObject getAccessToken() {
        Assert.assertEquals(200, response.getStatusCodeValue());
        try {
            final var jsonString = response.getBody();
            final var jsonCode = new JSONObject(new JSONTokener(jsonString));
            Assert.assertEquals("bearer", jsonCode.getString("token_type"));
            Assert.assertFalse(jsonCode.has("refresh_token"));
            Assert.assertTrue(jsonCode.has("expires_in"));

            final var accessToken = jsonCode.getString("access_token");
            return JWEObject.parse(accessToken);
        } catch (JSONException | ParseException e) {
            throw new AssertionError("Could not parse json response", e);
        }
    }
}

package ch.shield.auth.system.requests;

import ch.shield.auth.system.utils.HttpRequestFactory;
import ch.shield.auth.system.utils.Utils;
import org.junit.Assert;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public class OauthFinalRequest {
    private final URI uri;
    private final ResponseEntity<String> response;

    OauthFinalRequest(final URI uri, final String loginCookie) {
        this.uri = uri;
        final var entity = Utils.getJSONEntity(loginCookie);
        final var template = new RestTemplate(new HttpRequestFactory());
        response = template.exchange(uri, HttpMethod.POST, entity, String.class);
    }

    private static String getBaseUrl(URI uri) {
        final var sb = new StringBuilder();
        sb.append(uri.getScheme());
        sb.append("://");
        sb.append(uri.getHost());
        if (uri.getPort() != -1) {
            sb.append(":");
            sb.append(uri.getPort());
        }
        sb.append(uri.getPath());
        return sb.toString();
    }

    private static Map<String, String> parseQueryString(URI uri) {
        return Arrays.stream(uri.getQuery().split("&"))
                .map(x -> x.split("="))
                .collect(Collectors.toMap(x -> x[0], x -> x[1]));
    }

    public OauthTokenRequest getTokenRequest(String redirectUri) {
        Assert.assertEquals(302, response.getStatusCodeValue());

        final var locationHeader = response.getHeaders().getLocation();
        Assert.assertNotNull(locationHeader);
        Assert.assertEquals(redirectUri, getBaseUrl(locationHeader));

        final var queryString = parseQueryString(locationHeader);
        final var code = queryString.get("code");
        Assert.assertNotNull("No code grand", code);
        return OauthTokenRequest.builder()
                .clientId("1234")
                .clientSecret("secret")
                .code(code)
                .grantType("authorization_code")
                .port(uri.getPort())
                .redirectUri(redirectUri)
                .build();
    }
}

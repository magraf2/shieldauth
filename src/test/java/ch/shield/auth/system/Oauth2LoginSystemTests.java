package ch.shield.auth.system;


import ch.shield.auth.repository.jdbc.db.PropertyEntity;
import ch.shield.auth.repository.jdbc.db.PropertyRepository;
import ch.shield.auth.repository.jdbc.db.UserEntity;
import ch.shield.auth.repository.jdbc.db.UserRepository;
import ch.shield.auth.state.otp.OtpCredential;
import ch.shield.auth.state.password.PasswordCredential;
import ch.shield.auth.system.requests.OauthInitialRequest;
import com.nimbusds.jose.crypto.RSADecrypter;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class Oauth2LoginSystemTests {

    @LocalServerPort
    private int port;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PropertyRepository propertyRepository;

    private static RSAPrivateKey getKey() throws NoSuchAlgorithmException, InvalidKeySpecException {
        @SuppressWarnings("SpellCheckingInspection") final var privateKeyBytes = Base64.getDecoder().decode("MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQCLoHrSw+fPPbWHWnjznk7NOF188la9XP7albgAEVgAj6dsNGpbjK6hIK8KG51Uf6VbWX3GDpZmvTE+jwn8v3HgRsB82e45k+3e55pwdLha5xy0QmIQHcaciG4NtrH2OqxAVQBmEMpLavXXcIz/nsQcjvfOJBex3oZoeHVitW6VIhph1+yNvhWgeWhCMrXimMPR9UR/GnTj1UTI+nDMWZNy4H75Rn1BBLVr6dgzdlPI6riZLUpdK8m2epKK04ud6qJ54BdTyvtwCHceJGm8VexJvpC4lyt40rISNtu0JnPVtOCeexXiKwuCcwPDSXkxu8ekNOh58XlOniIvL17a13arAgMBAAECggEAfvnXGHreAWJ+BfRp0CqkQQ7EPg9ogFbk0UWcijs7i28Yc1eLbWwXlyJQ7GFGh4R5jQRqU8YKUq407U5JZ9d2KN5OyHuroCL+DFsU4LXUvZ41RVylL0DU0J9lbgU5+9FJuktSzH7yaPmBToXlQ8VPVSVTB91cyv2xv6nkHnsecHGv+z4mI/8uhGXL9Mc405HFGeKqKI66Czc1EQBvGTtrVJfTVSdHXCxLUexXZ/OZnmYN7xSWiS/LxMsj/qm6ko7cR2a7JIwHLeqYhjrWKy1AtHs11oHtvmsKuD33WIYT45LuQxuO1fS+AaAlTB1WTxls1hheYCgbTAm3tSsFDhLf+QKBgQDkf2n4NpZUWGFGVzMqt9RCZz/Tz1v6wMEO07OJ5kdg52cLtFpEj1Q9Nc/9qQRhAtcFaSxLBBu9ek/SVaQ1FKPwiR94tzScP6jwL0K+5P1M+rnYWzGK5sU8iXyCxAQMUiz0CEHoTgbWJMcKb/uhQx1U2cjQpHQoaVYsh5s5j0UvzQKBgQCcbrupEVQL9Eb8Iz7Q8SfNmMX6KLPIiDrJTBEwF+QSQIyuVKmkFckyKDp3j61GCiFFm47NYpRucSI2Z4cjheyEyU28yi6a+OLwF93N2TMxYws41QP71BuWBM9LHiG5UhilBXufmOBy9/TnyUlkQ6+ICIy1zGGZrQL6GugHcj0YVwKBgQDfURONbjRAHNx9Na4hOuatO2GrLgb0LDzCs+7NgI0UTTRtwQdT6EzpQdWUUQKhmXfyptaOPmaqKYcnx2GkKxcKd8mVXmCBsMn4bAgsr3pMKONHa+EgoufzgV96If7e19Vtb0CSvOd7lRgewFFKS2C4U90q5P9Nm52ICcSQXdC9pQKBgQCG8p/BAcnXZiu2Z1FRPjhwDFL9sw8k9yB/drd9HZ8Kf9TcrvyJKKIgOsP1UGTR/vsk6UUPuPMDM2WoOTdDVczr4BzHuELsPbjIFk6pva2D9UVBCcIeBKSnjg6lZqEBFelcspYq398ATxdevS4q6JkMaRO54U5xG+dtiriPc+VwkQKBgQCppDDbcITlqVveZLf05PgdXCnzZ9oOuTigie8vMfDK90mGorAN9oTNAhLGPASQ22v47o2mV20cxIxXHxfqPkGHmAHveIt7xpq7eRJfVeC8Q1/AGSm9bknMRbwUc3/GtR0yY6Higs2J1uaSdrf8eeaDKSQMinVplmhRjvxAPXbIeQ==");
        final var keyFactory = KeyFactory.getInstance("RSA");
        final var privateKeySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
        return (RSAPrivateKey) keyFactory.generatePrivate(privateKeySpec);
    }

    @Test
    public void testRequest() throws Exception {
        createUser();
        final var oauthInitialRequest = OauthInitialRequest.builder().clientId("1234").port(port).redirectUri("https://www.example.com/test").responseType("code").build();
        final var jsonRequest = oauthInitialRequest.followJsonRedirect();
        jsonRequest.sendUsernamePassword("test_user", "password");
        jsonRequest.sendTotpToken("12345");
        final var oauthFinalRequest = jsonRequest.followOauthRedirect();
        final var tokenRequest = oauthFinalRequest.getTokenRequest("https://www.example.com/test");
        final var accessToken = tokenRequest.getAccessToken();
        accessToken.decrypt(new RSADecrypter(getKey()));
        Assert.assertEquals("test_user", accessToken.getPayload().toJSONObject().getAsString("sub"));
    }

    private void createUser() {
        final var jdbcUser = new UserEntity();
        jdbcUser.setUserId("test_user");
        userRepository.save(jdbcUser);

        final var passwordEncoder = new BCryptPasswordEncoder(16, new SecureRandom());
        final var encodedPassword = passwordEncoder.encode("password");
        final var passwordProperty = new PropertyEntity(PasswordCredential.class, jdbcUser);
        passwordProperty.setValue(encodedPassword);
        propertyRepository.save(passwordProperty);

        final var totpProperty = new PropertyEntity(OtpCredential.class, jdbcUser);
        totpProperty.setValue("12345");
        propertyRepository.save(totpProperty);


        final var properties = propertyRepository.findByUserAndType(jdbcUser, PasswordCredential.class.getName());
        Assert.assertEquals(1, properties.size());
    }
}

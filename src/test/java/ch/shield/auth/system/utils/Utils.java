package ch.shield.auth.system.utils;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Utils {

    public static HttpEntity getJSONEntity(String loginCookie) {
        final var headers = new HttpHeaders();
        headers.add("Cookie", "login=" + loginCookie);
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(List.of(MediaType.APPLICATION_JSON));
        return new HttpEntity(new LinkedMultiValueMap<>(headers));
    }

    public static HttpEntity getEntity(Map<String, String> parameters) {
        final var body = parameters.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, x -> List.of(x.getValue())));
        return new HttpEntity<MultiValueMap<String, String>>(
                new LinkedMultiValueMap<>(body),
                new LinkedMultiValueMap<>());
    }

    public static HttpEntity getJSONEntity(Map<String, String> parameters, String loginCookie) throws JSONException {
        final var headers = new HttpHeaders();
        headers.add("Cookie", "login=" + loginCookie);
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(List.of(MediaType.APPLICATION_JSON));
        final var jsonObject = new JSONObject();
        for (Map.Entry<String, String> entry : parameters.entrySet()) {
            jsonObject.put(entry.getKey(), entry.getValue());
        }
        return new HttpEntity<>(jsonObject.toString(), new LinkedMultiValueMap<>(headers));
    }
}

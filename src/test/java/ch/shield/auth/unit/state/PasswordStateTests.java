package ch.shield.auth.unit.state;

import ch.shield.auth.core.repository.User;
import ch.shield.auth.core.state.*;
import ch.shield.auth.core.state.result.StateFinalException;
import ch.shield.auth.core.state.result.StateResult;
import ch.shield.auth.state.password.PasswordCredential;
import ch.shield.auth.state.password.PasswordState;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;
import java.util.Properties;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PasswordStateTests {

    private PasswordState target;

    @Mock
    private Context context;

    @Mock
    private User user;

    @Mock
    private PasswordCredential passwordProperty;

    @Before
    public void before() {
        target = new PasswordState();
        final var startInitialization = StateInitialization.builder()
                .name("test")
                .properties(new Properties())
                .build();
        target.initialize(startInitialization);

        when(context.getInput("password")).thenReturn(Optional.of("pass-word"));
        when(context.getUser()).thenReturn(Optional.of(user));
        when(user.getProperties(PasswordCredential.class)).thenReturn(List.of(passwordProperty));
        when(passwordProperty.isValid()).thenReturn(true);
        when(passwordProperty.check("pass-word")).thenReturn(true);
    }

    @Test(expected = StateRenderableException.class)
    public void noPassword() {
        when(context.getInput("password")).thenReturn(Optional.empty());

        target.execute(context);
    }

    @Test(expected = StateFinalException.class)
    public void userNotKnown() {
        when(context.getUser()).thenReturn(Optional.empty());

        target.execute(context);
    }

    @Test(expected = StateFinalException.class)
    public void userNoPassword() {
        when(user.getProperties(PasswordCredential.class)).thenReturn(List.of());

        target.execute(context);
    }

    @Test(expected = StateFinalException.class)
    public void notValid() {
        when(passwordProperty.isValid()).thenReturn(false);

        target.execute(context);
    }

    @Test(expected = StateRenderableException.class)
    public void wrongPassword() {
        when(passwordProperty.check("pass-word")).thenReturn(false);

        target.execute(context);
    }

    @Test()
    public void execute() {

        StateResult stateResult = target.execute(context);

        verify(passwordProperty, times(1)).check("pass-word");
        Assert.assertEquals(0, stateResult.getAttributes().size());
        Assert.assertFalse(stateResult.getRenderable().isPresent());
        Assert.assertTrue(stateResult.getTransition().isPresent());
        Assert.assertEquals("ok", stateResult.getTransition().get());
    }
}

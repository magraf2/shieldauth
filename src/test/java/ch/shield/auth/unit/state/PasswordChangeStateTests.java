package ch.shield.auth.unit.state;

import ch.shield.auth.core.repository.User;
import ch.shield.auth.core.state.Context;
import ch.shield.auth.core.state.result.StateFinalException;
import ch.shield.auth.core.state.StateInitialization;
import ch.shield.auth.state.password.PasswordChangeState;
import ch.shield.auth.state.password.PasswordCredential;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;
import java.util.Properties;

import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class PasswordChangeStateTests {

    private PasswordChangeState target;

    @Mock
    private Context context;

    @Mock
    private User user;

    @Mock
    private PasswordCredential passwordProperty;

    private Properties properties;

    @Before
    public void before() {
        properties = new Properties();
        properties.setProperty("enterPasswordTwice", "false");
        properties.setProperty("oldPasswordRequired", "false");
        target = new PasswordChangeState();
        initializeTarget();

        when(context.getInput("password")).thenReturn(Optional.of("new"));
        when(context.getUser()).thenReturn(Optional.of(user));
        when(user.getProperties(PasswordCredential.class)).thenReturn(List.of(passwordProperty));
    }

    private void initializeTarget() {
        final var startInitialization = StateInitialization.builder()
                .name("test")
                .properties(properties)
                .build();
        target.initialize(startInitialization);
    }

    @Test(expected = StateRenderableException.class)
    public void noNewPasswordGiven() {
        when(context.getInput("password")).thenReturn(Optional.empty());

        target.execute(context);
    }

    @Test(expected = StateFinalException.class)
    public void noUserGiven() {
        when(context.getUser()).thenReturn(Optional.empty());

        target.execute(context);
    }

    @Test
    public void noOldAndNotTwiceNeeded() {
        target.execute(context);

        verify(user, times(1)).getProperties(PasswordCredential.class);
        verify(passwordProperty, times(1)).update("new", null);
    }

    @Test
    public void createNew() {
        when(user.getProperties(PasswordCredential.class)).thenReturn(List.of());
        when(user.createProperty(PasswordCredential.class)).thenReturn(passwordProperty);

        target.execute(context);

        verify(user, times(1)).createProperty(PasswordCredential.class);
        verify(passwordProperty, times(1)).update("new", null);
        verify(passwordProperty, times(0)).check(anyString());
    }

    @Test(expected = StateRenderableException.class)
    public void noOldPasswordGivenButRequired() {
        properties.setProperty("oldPasswordRequired", "true");
        initializeTarget();
        when(context.getInput("oldPassword")).thenReturn(Optional.empty());

        target.execute(context);
    }


    @Test(expected = StateFinalException.class)
    public void oldPasswordButNoCredential() {
        properties.setProperty("oldPasswordRequired", "true");
        initializeTarget();
        when(user.getProperties(PasswordCredential.class)).thenReturn(List.of());
        when(context.getInput("oldPassword")).thenReturn(Optional.of("old"));

        target.execute(context);
    }

    @Test(expected = StateFinalException.class)
    public void oldPasswordNotValid() {
        properties.setProperty("oldPasswordRequired", "true");
        initializeTarget();
        when(context.getInput("oldPassword")).thenReturn(Optional.of("old"));
        when(passwordProperty.isValid()).thenReturn(false);

        target.execute(context);
    }

    @Test(expected = StateRenderableException.class)
    public void oldPasswordNotCorrect() {
        properties.setProperty("oldPasswordRequired", "true");
        initializeTarget();
        when(context.getInput("oldPassword")).thenReturn(Optional.of("old"));
        when(passwordProperty.check("old")).thenReturn(false);
        when(passwordProperty.isValid()).thenReturn(true);

        target.execute(context);
    }

    @Test
    public void oldPasswordSuccessful() {
        properties.setProperty("oldPasswordRequired", "true");
        initializeTarget();
        when(context.getInput("oldPassword")).thenReturn(Optional.of("old"));
        when(passwordProperty.check("old")).thenReturn(true);
        when(passwordProperty.isValid()).thenReturn(true);

        target.execute(context);
    }

    @Test(expected = StateRenderableException.class)
    public void twoNewPasswordsNeededButOnlyOneGiven() {
        properties.setProperty("enterPasswordTwice", "true");
        initializeTarget();
        when(context.getInput("password2")).thenReturn(Optional.empty());

        target.execute(context);
    }

    @Test(expected = StateRenderableException.class)
    public void twoNewPasswordsNeededButNotSame() {
        properties.setProperty("enterPasswordTwice", "true");
        initializeTarget();
        when(context.getInput("password2")).thenReturn(Optional.of("different"));

        target.execute(context);
    }

    @Test
    public void twoNewSuccessful() {
        properties.setProperty("enterPasswordTwice", "true");
        initializeTarget();
        when(context.getInput("password2")).thenReturn(Optional.of("new"));

        target.execute(context);
    }
}

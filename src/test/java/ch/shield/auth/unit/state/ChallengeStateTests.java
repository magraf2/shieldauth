package ch.shield.auth.unit.state;

import ch.shield.auth.core.repository.User;
import ch.shield.auth.core.state.*;
import ch.shield.auth.core.state.result.StateFinalException;
import ch.shield.auth.core.state.result.StateResult;
import ch.shield.auth.state.challenge.ChallengeCredential;
import ch.shield.auth.state.challenge.ChallengeState;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;
import java.util.Properties;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ChallengeStateTests {

    private ChallengeState target;

    @Mock
    private Context context;

    @Mock
    private User user;

    @Mock
    private ChallengeCredential challengeCredential;

    @Before
    public void before() {
        target = new ChallengeState();
        final var startInitialization = StateInitialization.builder()
                .name("test")
                .properties(new Properties())
                .build();
        target.initialize(startInitialization);

        when(context.getUser()).thenReturn(Optional.of(user));
        when(user.getProperties(ChallengeCredential.class)).thenReturn(List.of(challengeCredential));
        when(challengeCredential.isValid()).thenReturn(true);
        when(challengeCredential.check("123456", "789")).thenReturn(true);
        when(context.getInput("response")).thenReturn(Optional.of("789"));
        when(context.getAttribute("ch.shield.auth.state.challenge.ChallengeState.challenge", String.class)).thenReturn(Optional.of("123456"));
    }

    @Test(expected = StateRenderableException.class)
    public void challengeButNoResponse() {
        when(context.getInput("response")).thenReturn(Optional.empty());

        target.execute(context);
    }

    @Test(expected = StateFinalException.class)
    public void noUserPresent() {
        when(context.getUser()).thenReturn(Optional.empty());

        target.execute(context);
    }

    @Test(expected = StateFinalException.class)
    public void noCredentialFound() {
        when(user.getProperties(ChallengeCredential.class)).thenReturn(List.of());

        target.execute(context);
    }

    @Test(expected = StateFinalException.class)
    public void credentialNotValid() {
        when(challengeCredential.isValid()).thenReturn(false);

        target.execute(context);
    }

    @Test(expected = StateRenderableException.class)
    public void verifyResponseFailed() {
        when(challengeCredential.check("123456", "789")).thenReturn(false);

        target.execute(context);
    }

    @Test
    public void generateChallenge() {
        when(challengeCredential.generateChallenge()).thenReturn("123456");
        when(context.getAttribute("ch.shield.auth.state.challenge.ChallengeState.challenge", String.class)).thenReturn(Optional.empty());

        StateResult result = target.execute(context);

        verify(challengeCredential, times(1)).generateChallenge();
        Assert.assertEquals(Optional.empty(), result.getTransition());
        Assert.assertTrue(result.getRenderable().isPresent());
        Assert.assertEquals(200, result.getRenderable().get().getStatusCode());
        Assert.assertEquals(0, result.getRenderable().get().getHeaders().size());
        Assert.assertEquals(1, result.getRenderable().get().getAttributes().size());
        Assert.assertEquals("123456", result.getRenderable().get().getAttributes().get("challenge"));
        Assert.assertEquals("ChallengeState", result.getRenderable().get().getName());
        Assert.assertEquals(Optional.empty(), result.getUser());
        Assert.assertFalse(result.isFinished());
        Assert.assertEquals(1, result.getAttributes().size());
        Assert.assertEquals("123456", result.getAttributes().get("ch.shield.auth.state.challenge.ChallengeState.challenge"));
        Assert.assertEquals(0, result.getClaims().size());
    }

    @Test
    public void verifyResponse() {

        StateResult result = target.execute(context);

        verify(challengeCredential, times(1)).check("123456", "789");
        Assert.assertTrue(result.getTransition().isPresent());
        Assert.assertEquals("ok", result.getTransition().get());
        Assert.assertEquals(Optional.empty(), result.getRenderable());
        Assert.assertEquals(Optional.empty(), result.getUser());
        Assert.assertFalse(result.isFinished());
        Assert.assertEquals(0, result.getAttributes().size());
        Assert.assertEquals(0, result.getClaims().size());
    }

}


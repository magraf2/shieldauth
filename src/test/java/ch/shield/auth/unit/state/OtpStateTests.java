package ch.shield.auth.unit.state;

import ch.shield.auth.core.repository.User;
import ch.shield.auth.core.state.*;
import ch.shield.auth.core.state.result.StateFinalException;
import ch.shield.auth.core.state.result.StateResult;
import ch.shield.auth.state.otp.OtpCredential;
import ch.shield.auth.state.otp.OtpState;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;
import java.util.Properties;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class OtpStateTests {

    private OtpState target;

    @Mock
    private Context context;

    @Mock
    private User user;

    @Mock
    private OtpCredential otpProperty;

    @Before
    public void before() {
        target = new OtpState();
        final var startInitialization = StateInitialization.builder()
                .name("test")
                .properties(new Properties())
                .build();
        target.initialize(startInitialization);

        when(context.getInput("otp")).thenReturn(Optional.of("1234"));
        when(context.getUser()).thenReturn(Optional.of(user));
        when(user.getProperties(OtpCredential.class)).thenReturn(List.of(otpProperty));
        when(otpProperty.isValid()).thenReturn(true);
        when(otpProperty.check("1234")).thenReturn(true);
    }

    @Test(expected = StateRenderableException.class)
    public void noOtp() {
        when(context.getInput("otp")).thenReturn(Optional.empty());

        target.execute(context);
    }

    @Test(expected = StateFinalException.class)
    public void userNotKnown() {
        when(context.getUser()).thenReturn(Optional.empty());

        target.execute(context);
    }

    @Test(expected = StateFinalException.class)
    public void userNoOtp() {
        when(user.getProperties(OtpCredential.class)).thenReturn(List.of());

        target.execute(context);
    }

    @Test(expected = StateFinalException.class)
    public void otpNotValid() {
        when(otpProperty.isValid()).thenReturn(false);

        target.execute(context);
    }

    @Test(expected = StateRenderableException.class)
    public void wrongOtp() {
        when(otpProperty.check("1234")).thenReturn(false);

        target.execute(context);
    }

    @Test()
    public void execute() {

        StateResult stateResult = target.execute(context);

        verify(otpProperty, times(1)).check("1234");
        Assert.assertEquals(0, stateResult.getAttributes().size());
        Assert.assertFalse(stateResult.getRenderable().isPresent());
        Assert.assertTrue(stateResult.getTransition().isPresent());
        Assert.assertEquals("ok", stateResult.getTransition().get());
    }
}

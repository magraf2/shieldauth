package ch.shield.auth.unit.engine;

import ch.shield.auth.core.Renderable;
import ch.shield.auth.core.config.ConfigFile;
import ch.shield.auth.core.config.Flow;
import ch.shield.auth.core.engine.Engine;
import ch.shield.auth.core.engine.EngineRequest;
import ch.shield.auth.core.engine.SessionAttributes;
import ch.shield.auth.core.repository.Repository;
import ch.shield.auth.core.repository.User;
import ch.shield.auth.core.state.State;
import ch.shield.auth.core.state.result.StateResult;
import ch.shield.auth.engine.EngineImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AttributesTests {
    @Mock
    private ConfigFile configFile;

    @Mock
    private SessionAttributes sessionAttributes;

    @Mock
    private Flow flow;

    @Mock
    private EngineRequest engineRequest;

    @Mock
    private Repository repository;

    @Mock
    private State state;

    private Engine target;

    @Before
    public void before() {
        target = new EngineImpl(20, 300, configFile);
        final var sessionId = target.init(sessionAttributes);
        when(engineRequest.getSessionId()).thenReturn(sessionId);
        when(sessionAttributes.getFlow()).thenReturn(flow);
        when(configFile.getRepository(flow)).thenReturn(repository);
        when(configFile.getNextState(any(), any(), any())).thenReturn(state);
    }

    @Test
    public void setUser() {
        final var stateResult = mock(StateResult.class);
        final var user = mock(User.class);
        when(stateResult.getUser()).thenReturn(Optional.of(user));
        when(stateResult.getRenderable()).thenReturn(Optional.of(mock(Renderable.class)));
        when(state.execute(any())).thenReturn(stateResult);

        final var result = target.request(engineRequest);

        Assert.assertTrue(result.getUser().isPresent());
        Assert.assertEquals(user, result.getUser().get());
    }

    @Test
    public void noSetUser() {
        final var stateResult = mock(StateResult.class);
        when(stateResult.getUser()).thenReturn(Optional.empty());
        when(stateResult.getRenderable()).thenReturn(Optional.of(mock(Renderable.class)));
        when(state.execute(any())).thenReturn(stateResult);

        final var result = target.request(engineRequest);

        Assert.assertFalse(result.getUser().isPresent());
    }

    @Test
    public void setAttribute() {
        final var stateResult = mock(StateResult.class);
        when(stateResult.getAttributes()).thenReturn(Map.of("test", "dummy"));
        when(stateResult.getRenderable()).thenReturn(Optional.of(mock(Renderable.class)));
        when(state.execute(any())).thenReturn(stateResult);

        final var result = target.request(engineRequest);

        Assert.assertEquals(1, result.getAttributes().size());
        Assert.assertEquals("dummy", result.getAttributes().get("test"));
    }

    @Test
    public void noSetAttribute() {
        final var stateResult = mock(StateResult.class);
        when(stateResult.getAttributes()).thenReturn(Map.of());
        when(stateResult.getRenderable()).thenReturn(Optional.of(mock(Renderable.class)));
        when(state.execute(any())).thenReturn(stateResult);

        final var result = target.request(engineRequest);

        Assert.assertEquals(0, result.getAttributes().size());
    }

    @Test
    public void noOverwriteAttribute() {
        final var stateResult = mock(StateResult.class);
        when(stateResult.getAttributes()).thenReturn(Map.of("test", "dummy"));
        when(stateResult.getRenderable()).thenReturn(Optional.of(mock(Renderable.class)));
        final var stateResult2 = mock(StateResult.class);
        when(stateResult2.getAttributes()).thenReturn(Map.of("test2", "dummyXX"));
        when(stateResult2.getRenderable()).thenReturn(Optional.of(mock(Renderable.class)));
        when(state.execute(any())).thenReturn(stateResult).thenReturn(stateResult2);

        target.request(engineRequest);
        final var result = target.request(engineRequest);

        Assert.assertEquals(2, result.getAttributes().size());
        Assert.assertEquals("dummy", result.getAttributes().get("test"));
        Assert.assertEquals("dummyXX", result.getAttributes().get("test2"));
    }


    @Test
    public void doOverwriteAttribute() {
        final var stateResult = mock(StateResult.class);
        when(stateResult.getAttributes()).thenReturn(Map.of("test", "dummy"));
        when(stateResult.getRenderable()).thenReturn(Optional.of(mock(Renderable.class)));
        final var stateResult2 = mock(StateResult.class);
        when(stateResult2.getAttributes()).thenReturn(Map.of("test", "dummyXX"));
        when(stateResult2.getRenderable()).thenReturn(Optional.of(mock(Renderable.class)));
        when(state.execute(any())).thenReturn(stateResult).thenReturn(stateResult2);

        target.request(engineRequest);
        final var result = target.request(engineRequest);

        Assert.assertEquals(1, result.getAttributes().size());
        Assert.assertEquals("dummyXX", result.getAttributes().get("test"));
    }

    @Test
    public void doDeleteAttribute() {
        final var stateResult = mock(StateResult.class);
        when(stateResult.getAttributes()).thenReturn(Map.of("test", "dummy"));
        when(stateResult.getRenderable()).thenReturn(Optional.of(mock(Renderable.class)));
        final var stateResult2 = mock(StateResult.class);
        final var retDeleteAttribute = new HashMap<String, Object>();
        retDeleteAttribute.put("test", null);
        when(stateResult2.getAttributes()).thenReturn(retDeleteAttribute);
        when(stateResult2.getRenderable()).thenReturn(Optional.of(mock(Renderable.class)));
        when(state.execute(any())).thenReturn(stateResult).thenReturn(stateResult2);

        target.request(engineRequest);
        final var result = target.request(engineRequest);

        Assert.assertEquals(0, result.getAttributes().size());
        Assert.assertNull(result.getAttributes().get("test"));
    }
}

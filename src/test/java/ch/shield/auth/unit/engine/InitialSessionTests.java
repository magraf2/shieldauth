package ch.shield.auth.unit.engine;

import ch.shield.auth.core.config.ConfigFile;
import ch.shield.auth.core.engine.Engine;
import ch.shield.auth.core.engine.SessionAttributes;
import ch.shield.auth.engine.EngineImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class InitialSessionTests {
    @Mock
    private ConfigFile configFile;

    @Mock
    private SessionAttributes sessionAttributes;

    private Engine target;

    @Before
    public void before() {
        target = new EngineImpl(20, 300, configFile);
    }

    @Test
    public void init() {
        final var sessionId = target.init(sessionAttributes);
        Assert.assertNotNull(sessionId);
    }

    @Test
    public void check() {
        final var sessionId = target.init(sessionAttributes);
        final var result = target.check(sessionId);

        Assert.assertNotNull(result);
        Assert.assertFalse(result.isFinished());
        Assert.assertNotNull(result.getAttributes());
        Assert.assertEquals(0, result.getAttributes().size());
        Assert.assertFalse(result.getUser().isPresent());
        Assert.assertFalse(result.getRenderable().isPresent());
    }

    @Test
    public void getAttributes() {
        final var sessionId = target.init(sessionAttributes);
        final var result = target.getAttributes(sessionId, SessionAttributes.class);

        Assert.assertEquals(sessionAttributes, result);
    }
}

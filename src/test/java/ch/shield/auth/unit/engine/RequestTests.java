package ch.shield.auth.unit.engine;

import ch.shield.auth.core.Renderable;
import ch.shield.auth.core.config.ConfigFile;
import ch.shield.auth.core.config.Flow;
import ch.shield.auth.core.engine.Engine;
import ch.shield.auth.core.engine.EngineException;
import ch.shield.auth.core.engine.EngineRequest;
import ch.shield.auth.core.engine.SessionAttributes;
import ch.shield.auth.core.repository.Repository;
import ch.shield.auth.core.state.State;
import ch.shield.auth.core.state.result.StateFinalException;
import ch.shield.auth.core.state.result.StateResult;
import ch.shield.auth.engine.EngineImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RequestTests {
    @Mock
    private ConfigFile configFile;

    @Mock
    private SessionAttributes sessionAttributes;

    @Mock
    private Flow flow;

    @Mock
    private EngineRequest engineRequest;

    @Mock
    private Repository repository;

    @Mock
    private State state;

    private Engine target;

    @Before
    public void before() {
        target = new EngineImpl(20, 300, configFile);
        final var sessionId = target.init(sessionAttributes);
        when(engineRequest.getSessionId()).thenReturn(sessionId);
        when(sessionAttributes.getFlow()).thenReturn(flow);
        when(configFile.getRepository(flow)).thenReturn(repository);
        when(configFile.getNextState(flow, null, null)).thenReturn(state);
    }

    @Test
    public void renderableResponse() {
        final var renderable = mock(Renderable.class);
        final var stateResult = mock(StateResult.class);
        when(stateResult.getRenderable()).thenReturn(Optional.of(renderable));
        when(state.execute(any())).thenReturn(stateResult);

        final var result = target.request(engineRequest);

        Assert.assertTrue(result.getRenderable().isPresent());
        Assert.assertEquals(renderable, result.getRenderable().get());
        verify(state, times(1)).execute(any());
    }

    @Test
    public void finishResponse() {
        final var stateResult = mock(StateResult.class);
        when(stateResult.isFinished()).thenReturn(true);
        when(state.execute(any())).thenReturn(stateResult);

        final var result = target.request(engineRequest);

        Assert.assertTrue(result.isFinished());
        verify(state, times(1)).execute(any());
    }

    @Test
    public void reRunFinishResponse() {
        final var stateResult = mock(StateResult.class);
        when(stateResult.isFinished()).thenReturn(true);
        when(state.execute(any())).thenReturn(stateResult);

        target.request(engineRequest);
        final var result = target.request(engineRequest);

        Assert.assertTrue(result.isFinished());

        //Still only called once since finished is remembered
        verify(state, times(1)).execute(any());
    }

    @Test
    public void stateException() {
        final var renderable = mock(Renderable.class);
        final var stateException = mock(StateRenderableException.class);
        when(stateException.getRenderable()).thenReturn(renderable);
        when(state.execute(any())).thenThrow(stateException);

        final var result = target.request(engineRequest);

        Assert.assertTrue(result.getRenderable().isPresent());
        Assert.assertEquals(renderable, result.getRenderable().get());
        verify(state, times(1)).execute(any());
    }


    @Test
    public void stateFinalException() {
        final var stateFinalException = mock(StateFinalException.class);
        when(stateFinalException.getOutputMessage()).thenReturn("test");
        when(stateFinalException.getStatusCode()).thenReturn(404);
        when(state.execute(any())).thenThrow(stateFinalException);

        try {
            target.request(engineRequest);
        } catch (EngineException e) {
            Assert.assertEquals(404, e.getStatusCode());
            Assert.assertEquals("test", e.getOutputMessage());
            return;
        }
        Assert.fail();
    }

    @Test
    public void reRunStateFinalException() {
        final var stateFinalException = mock(StateFinalException.class);
        when(stateFinalException.getOutputMessage()).thenReturn("test");
        when(stateFinalException.getStatusCode()).thenReturn(404);
        when(state.execute(any())).thenThrow(stateFinalException);

        try {
            target.request(engineRequest);
        } catch (EngineException ignored) {
        }

        verify(state, times(1)).execute(any());


        try {
            target.request(engineRequest);
        } catch (EngineException e) {
            Assert.assertEquals(404, e.getStatusCode());
            Assert.assertEquals("test", e.getOutputMessage());

            //Still only called once since finished is remembered
            verify(state, times(1)).execute(any());
            return;
        }
        Assert.fail();
    }

    @Test
    public void transition() {
        final var renderable = mock(Renderable.class);
        final var stateResult = mock(StateResult.class);
        when(stateResult.getTransition()).thenReturn(Optional.of("trans"));
        final var stateResult2 = mock(StateResult.class);
        when(stateResult2.getRenderable()).thenReturn(Optional.of(renderable));
        when(state.execute(any())).thenReturn(stateResult).thenReturn(stateResult2);
        when(configFile.getNextState(flow, state, "trans")).thenReturn(state);


        final var result = target.request(engineRequest);

        Assert.assertTrue(result.getRenderable().isPresent());
        Assert.assertEquals(renderable, result.getRenderable().get());
        verify(state, times(2)).execute(any());
    }
}

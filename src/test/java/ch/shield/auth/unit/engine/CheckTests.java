package ch.shield.auth.unit.engine;

import ch.shield.auth.core.Renderable;
import ch.shield.auth.core.config.ConfigFile;
import ch.shield.auth.core.config.Flow;
import ch.shield.auth.core.engine.Engine;
import ch.shield.auth.core.engine.EngineException;
import ch.shield.auth.core.engine.EngineRequest;
import ch.shield.auth.core.engine.SessionAttributes;
import ch.shield.auth.core.repository.Repository;
import ch.shield.auth.core.state.State;
import ch.shield.auth.core.state.result.StateFinalException;
import ch.shield.auth.core.state.result.StateResult;
import ch.shield.auth.engine.EngineImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CheckTests {
    @Mock
    private ConfigFile configFile;

    @Mock
    private SessionAttributes sessionAttributes;

    @Mock
    private Flow flow;

    @Mock
    private EngineRequest engineRequest;

    @Mock
    private Repository repository;

    @Mock
    private State state;

    private Engine target;

    @Before
    public void before() {
        target = new EngineImpl(20, 300, configFile);
        final var sessionId = target.init(sessionAttributes);
        when(engineRequest.getSessionId()).thenReturn(sessionId);
        when(sessionAttributes.getFlow()).thenReturn(flow);
        when(configFile.getRepository(flow)).thenReturn(repository);
        when(configFile.getNextState(flow, null, null)).thenReturn(state);
    }

    @Test
    public void renderableCheck() {
        final var renderable = mock(Renderable.class);
        final var stateResult = mock(StateResult.class);
        when(stateResult.getRenderable()).thenReturn(Optional.of(renderable));
        when(state.execute(any())).thenReturn(stateResult);
        target.request(engineRequest);

        final var result = target.check(engineRequest.getSessionId());

        Assert.assertFalse(result.getRenderable().isPresent());
        Assert.assertFalse(result.isFinished());
    }

    @Test
    public void finishCheck() {
        final var stateResult = mock(StateResult.class);
        when(stateResult.isFinished()).thenReturn(true);
        when(state.execute(any())).thenReturn(stateResult);
        target.request(engineRequest);

        final var result = target.check(engineRequest.getSessionId());

        Assert.assertFalse(result.getRenderable().isPresent());
        Assert.assertTrue(result.isFinished());
    }


    @Test
    public void exceptionCheck() {
        final var stateFinalException = mock(StateFinalException.class);
        when(stateFinalException.getOutputMessage()).thenReturn("test");
        when(stateFinalException.getStatusCode()).thenReturn(404);
        when(state.execute(any())).thenThrow(stateFinalException);

        try {
            target.request(engineRequest);
        } catch (EngineException ignore) {
        }

        try {
            target.check(engineRequest.getSessionId());
        } catch (EngineException e) {
            Assert.assertEquals(404, e.getStatusCode());
            Assert.assertEquals("test", e.getOutputMessage());
            return;
        }

        Assert.fail();
    }
}

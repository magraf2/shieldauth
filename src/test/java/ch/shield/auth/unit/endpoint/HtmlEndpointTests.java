package ch.shield.auth.unit.endpoint;

import ch.shield.auth.core.Renderable;
import ch.shield.auth.core.engine.Engine;
import ch.shield.auth.core.engine.EngineRequest;
import ch.shield.auth.core.engine.EngineResult;
import ch.shield.auth.core.engine.SessionAttributes;
import ch.shield.auth.endpoint.html.HtmlEndpoint;
import ch.shield.auth.engine.EngineExceptionImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class HtmlEndpointTests {

    @Mock
    private Engine engine;

    @Mock
    private SessionAttributes sessionAttributes;

    private HtmlEndpoint target;
    private UUID sessionId;

    @Before
    public void before() throws URISyntaxException {
        target = new HtmlEndpoint(engine, "/var/opt/shieldAuth/template/");
        sessionId = UUID.randomUUID();
        when(sessionAttributes.getFinalEndpoint()).thenReturn(new URI("http://www.ecxample.com/auth"));
        when(engine.getAttributes(sessionId, SessionAttributes.class)).thenReturn(sessionAttributes);
    }

    @Test
    public void testNoSession() {
        final var result = target.get(new LinkedMultiValueMap<>(), null, Locale.ENGLISH, UriComponentsBuilder.fromHttpUrl("http://www.example.com/"));

        Assert.assertEquals(400, result.getStatusCodeValue());
        Assert.assertEquals("text/html", result.getHeaders().getFirst("Content-Type"));
        Assert.assertEquals("<!--suppress ALL -->\r\n" +
                "<html>\r\n" +
                "<head><title>test</title></head>\r\n" +
                "<body><p>Invalid Session Cookie</p></body>\r\n" +
                "</html>", result.getBody());
    }

    @Test
    public void testInvalidSession() {
        final var result = target.get(new LinkedMultiValueMap<>(), "invalid", Locale.ENGLISH, UriComponentsBuilder.fromHttpUrl("http://www.example.com/"));

        Assert.assertEquals(400, result.getStatusCodeValue());
        Assert.assertEquals("text/html", result.getHeaders().getFirst("Content-Type"));
        Assert.assertEquals("<!--suppress ALL -->\r\n" +
                "<html>\r\n" +
                "<head><title>test</title></head>\r\n" +
                "<body><p>Invalid Session Cookie</p></body>\r\n" +
                "</html>", result.getBody());
    }

    @Test
    public void testOutdatedSession() {
        when(engine.getAttributes(sessionId, SessionAttributes.class)).thenThrow(EngineExceptionImpl.invalidSession(sessionId));

        final var result = target.get(new LinkedMultiValueMap<>(), sessionId.toString(), Locale.ENGLISH, UriComponentsBuilder.fromHttpUrl("http://www.example.com/"));

        Assert.assertEquals(400, result.getStatusCodeValue());
        Assert.assertEquals("text/html", result.getHeaders().getFirst("Content-Type"));
        Assert.assertEquals("<!--suppress ALL -->\r\n" +
                "<html>\r\n" +
                "<head><title>test</title></head>\r\n" +
                "<body><p>Invalid session or timeout</p></body>\r\n" +
                "</html>", result.getBody());
    }

    @Test
    public void testFinished() {
        final var engineResult = mock(EngineResult.class);
        when(engineResult.isFinished()).thenReturn(true);
        when(engine.request(argThat(a -> a.getSessionId().equals(sessionId)))).thenReturn(engineResult);

        final var result = target.get(new LinkedMultiValueMap<>(), sessionId.toString(), Locale.ENGLISH, UriComponentsBuilder.fromHttpUrl("http://www.example.com/"));

        Assert.assertEquals(302, result.getStatusCodeValue());
        Assert.assertEquals("http://www.ecxample.com/auth", result.getHeaders().getFirst("Location"));
    }

    @Test
    public void testException() {
        when(engine.request(argThat(a -> a.getSessionId().equals(sessionId)))).thenThrow(EngineExceptionImpl.wrap(new RuntimeException("test"), 404, "TEST"));

        final var result = target.get(new LinkedMultiValueMap<>(), sessionId.toString(), Locale.ENGLISH, UriComponentsBuilder.fromHttpUrl("http://www.example.com/"));

        Assert.assertEquals(302, result.getStatusCodeValue());
        Assert.assertEquals("http://www.ecxample.com/auth", result.getHeaders().getFirst("Location"));
    }

    @Test
    public void testGui() {
        final var renderable = mock(Renderable.class);
        when(renderable.getAttributes()).thenReturn(Map.of("out", "text"));
        when(renderable.getStatusCode()).thenReturn(200);
        when(renderable.getName()).thenReturn("test_gui");
        final var engineResult = mock(EngineResult.class);
        when(engineResult.getRenderable()).thenReturn(Optional.of(renderable));
        when(engine.request(argThat(a -> a.getSessionId().equals(sessionId)))).thenReturn(engineResult);

        final var result = target.get(new LinkedMultiValueMap<>(), sessionId.toString(), Locale.ENGLISH, UriComponentsBuilder.fromHttpUrl("http://www.example.com/"));

        Assert.assertEquals(200, result.getStatusCodeValue());
        Assert.assertEquals("text/html", result.getHeaders().getFirst("Content-Type"));
        Assert.assertEquals("<html>\r\n<head><title>test</title></head>\r\n<body></body>\r\n</html>", result.getBody());
    }

    @Test
    public void inputTests() {
        final var engineResult = mock(EngineResult.class);
        when(engineResult.isFinished()).thenReturn(true);
        when(engine.request(argThat(a -> a.getSessionId().equals(sessionId)))).thenReturn(engineResult);

        target.post(new LinkedMultiValueMap<>(Map.of("t1", List.of("v1"), "t2", List.of("v21", "v22"))), new LinkedMultiValueMap<>(Map.of("h1", List.of("v1"), "h2", List.of("v21", "v22"))), sessionId.toString(), Locale.ENGLISH, UriComponentsBuilder.fromHttpUrl("http://www.example.com/"));

        ArgumentCaptor<EngineRequest> requestArgumentCaptor = ArgumentCaptor.forClass(EngineRequest.class);
        verify(engine).request(requestArgumentCaptor.capture());

        var inputs = requestArgumentCaptor.getValue().getInputs();
        Assert.assertEquals(2, inputs.size());
        Assert.assertEquals(1, inputs.get("t1").length);
        Assert.assertEquals("v1", inputs.get("t1")[0]);
        Assert.assertEquals(2, inputs.get("t2").length);
        Assert.assertEquals("v21", inputs.get("t2")[0]);
        Assert.assertEquals("v22", inputs.get("t2")[1]);
        var headers = requestArgumentCaptor.getValue().getHeaders();
        Assert.assertEquals(2, headers.size());
        Assert.assertEquals(1, headers.get("h1").length);
        Assert.assertEquals("v1", headers.get("h1")[0]);
        Assert.assertEquals(2, headers.get("h2").length);
        Assert.assertEquals("v21", headers.get("h2")[0]);
        Assert.assertEquals("v22", headers.get("h2")[1]);
    }
}
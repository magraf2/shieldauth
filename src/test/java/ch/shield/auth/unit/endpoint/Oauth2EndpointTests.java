package ch.shield.auth.unit.endpoint;

import ch.shield.auth.core.config.ConfigFile;
import ch.shield.auth.core.engine.Engine;
import ch.shield.auth.core.engine.SessionAttributes;
import ch.shield.auth.endpoint.oauth2.Oauth2Attributes;
import ch.shield.auth.endpoint.oauth2.Oauth2Endpoint;
import ch.shield.auth.endpoint.oauth2.Oauth2Flow;
import ch.shield.auth.endpoint.oauth2.dto.Error;
import ch.shield.auth.endpoint.oauth2.dto.ResponseType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class Oauth2EndpointTests {
    @Mock
    private Engine engine;

    @Mock
    private ConfigFile configFile;

    @Mock
    private Oauth2Flow flow;

    private Oauth2Endpoint target;
    private UUID sessionId;

    @Before
    public void before() {
        target = new Oauth2Endpoint(engine, configFile, "/auth/");
        when(flow.isValid("client123", ResponseType.CODE)).thenReturn(true);
        when(configFile.getFlows(Oauth2Flow.class)).thenReturn(List.of(flow));
        sessionId = UUID.randomUUID();
        when(engine.init(any())).thenReturn(sessionId);
    }

    @Test
    public void authenticateWithSession() throws URISyntaxException {
        when(engine.getAttributes(eq(sessionId), any())).thenReturn(mock(Oauth2Attributes.class));
        final var params = Map.of("response_type", "code","client_id", "client123","state", "xyz","redirect_uri", "http://client.example.com/test");

        final var response = target.authenticate(params, sessionId, UriComponentsBuilder.fromHttpUrl("http://login.example.com/auth/oauth2/authenticate"));

        Assert.assertEquals(302, response.getStatusCodeValue());
        Assert.assertEquals(new URI("http://client.example.com/test?error=server_error&error_description=Step+up+is+not+supported&state=xyz"), response.getHeaders().getLocation());
    }

    @Test
    public void authenticateOutdatedSession() throws URISyntaxException {
        final var params = Map.of("response_type", "code","client_id", "client123","state", "xyz","redirect_uri", "http://client.example.com/test");

        final var response = target.authenticate(params, UUID.randomUUID(), UriComponentsBuilder.fromHttpUrl("http://login.example.com/auth/oauth2/authenticate"));

        Assert.assertEquals(302, response.getStatusCodeValue());
        Assert.assertEquals(new URI("http://login.example.com/auth/authenticate"), response.getHeaders().getLocation());
        Assert.assertEquals("login=" + sessionId.toString()+"; Path=/auth/; HttpOnly", response.getHeaders().getFirst("Set-Cookie"));
    }

    @Test
    public void authenticateInvalidRedirectUri() {
        final var params = Map.of("response_type", "code","client_id", "client123","state", "xyz","redirect_uri", "\\][invalid");

        final var response = target.authenticate(params, UUID.randomUUID(), UriComponentsBuilder.fromHttpUrl("http://login.example.com/auth/oauth2/authenticate"));

        Assert.assertEquals(400, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(Error.INVALID_REQUEST, response.getBody().getError());
        Assert.assertEquals("Could not parse request URI", response.getBody().getErrorDescription());
    }

    @Test
    public void authenticateNoRedirectUri() {
        final var params = Map.of("response_type", "code","client_id", "client123","state", "xyz");

        final var response = target.authenticate(params, UUID.randomUUID(), UriComponentsBuilder.fromHttpUrl("http://login.example.com/auth/oauth2/authenticate"));

        Assert.assertEquals(400, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(Error.INVALID_REQUEST, response.getBody().getError());
        Assert.assertEquals("Could not parse request URI", response.getBody().getErrorDescription());
    }

    @Test
    public void authenticateNoClientId() throws URISyntaxException {
        final var params = Map.of("response_type", "code","state", "xyz","redirect_uri", "http://client.example.com/test");

        final var response = target.authenticate(params, UUID.randomUUID(), UriComponentsBuilder.fromHttpUrl("http://login.example.com/auth/oauth2/authenticate"));

        Assert.assertEquals(302, response.getStatusCodeValue());
        Assert.assertEquals(new URI("http://client.example.com/test?error=invalid_client&error_description=Client+ID+not+allowed+for+this+request+type&state=xyz"), response.getHeaders().getLocation());
    }

    @Test
    public void authenticateInvalidClientId() throws URISyntaxException {
        final var params = Map.of("response_type", "code","client_id", "wrong","state", "xyz","redirect_uri", "http://client.example.com/test");

        final var response = target.authenticate(params, UUID.randomUUID(), UriComponentsBuilder.fromHttpUrl("http://login.example.com/auth/oauth2/authenticate"));

        Assert.assertEquals(302, response.getStatusCodeValue());
        Assert.assertEquals(new URI("http://client.example.com/test?error=invalid_client&error_description=Client+ID+not+allowed+for+this+request+type&state=xyz"), response.getHeaders().getLocation());
    }

    @Test
    public void authenticate() throws URISyntaxException {
        final var params = Map.of("response_type", "code","client_id", "client123","state", "xyz","redirect_uri", "http://client.example.com/test","scope", "role1 role2");

        final var response = target.authenticate(params, UUID.randomUUID(), UriComponentsBuilder.fromHttpUrl("http://login.example.com/auth/oauth2/authenticate"));

        Assert.assertEquals(302, response.getStatusCodeValue());
        Assert.assertEquals(new URI("http://login.example.com/auth/authenticate"), response.getHeaders().getLocation());
    }


    @Test
    public void cookie() {
        final var params = Map.of("response_type", "code","client_id", "client123","state", "xyz","redirect_uri", "http://client.example.com/test","scope", "role1 role2");

        final var response = target.authenticate(params, UUID.randomUUID(), UriComponentsBuilder.fromHttpUrl("http://login.example.com/auth/oauth2/authenticate"));

        Assert.assertEquals("login=" + sessionId.toString()+"; Path=/auth/; HttpOnly", response.getHeaders().getFirst("Set-Cookie"));
    }

    @Test
    public void createdSession() throws URISyntaxException {
        final var params = Map.of("response_type", "code","client_id", "client123","state", "xyz","redirect_uri", "http://client.example.com/test","scope", "role1 role2");

        target.authenticate(params, UUID.randomUUID(), UriComponentsBuilder.fromHttpUrl("http://login.example.com/auth/oauth2/authenticate"));

        ArgumentCaptor<SessionAttributes> sessionAttributesArgumentCaptor = ArgumentCaptor.forClass(SessionAttributes.class);
        verify(engine).init(sessionAttributesArgumentCaptor.capture());
        final var sessionAttributes = sessionAttributesArgumentCaptor.getValue();
        Assert.assertTrue(sessionAttributes instanceof Oauth2Attributes);
        final var sessionAttributesOauth2 = (Oauth2Attributes) sessionAttributesArgumentCaptor.getValue();

        Assert.assertEquals("xyz", sessionAttributesOauth2.getState());
        Assert.assertEquals("client123", sessionAttributesOauth2.getClientId());
        Assert.assertEquals(new URI("http://client.example.com/test"), sessionAttributesOauth2.getRedirectUri());
        Assert.assertEquals(List.of("role1", "role2"), sessionAttributesOauth2.getScopes());
        Assert.assertEquals(new URI("http://login.example.com/auth/oauth2/final"), sessionAttributesOauth2.getFinalEndpoint());
    }
}

package ch.shield.auth.unit.config;

import ch.shield.auth.core.state.Context;
import ch.shield.auth.core.state.State;
import ch.shield.auth.core.state.StateInitialization;
import ch.shield.auth.core.state.result.StateResult;
import lombok.Getter;

public class DummyState implements State {
    @Getter
    private String name;

    @Override
    public void initialize(StateInitialization stateInitialization) {
        name = stateInitialization.getName();
    }

    @Override
    public StateResult execute(Context context) {
        return null;
    }
}

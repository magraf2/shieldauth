package ch.shield.auth.unit.config;

import ch.shield.auth.config.ConfigFileImpl;
import ch.shield.auth.config.xml.XmlFileReadException;
import ch.shield.auth.core.config.Flow;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConfigFileImplTests {
    @Mock
    AutowireCapableBeanFactory autowireCapableBeanFactory;

    @Test
    public void empty() {

        final var target = new ConfigFileImpl("test_empty.xml", autowireCapableBeanFactory);

        Assert.assertEquals(0, target.getFlows(Flow.class).size());
        Assert.assertFalse("test", target.getFlow("test", Flow.class).isPresent());
    }

    @Test
    public void normal() {
        when(autowireCapableBeanFactory.createBean(DummyFlow.class)).thenAnswer(c -> new DummyFlow());
        when(autowireCapableBeanFactory.createBean(DummyRepo.class)).thenAnswer(c -> new DummyRepo());
        when(autowireCapableBeanFactory.createBean(DummyState.class)).thenAnswer(c -> new DummyState());

        final var target = new ConfigFileImpl("test_flow.xml", autowireCapableBeanFactory);

        Assert.assertEquals(1, target.getFlows(Flow.class).size());
        Assert.assertEquals("test", target.getFlows(Flow.class).get(0).getName());
        Assert.assertTrue("test", target.getFlow("test", Flow.class).isPresent());
        Assert.assertNotNull(target.getRepository(target.getFlow("test", Flow.class).get()));
    }

    @Test(expected = XmlFileReadException.class)
    public void fileNotFound() {
        new ConfigFileImpl("test_not_existing.xml", autowireCapableBeanFactory);
    }

    @Test(expected = XmlFileReadException.class)
    public void invalid() {
        new ConfigFileImpl("test_invalid.xml", autowireCapableBeanFactory);
    }
}

package ch.shield.auth.unit.config;

import ch.shield.auth.core.repository.Property;
import ch.shield.auth.core.repository.Repository;
import ch.shield.auth.core.repository.RepositoryInitialization;
import ch.shield.auth.core.repository.User;

import java.util.List;
import java.util.Optional;

public class DummyRepo implements Repository {
    @Override
    public Optional<User> getUserByUserId(String userId) {
        return Optional.empty();
    }

    @Override
    public <T extends Property> List<T> getPropertyByValue(Class<T> type, String value) {
        return null;
    }

    @Override
    public User createUser(String userId) {
        return null;
    }

    @Override
    public void initialize(RepositoryInitialization repositoryInitialization) {

    }
}

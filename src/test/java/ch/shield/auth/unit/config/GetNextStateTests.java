package ch.shield.auth.unit.config;

import ch.shield.auth.config.ConfigFileImpl;
import ch.shield.auth.config.FlowInitializeException;
import ch.shield.auth.core.config.ConfigFile;
import ch.shield.auth.core.config.Flow;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GetNextStateTests {
    @Mock
    AutowireCapableBeanFactory autowireCapableBeanFactory;

    private ConfigFile target;

    @Before
    public void before() {
        when(autowireCapableBeanFactory.createBean(DummyFlow.class)).thenAnswer(c -> new DummyFlow());
        when(autowireCapableBeanFactory.createBean(DummyRepo.class)).thenAnswer(c -> new DummyRepo());
        when(autowireCapableBeanFactory.createBean(DummyState.class)).thenAnswer(c -> new DummyState());

        target = new ConfigFileImpl("test_flow.xml", autowireCapableBeanFactory);
    }

    @Test
    public void getStartState() {
        final var flow = target.getFlows(Flow.class).get(0);

        final var result = target.getNextState(flow, null, null);

        Assert.assertEquals("start", result.getName());
    }

    @Test
    public void continueStartState() {
        final var flow = target.getFlows(Flow.class).get(0);

        final var state1 = target.getNextState(flow, null, null);
        final var result = target.getNextState(flow, state1, null);

        Assert.assertEquals(state1, result);
    }

    @Test(expected = FlowInitializeException.class)
    public void unknownTransition() {
        final var flow = target.getFlows(Flow.class).get(0);

        final var state1 = target.getNextState(flow, null, null);
        target.getNextState(flow, state1, "wrong");
    }

    @Test
    public void transition() {
        final var flow = target.getFlows(Flow.class).get(0);

        final var state1 = target.getNextState(flow, null, null);
        final var result = target.getNextState(flow, state1, "next");

        Assert.assertEquals("next", result.getName());
    }

}

package ch.shield.auth.unit.config;

import ch.shield.auth.core.config.Flow;
import ch.shield.auth.core.config.FlowInitialization;
import lombok.Getter;

public class DummyFlow implements Flow {
    @Getter
    private String name;

    @Override
    public void initialize(FlowInitialization flowInitialization) {
        name = flowInitialization.getName();
    }
}

package ch.shield.auth.unit.repository;

import ch.shield.auth.repository.base.TotpValidator;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDateTime;

public class TotpValidatorTests {

    @Test
    public void sha1time59() {
        TotpValidator target = new TotpValidator();
        target.setAlgorithm(TotpValidator.SHA1);
        target.setDigits(8);
        target.setSharedSecretKey("12345678901234567890");
        target.setTime(LocalDateTime.of(1970, 1, 1, 0, 0, 59));
        target.setTimeStep(30);
        target.setTolerance(0);
        Assert.assertTrue(target.validate("94287082"));
    }

    @Test
    public void sha256time59() {
        TotpValidator target = new TotpValidator();
        target.setAlgorithm(TotpValidator.SHA256);
        target.setDigits(8);
        target.setSharedSecretKey("12345678901234567890");
        target.setTime(LocalDateTime.of(1970, 1, 1, 0, 0, 59));
        target.setTimeStep(30);
        target.setTolerance(0);
        Assert.assertTrue(target.validate("32247374"));
    }

    @Test
    public void sha512time59() {
        TotpValidator target = new TotpValidator();
        target.setAlgorithm(TotpValidator.SHA512);
        target.setDigits(8);
        target.setSharedSecretKey("12345678901234567890");
        target.setTime(LocalDateTime.of(1970, 1, 1, 0, 0, 59));
        target.setTimeStep(30);
        target.setTolerance(0);
        Assert.assertTrue(target.validate("69342147"));
    }

    @Test
    public void sha1time1111111109() {
        TotpValidator target = new TotpValidator();
        target.setAlgorithm(TotpValidator.SHA1);
        target.setDigits(8);
        target.setSharedSecretKey("12345678901234567890");
        target.setTime(LocalDateTime.of(2005, 3, 18, 1, 58, 29));
        target.setTimeStep(30);
        target.setTolerance(0);
        Assert.assertTrue(target.validate("07081804"));
    }

    @Test
    public void sha256time1111111109() {
        TotpValidator target = new TotpValidator();
        target.setAlgorithm(TotpValidator.SHA256);
        target.setDigits(8);
        target.setSharedSecretKey("12345678901234567890");
        target.setTime(LocalDateTime.of(2005, 3, 18, 1, 58, 29));
        target.setTimeStep(30);
        target.setTolerance(0);
        Assert.assertTrue(target.validate("34756375"));
    }

    @Test
    public void sha512time1111111109() {
        TotpValidator target = new TotpValidator();
        target.setAlgorithm(TotpValidator.SHA512);
        target.setDigits(8);
        target.setSharedSecretKey("12345678901234567890");
        target.setTime(LocalDateTime.of(2005, 3, 18, 1, 58, 29));
        target.setTimeStep(30);
        target.setTolerance(0);
        Assert.assertTrue(target.validate("63049338"));
    }

    @Test
    public void testTolerance() {
        TotpValidator target = new TotpValidator();
        target.setAlgorithm(TotpValidator.SHA1);
        target.setDigits(8);
        target.setSharedSecretKey("12345678901234567890");
        target.setTime(LocalDateTime.of(2005, 3, 18, 1, 58, 30));
        target.setTimeStep(30);
        target.setTolerance(0);
        Assert.assertFalse(target.validate("07081804"));

        target.setTolerance(1);
        Assert.assertTrue(target.validate("07081804"));

        target.setTime(LocalDateTime.of(2005, 3, 18, 1, 59, 0));
        Assert.assertFalse(target.validate("07081804"));
        target.setTolerance(2);
        Assert.assertTrue(target.validate("07081804"));
    }


    @Test
    public void wrong() {
        TotpValidator target = new TotpValidator();
        target.setAlgorithm(TotpValidator.SHA1);
        target.setDigits(8);
        target.setSharedSecretKey("12345678901234567890");
        target.setTime(LocalDateTime.of(1970, 1, 1, 0, 0, 59));
        target.setTimeStep(30);
        target.setTolerance(0);
        Assert.assertFalse(target.validate("90693936"));
    }
}


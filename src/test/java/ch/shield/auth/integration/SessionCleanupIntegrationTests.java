package ch.shield.auth.integration;

import ch.shield.auth.Application;
import ch.shield.auth.core.engine.Engine;
import ch.shield.auth.core.engine.EngineException;
import ch.shield.auth.core.engine.SessionAttributes;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@TestPropertySource(
        properties = {
                "session.inmemory.timeoutSec=2",
                "session.inmemory.cleanupRateMs=500"})
public class SessionCleanupIntegrationTests {
    @Autowired
    private Engine target;

    @Mock
    private SessionAttributes sessionAttributes;

    @Test(expected = EngineException.class)
    public void cleanup() throws InterruptedException {
        final var sessionId = target.init(sessionAttributes);
        target.check(sessionId);

        Thread.sleep(4000);

        target.check(sessionId);
    }
}

package ch.shield.auth.integration.repository.jdbc;

import ch.shield.auth.Application;
import ch.shield.auth.core.repository.Property;
import ch.shield.auth.core.repository.Repository;
import ch.shield.auth.repository.jdbc.JdbcRepository;
import ch.shield.auth.repository.jdbc.JdbcRepositoryException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class UserIntegrationTests {
    @Autowired
    private AutowireCapableBeanFactory factory;

    private Repository target;

    @Before
    public void before() {
        target = factory.createBean(JdbcRepository.class);
    }

    @Test
    public void createUser() {
        final var user = target.createUser("test-1");

        Assert.assertEquals("test-1", user.getId());
        Assert.assertEquals(0, user.getProperties(Property.class).size());
    }

    @Test
    public void getUser() {
        target.createUser("test-2");

        final var user = target.getUserByUserId("test-2");

        Assert.assertTrue(user.isPresent());
        Assert.assertEquals("test-2", user.get().getId());
        Assert.assertEquals(0, user.get().getProperties(Property.class).size());
    }

    @Test
    public void getNonExisting() {
        final var user = target.getUserByUserId("test-error");

        Assert.assertFalse(user.isPresent());
    }

    @Test(expected = JdbcRepositoryException.class)
    public void createExisting() {
        target.createUser("test-3");

        target.createUser("test-3");
    }
}

package ch.shield.auth.integration.repository.jdbc;

import ch.shield.auth.Application;
import ch.shield.auth.core.repository.EmailProperty;
import ch.shield.auth.core.repository.Repository;
import ch.shield.auth.repository.jdbc.JdbcRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class PropertyIntegrationTests {
    @Autowired
    private AutowireCapableBeanFactory factory;

    private Repository target;

    @Before
    public void before() {
        target = factory.createBean(JdbcRepository.class);
    }

    @Test
    public void createProperty() {
        final var user = target.createUser("test-prop-1");
        final var newProperty = user.createProperty(EmailProperty.class);

        Assert.assertNotNull(newProperty);
        Assert.assertEquals("", newProperty.getEmail());
    }

    @Test
    public void getNewProperty() {
        final var user = target.createUser("test-prop-2");
        user.createProperty(EmailProperty.class);

        final var properties = user.getProperties(EmailProperty.class);

        Assert.assertEquals(1, properties.size());
        Assert.assertEquals("", properties.get(0).getEmail());
    }

    @Test
    public void updateNewProperty() {
        final var user = target.createUser("test-prop-3");
        final var newProperty = user.createProperty(EmailProperty.class);
        newProperty.setEmail("teset@example.com");
    }

    @Test
    public void getUpdatedNewProperty() {
        final var user = target.createUser("test-prop-4");
        final var newProperty = user.createProperty(EmailProperty.class);
        newProperty.setEmail("test@example.com");

        final var properties = user.getProperties(EmailProperty.class);

        Assert.assertEquals(1, properties.size());
        Assert.assertEquals("test@example.com", properties.get(0).getEmail());
    }

    @Test
    public void updateExistingProperty() {
        final var user = target.createUser("test-prop-5");
        final var newProperty = user.createProperty(EmailProperty.class);
        newProperty.setEmail("test@example.com");
        final var propertyExisting = user.getProperties(EmailProperty.class).get(0);
        propertyExisting.setEmail("eadfsg@asdg.com");

        final var properties = user.getProperties(EmailProperty.class);

        Assert.assertEquals(1, properties.size());
        Assert.assertEquals("eadfsg@asdg.com", properties.get(0).getEmail());
    }

    @Test
    public void findPropertyByValue() {
        final var newUser = target.createUser("test-prop-6");
        final var newProperty = newUser.createProperty(EmailProperty.class);
        newProperty.setEmail("test-unique@example.com");

        final var properties = target.getPropertyByValue(EmailProperty.class, "test-unique@example.com");

        Assert.assertEquals(1, properties.size());
        Assert.assertEquals("test-unique@example.com", properties.get(0).getEmail());
        Assert.assertEquals("test-prop-6", properties.get(0).getUser().getId());
    }

    @Test
    public void deleteProperty() {
        final var user = target.createUser("test-prop-7");
        final var newProperty = user.createProperty(EmailProperty.class);
        newProperty.setEmail("test-1123@example.com");

        final var properties = user.getProperties(EmailProperty.class);
        user.removeProperty(properties.get(0));

        final var properties2 = user.getProperties(EmailProperty.class);

        Assert.assertEquals(0, properties2.size());
    }

    @Test
    public void getNoAdditional() {
        final var user = target.createUser("test-prop-8");
        final var newProperty = user.createProperty(EmailProperty.class);
        newProperty.setEmail("test-1123@example.com");

        final var properties = user.getProperties(EmailProperty.class);

        Assert.assertNull(properties.get(0).getUsage());
    }

    @Test
    public void addAdditional() {
        final var user = target.createUser("test-prop-9");
        final var newProperty = user.createProperty(EmailProperty.class);
        newProperty.setEmail("test-1123@example.com");
        newProperty.setUsage("private");

        final var properties = user.getProperties(EmailProperty.class);
        Assert.assertEquals("private", properties.get(0).getUsage());
    }
}
